﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors

Public Class XtraEmployeeQuickEdit
    Dim ulf As UserLookAndFeel
    Public Sub New()
        InitializeComponent()
    End Sub
    Private Sub AllgridlookupLoad()
        If Common.servername = "Access" Then
            Me.TblDepartment1TableAdapter1.Fill(Me.SSSDBDataSet.tblDepartment1)
            GridLookUpEditDept.Properties.DataSource = SSSDBDataSet.tblDepartment1

            Me.TblCatagory1TableAdapter1.Fill(Me.SSSDBDataSet.tblCatagory1)
            GridLookUpEditCat.Properties.DataSource = SSSDBDataSet.tblCatagory1

            Me.EmployeeGroup1TableAdapter1.Fill(Me.SSSDBDataSet.EmployeeGroup1)
            GridLookUpEditEmpGrp.Properties.DataSource = SSSDBDataSet.EmployeeGroup1

            Me.TblGrade1TableAdapter1.Fill(Me.SSSDBDataSet.tblGrade1)
            GridLookUpEditGrd.Properties.DataSource = SSSDBDataSet.tblGrade1

        Else

            TblDepartmentTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblDepartmentTableAdapter.Fill(Me.SSSDBDataSet.tblDepartment)
            GridLookUpEditDept.Properties.DataSource = SSSDBDataSet.tblDepartment

            TblCatagoryTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblCatagoryTableAdapter.Fill(Me.SSSDBDataSet.tblCatagory)
            GridLookUpEditCat.Properties.DataSource = SSSDBDataSet.tblCatagory

            EmployeeGroupTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.EmployeeGroupTableAdapter.Fill(Me.SSSDBDataSet.EmployeeGroup)
            GridLookUpEditEmpGrp.Properties.DataSource = SSSDBDataSet.EmployeeGroup

            TblGradeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblGradeTableAdapter.Fill(Me.SSSDBDataSet.tblGrade)
            GridLookUpEditGrd.Properties.DataSource = SSSDBDataSet.tblGrade

        End If

        GridLookUpEditBr.Properties.DataSource = Common.LocationNonAdmin
        GridLookUpEditCom.Properties.DataSource = Common.CompanyNonAdmin

        GridLookUpEditCom.EditValue = GridLookUpEditCom.Properties.GetKeyValue(0)
        GridLookUpEditCat.EditValue = GridLookUpEditCat.Properties.GetKeyValue(0)
        GridLookUpEditBr.EditValue = GridLookUpEditBr.Properties.GetKeyValue(0)
        GridLookUpEditDept.EditValue = GridLookUpEditDept.Properties.GetKeyValue(0)
        GridLookUpEditEmpGrp.EditValue = GridLookUpEditEmpGrp.Properties.GetKeyValue(0)
        GridLookUpEditGrd.EditValue = GridLookUpEditGrd.Properties.GetKeyValue(0)


        GridLookUpEditCom.Enabled = False
        GridLookUpEditCat.Enabled = False
        GridLookUpEditBr.Enabled = False
        GridLookUpEditDept.Enabled = False
        GridLookUpEditEmpGrp.Enabled = False
        GridLookUpEditGrd.Enabled = False
        DateEditJoin.Enabled = False
        ComboBoxEditNepaliDate.Enabled = False
        ComboBoxEditNEpaliMonth.Enabled = False
        ComboBoxEditNepaliYear.Enabled = False


        CheckCom.Checked = False
        CheckDept.Checked = False
        CheckEmpGrp.Checked = False
        CheckGrd.Checked = False
        CheckCat.Checked = False
        CheckLoc.Checked = False
        CheckDOJ.Checked = False
        'If Common.USERTYPE <> "A" Then
        '    Dim emp() As String = Common.Auth_Branch.Split(",")
        '    Dim ls As New List(Of String)()
        '    For x As Integer = 0 To emp.Length - 1
        '        ls.Add(emp(x).Trim)
        '    Next
        '    Dim locatuontbl As DataTable = New DataTable("tblbranch")
        '    Dim gridselet As String = "select * from tblbranch where BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
        '    If Common.servername = "Access" Then
        '        Dim dataAdapter As New OleDbDataAdapter(gridselet, Common.con1)
        '        dataAdapter.Fill(locatuontbl)
        '    Else
        '        Dim dataAdapter As New SqlClient.SqlDataAdapter(gridselet, Common.con)
        '        dataAdapter.Fill(locatuontbl)
        '    End If
        '    GridLookUpEdit8.Properties.DataSource = Common.LocationNonAdmin         
        'End If
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Me.Cursor = Cursors.WaitCursor
        Common.LogPost("Employee Quick Update")
        Dim cmd As SqlCommand
        Dim cmd1 As OleDbCommand
        Dim sSql As String = "Update TblEmployee set "
        Dim sSql1 As String = ""
        If CheckCom.Checked = True Then
            sSql1 = sSql1 & "COMPANYCODE='" & GridLookUpEditCom.EditValue & "'"
        End If
        If CheckLoc.Checked = True Then
            sSql1 = sSql1 & ", BRANCHCODE='" & GridLookUpEditBr.EditValue & "'"
        End If
        If CheckDept.Checked = True Then
            sSql1 = sSql1 & ", DEPARTMENTCODE='" & GridLookUpEditDept.EditValue & "'"
        End If
        If CheckCat.Checked = True Then
            sSql1 = sSql1 & ", CAT='" & GridLookUpEditCat.EditValue & "'"
        End If
        If CheckEmpGrp.Checked = True Then
            sSql1 = sSql1 & ", EmployeeGroupId='" & GridLookUpEditEmpGrp.EditValue & "'"
        End If
        If CheckGrd.Checked = True Then
            sSql1 = sSql1 & ", GradeCode='" & GridLookUpEditGrd.EditValue & "'"
        End If

        If CheckDOJ.Checked = True Then
            If Common.IsNepali = "Y" Then
                Dim DC As New DateConverter()
                If ComboBoxEditNepaliYear.EditValue.ToString.Trim <> "" And ComboBoxEditNEpaliMonth.EditValue.ToString.Trim <> "" And ComboBoxEditNepaliDate.EditValue.ToString.Trim <> "" Then
                    Try
                        'DateEdit2.DateTime = DC.ToAD(New Date(ComboBoxEditNepaliYear.EditValue, ComboBoxEditNEpaliMonth.SelectedIndex + 1, ComboBoxEditNepaliDate.EditValue))
                        DateEditJoin.DateTime = DC.ToAD(ComboBoxEditNepaliYear.EditValue & "-" & ComboBoxEditNEpaliMonth.SelectedIndex + 1 & "-" & ComboBoxEditNepaliDate.EditValue)
                    Catch ex As Exception
                        XtraMessageBox.Show(ulf, "<size=10>Invalid Date Of Joining</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        ComboBoxEditNepaliDate.Select()
                        Exit Sub
                    End Try
                End If
            End If
            sSql1 = sSql1 & ", DateOFJOIN='" & DateEditJoin.DateTime.ToString("yyyy-MM-dd HH:mm:ss") & "'"
        End If
        sSql = sSql & sSql1.TrimStart(",") & " where PAYCODE IN ('" & String.Join("', '", XtraEmployee.EmpQuickList.ToArray()) & "') "

        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        If CheckEmpGrp.Checked = True Then
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds2 As DataSet = New DataSet
            sSql = "select * from EmployeeGroup where GroupId = '" & GridLookUpEditEmpGrp.EditValue & "'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds2)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds2)
            End If


            If Common.servername = "Access" Then
                sSql = "Update tblEmployeeShiftMaster set  SHIFT = '" & ds2.Tables(0).Rows(0).Item("SHIFT").ToString() & "', " &
              "SHIFTTYPE = '" & ds2.Tables(0).Rows(0).Item("SHIFTTYPE").ToString() & "', SHIFTPATTERN = '" & ds2.Tables(0).Rows(0).Item("SHIFTPATTERN").ToString() & "', " &
              "SHIFTREMAINDAYS = '" & ds2.Tables(0).Rows(0).Item("SHIFTREMAINDAYS").ToString() & "', " &
              "LASTSHIFTPERFORMED  = '" & ds2.Tables(0).Rows(0).Item("LASTSHIFTPERFORMED").ToString() & "', " &
              "INONLY = '" & ds2.Tables(0).Rows(0).Item("INONLY").ToString() & "', ISPUNCHALL = '" & ds2.Tables(0).Rows(0).Item("ISPUNCHALL").ToString() & "', " &
              "ISTIMELOSSALLOWED = '" & ds2.Tables(0).Rows(0).Item("ISTIMELOSSALLOWED").ToString() & "', ALTERNATE_OFF_DAYS = '" & ds2.Tables(0).Rows(0).Item("ALTERNATE_OFF_DAYS").ToString() & "', " &
              "CDAYS = '" & ds2.Tables(0).Rows(0).Item("CDAYS").ToString() & "', ISROUNDTHECLOCKWORK = '" & ds2.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString() & "', " &
              "ISOT = '" & ds2.Tables(0).Rows(0).Item("ISOT").ToString() & "', OTRATE = '" & ds2.Tables(0).Rows(0).Item("OTRATE").ToString() & "', " &
              "FIRSTOFFDAY = '" & ds2.Tables(0).Rows(0).Item("FIRSTOFFDAY").ToString() & "', SECONDOFFTYPE = '" & ds2.Tables(0).Rows(0).Item("SECONDOFFTYPE").ToString() & "', " &
              "HALFDAYSHIFT = '" & ds2.Tables(0).Rows(0).Item("HALFDAYSHIFT").ToString() & "', SECONDOFFDAY = '" & ds2.Tables(0).Rows(0).Item("SECONDOFFDAY").ToString() & "', " &
              "PERMISLATEARRIVAL = '" & ds2.Tables(0).Rows(0).Item("PERMISLATEARRIVAL").ToString() & "', PERMISEARLYDEPRT = '" & ds2.Tables(0).Rows(0).Item("PERMISEARLYDEPRT").ToString() & "', " &
              "ISAUTOSHIFT = '" & ds2.Tables(0).Rows(0).Item("ISAUTOSHIFT").ToString() & "', ISOUTWORK = '" & ds2.Tables(0).Rows(0).Item("ISOUTWORK").ToString() & "', " &
              "MAXDAYMIN = '" & ds2.Tables(0).Rows(0).Item("MAXDAYMIN").ToString() & "', ISOS = '" & ds2.Tables(0).Rows(0).Item("ISOS").ToString() & "', " &
              "AUTH_SHIFTS = '" & ds2.Tables(0).Rows(0).Item("AUTH_SHIFTS").ToString() & "', TIME1 = '" & ds2.Tables(0).Rows(0).Item("TIME1").ToString() & "', " &
              "SHORT1 = '" & ds2.Tables(0).Rows(0).Item("SHORT1").ToString() & "', HALF = '" & ds2.Tables(0).Rows(0).Item("HALF").ToString() & "', " &
              "ISHALFDAY = '" & ds2.Tables(0).Rows(0).Item("ISHALFDAY").ToString() & "', ISSHORT = '" & ds2.Tables(0).Rows(0).Item("ISSHORT").ToString() & "', " &
              "TWO = '" & ds2.Tables(0).Rows(0).Item("TWO").ToString() & "', MARKMISSASHALFDAY='" & ds2.Tables(0).Rows(0).Item("MARKMISSASHALFDAY").ToString() & "', " &
              "MSHIFT ='" & ds2.Tables(0).Rows(0).Item("MSHIFT").ToString() & "' " &
              "where PAYCODE IN ('" & String.Join("', '", XtraEmployee.EmpQuickList.ToArray()) & "') "
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                sSql = "Update tblEmployeeShiftMaster set  SHIFT = '" & ds2.Tables(0).Rows(0).Item("SHIFT").ToString() & "', " &
              "SHIFTTYPE = '" & ds2.Tables(0).Rows(0).Item("SHIFTTYPE").ToString() & "', SHIFTPATTERN = '" & ds2.Tables(0).Rows(0).Item("SHIFTPATTERN").ToString() & "', " &
              "SHIFTREMAINDAYS = '" & ds2.Tables(0).Rows(0).Item("SHIFTREMAINDAYS").ToString() & "', " &
              "LASTSHIFTPERFORMED  = '" & ds2.Tables(0).Rows(0).Item("LASTSHIFTPERFORMED").ToString() & "', " &
              "INONLY = '" & ds2.Tables(0).Rows(0).Item("INONLY").ToString() & "', ISPUNCHALL = '" & ds2.Tables(0).Rows(0).Item("ISPUNCHALL").ToString() & "', " &
              "ISTIMELOSSALLOWED = '" & ds2.Tables(0).Rows(0).Item("ISTIMELOSSALLOWED").ToString() & "', ALTERNATE_OFF_DAYS = '" & ds2.Tables(0).Rows(0).Item("ALTERNATE_OFF_DAYS").ToString() & "', " &
              "CDAYS = '" & ds2.Tables(0).Rows(0).Item("CDAYS").ToString() & "', ISROUNDTHECLOCKWORK = '" & ds2.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString() & "', " &
              "ISOT = '" & ds2.Tables(0).Rows(0).Item("ISOT").ToString() & "', OTRATE = '" & ds2.Tables(0).Rows(0).Item("OTRATE").ToString() & "', " &
              "FIRSTOFFDAY = '" & ds2.Tables(0).Rows(0).Item("FIRSTOFFDAY").ToString() & "', SECONDOFFTYPE = '" & ds2.Tables(0).Rows(0).Item("SECONDOFFTYPE").ToString() & "', " &
              "HALFDAYSHIFT = '" & ds2.Tables(0).Rows(0).Item("HALFDAYSHIFT").ToString() & "', SECONDOFFDAY = '" & ds2.Tables(0).Rows(0).Item("SECONDOFFDAY").ToString() & "', " &
              "PERMISLATEARRIVAL = '" & ds2.Tables(0).Rows(0).Item("PERMISLATEARRIVAL").ToString() & "', PERMISEARLYDEPRT = '" & ds2.Tables(0).Rows(0).Item("PERMISEARLYDEPRT").ToString() & "', " &
              "ISAUTOSHIFT = '" & ds2.Tables(0).Rows(0).Item("ISAUTOSHIFT").ToString() & "', ISOUTWORK = '" & ds2.Tables(0).Rows(0).Item("ISOUTWORK").ToString() & "', " &
              "MAXDAYMIN = '" & ds2.Tables(0).Rows(0).Item("MAXDAYMIN").ToString() & "', ISOS = '" & ds2.Tables(0).Rows(0).Item("ISOS").ToString() & "', " &
              "AUTH_SHIFTS = '" & ds2.Tables(0).Rows(0).Item("AUTH_SHIFTS").ToString() & "', TIME = '" & ds2.Tables(0).Rows(0).Item("TIME").ToString() & "', " &
              "SHORT = '" & ds2.Tables(0).Rows(0).Item("SHORT").ToString() & "', HALF = '" & ds2.Tables(0).Rows(0).Item("HALF").ToString() & "', " &
              "ISHALFDAY = '" & ds2.Tables(0).Rows(0).Item("ISHALFDAY").ToString() & "', ISSHORT = '" & ds2.Tables(0).Rows(0).Item("ISSHORT").ToString() & "', " &
              "TWO = '" & ds2.Tables(0).Rows(0).Item("TWO").ToString() & "', MARKMISSASHALFDAY='" & ds2.Tables(0).Rows(0).Item("MARKMISSASHALFDAY").ToString() & "', " &
              "MSHIFT ='" & ds2.Tables(0).Rows(0).Item("MSHIFT").ToString() & "' " &
              "where PAYCODE IN ('" & String.Join("', '", XtraEmployee.EmpQuickList.ToArray()) & "') "
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If

            Dim comclass As Common = New Common
            Dim paycode() As String = XtraEmployee.EmpQuickList.Distinct.ToArray
            For i As Integer = 0 To paycode.Length - 1
                Dim ds As DataSet = New DataSet
                sSql1 = "Select PAYCODE, dateofjoin from tblEmployee Where tblEmployee.active = 'Y' and PayCode = '" & paycode(i).Trim & "'  Order By Paycode"
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql1, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql1, Common.con)
                    adap.Fill(ds)
                End If
                'MsgBox(ds.Tables(0).Rows.Count)
                Dim mmin_date As String
                'If Convert.ToDateTime(ds.Tables(0).Rows(0).Item("dateofjoin")) > Convert.ToDateTime(mmin_date) Then
                mmin_date = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("dateofjoin")).ToString("dd/MMM/yyyy")
                'Else
                'mmin_date = DateEdit3.DateTime.ToString("dd/MMM/yyyy")
                'End If
                Dim g_Wo_Include As String = Common.EmpGrpArr(ds2.Tables(0).Rows(0).Item("Id")).g_Wo_Include
                comclass.DutyRosterUpd(ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, mmin_date, g_Wo_Include)
            Next
        End If
        If CheckDOJ.Checked = True Then
            Dim comclass As Common = New Common
            Dim paycode() As String = XtraEmployee.EmpQuickList.Distinct.ToArray
            For i As Integer = 0 To paycode.Length - 1
                Dim ds As DataSet = New DataSet
                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter
                'sSql1 = "Select PAYCODE, dateofjoin from tblEmployee Where tblEmployee.active = 'Y' and PayCode = '" & paycode(i).Trim & "'  Order By Paycode"
                sSql1 = "select TblEmployee.PAYCODE,tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK,EmployeeGroup.ID 
                from tblEmployeeShiftMaster, TblEmployee, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE 
                and TblEmployee.ACTIVE = 'Y' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId 
                and TblEmployee.PAYCODE = '" & paycode(i).Trim & "' Order By TblEmployee.PAYCODE"
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql1, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql1, Common.con)
                    adap.Fill(ds)
                End If
                'MsgBox(ds.Tables(0).Rows.Count)
                Dim mmin_date As String = DateEditJoin.DateTime.ToString("dd/MMM/yyyy")
                'If Convert.ToDateTime(ds.Tables(0).Rows(0).Item("dateofjoin")) > Convert.ToDateTime(mmin_date) Then
                'mmin_date = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("dateofjoin")).ToString("dd/MMM/yyyy")
                'Else
                'mmin_date = DateEdit3.DateTime.ToString("dd/MMM/yyyy")
                'End If
                Dim g_Wo_Include As String = Common.EmpGrpArr(ds.Tables(0).Rows(0).Item("ID")).g_Wo_Include
                comclass.DutyRoster(ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, mmin_date, g_Wo_Include)
            Next
        End If
        Me.Cursor = Cursors.Default
        XtraMessageBox.Show(ulf, "<size=10>Updated Successfully</size>", "<size=9>iAS</size>")
        Me.Close()
    End Sub

    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        Me.Close()
    End Sub


    Private Sub CheckCom_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckCom.CheckedChanged
        If CheckCom.Checked = True Then
            GridLookUpEditCom.Enabled = True
        Else
            GridLookUpEditCom.Enabled = False
        End If
    End Sub

    Private Sub CheckLoc_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckLoc.CheckedChanged
        If CheckLoc.Checked = True Then
            GridLookUpEditBr.Enabled = True
        Else
            GridLookUpEditBr.Enabled = False
        End If
    End Sub

    Private Sub CheckDept_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckDept.CheckedChanged
        If CheckDept.Checked = True Then
            GridLookUpEditDept.Enabled = True
        Else
            GridLookUpEditDept.Enabled = False
        End If
    End Sub

    Private Sub CheckCat_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckCat.CheckedChanged
        If CheckCat.Checked = True Then
            GridLookUpEditCat.Enabled = True
        Else
            GridLookUpEditCat.Enabled = False
        End If
    End Sub

    Private Sub CheckEmpGrp_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEmpGrp.CheckedChanged
        If CheckEmpGrp.Checked = True Then
            GridLookUpEditEmpGrp.Enabled = True
        Else
            GridLookUpEditEmpGrp.Enabled = False
        End If
    End Sub

    Private Sub CheckGrd_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckGrd.CheckedChanged
        If CheckGrd.Checked = True Then
            GridLookUpEditGrd.Enabled = True
        Else
            GridLookUpEditGrd.Enabled = False
        End If
    End Sub

    Private Sub XtraEmployeeQuickEdit_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        DateEditJoin.DateTime = Now.ToString("yyyy-MM-01")
        If Common.IsNepali = "Y" Then
            ComboBoxEditNepaliYear.Visible = True
            ComboBoxEditNEpaliMonth.Visible = True
            ComboBoxEditNepaliDate.Visible = True
            DateEditJoin.Visible = False
        Else
            ComboBoxEditNepaliYear.Visible = False
            ComboBoxEditNEpaliMonth.Visible = False
            ComboBoxEditNepaliDate.Visible = False
            DateEditJoin.Visible = True
        End If
        AllgridlookupLoad()
    End Sub

    Private Sub CheckDOJ_CheckedChanged(sender As Object, e As EventArgs) Handles CheckDOJ.CheckedChanged
        If CheckDOJ.Checked = True Then
            DateEditJoin.Enabled = True
            ComboBoxEditNepaliDate.Enabled = True
            ComboBoxEditNEpaliMonth.Enabled = True
            ComboBoxEditNepaliYear.Enabled = True
        Else
            DateEditJoin.Enabled = False
            ComboBoxEditNepaliDate.Enabled = False
            ComboBoxEditNEpaliMonth.Enabled = False
            ComboBoxEditNepaliYear.Enabled = False
        End If
    End Sub
End Class