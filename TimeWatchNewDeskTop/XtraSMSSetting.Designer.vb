﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraSMSSetting
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraSMSSetting))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.GroupControl8 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleMachineWise = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.TextAllSms2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextAllSms1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitchAllPunch = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl7 = New DevExpress.XtraEditors.GroupControl()
        Me.TextOutSMSAfter = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.TextOutPunchSMSContent2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextOutPunchSMSContent1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleOUTSMS = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl6 = New DevExpress.XtraEditors.GroupControl()
        Me.TextInPunchSMSContent2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextInPunchSMSContent1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleINSMS = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.TextLateSMSContent2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextLateSMSContent1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleLateSMS = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckName = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckDate = New DevExpress.XtraEditors.CheckEdit()
        Me.TextAbsentSMSContent2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextAbsentSMSContent1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.TextEditSMSTrigerAfterShiftStartTime = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditPreviousdaySMSTriggerTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.CheckPreviousDay = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckCurrentDate = New DevExpress.XtraEditors.CheckEdit()
        Me.ToggleAbsentSMS = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.TextKey = New DevExpress.XtraEditors.MemoEdit()
        Me.TextSenderId = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl8.SuspendLayout()
        CType(Me.ToggleMachineWise.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextAllSms2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextAllSms1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitchAllPunch.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl7.SuspendLayout()
        CType(Me.TextOutSMSAfter.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextOutPunchSMSContent2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextOutPunchSMSContent1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleOUTSMS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        CType(Me.TextInPunchSMSContent2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextInPunchSMSContent1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleINSMS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.TextLateSMSContent2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextLateSMSContent1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleLateSMS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.CheckName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextAbsentSMSContent2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextAbsentSMSContent1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.TextEditSMSTrigerAfterShiftStartTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditPreviousdaySMSTriggerTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.CheckPreviousDay.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckCurrentDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleAbsentSMS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.TextKey.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextSenderId.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl8)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SimpleButton1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl7)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl6)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl5)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl2)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1145, 568)
        Me.SplitContainerControl1.SplitterPosition = 1036
        Me.SplitContainerControl1.TabIndex = 3
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'GroupControl8
        '
        Me.GroupControl8.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl8.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl8.AppearanceCaption.Options.UseFont = True
        Me.GroupControl8.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl8.Controls.Add(Me.LabelControl22)
        Me.GroupControl8.Controls.Add(Me.ToggleMachineWise)
        Me.GroupControl8.Controls.Add(Me.LabelControl18)
        Me.GroupControl8.Controls.Add(Me.TextAllSms2)
        Me.GroupControl8.Controls.Add(Me.TextAllSms1)
        Me.GroupControl8.Controls.Add(Me.LabelControl19)
        Me.GroupControl8.Controls.Add(Me.LabelControl20)
        Me.GroupControl8.Controls.Add(Me.ToggleSwitchAllPunch)
        Me.GroupControl8.Controls.Add(Me.LabelControl21)
        Me.GroupControl8.Location = New System.Drawing.Point(484, 300)
        Me.GroupControl8.Name = "GroupControl8"
        Me.GroupControl8.Size = New System.Drawing.Size(469, 174)
        Me.GroupControl8.TabIndex = 33
        Me.GroupControl8.Text = "All Punch SMS"
        '
        'LabelControl22
        '
        Me.LabelControl22.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl22.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl22.Appearance.Options.UseFont = True
        Me.LabelControl22.Appearance.Options.UseForeColor = True
        Me.LabelControl22.Location = New System.Drawing.Point(232, 69)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(91, 14)
        Me.LabelControl22.TabIndex = 36
        Me.LabelControl22.Text = "(Real Time Only)"
        Me.LabelControl22.Visible = False
        '
        'ToggleMachineWise
        '
        Me.ToggleMachineWise.Location = New System.Drawing.Point(131, 63)
        Me.ToggleMachineWise.Name = "ToggleMachineWise"
        Me.ToggleMachineWise.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleMachineWise.Properties.Appearance.Options.UseFont = True
        Me.ToggleMachineWise.Properties.OffText = "No"
        Me.ToggleMachineWise.Properties.OnText = "Yes"
        Me.ToggleMachineWise.Size = New System.Drawing.Size(95, 25)
        Me.ToggleMachineWise.TabIndex = 35
        Me.ToggleMachineWise.Visible = False
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl18.Appearance.Options.UseFont = True
        Me.LabelControl18.Location = New System.Drawing.Point(5, 68)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(115, 14)
        Me.LabelControl18.TabIndex = 34
        Me.LabelControl18.Text = "Machine Wise In/Out"
        Me.LabelControl18.Visible = False
        '
        'TextAllSms2
        '
        Me.TextAllSms2.Location = New System.Drawing.Point(118, 129)
        Me.TextAllSms2.Name = "TextAllSms2"
        Me.TextAllSms2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextAllSms2.Properties.Appearance.Options.UseFont = True
        Me.TextAllSms2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextAllSms2.Properties.MaxLength = 250
        Me.TextAllSms2.Size = New System.Drawing.Size(344, 20)
        Me.TextAllSms2.TabIndex = 33
        '
        'TextAllSms1
        '
        Me.TextAllSms1.Location = New System.Drawing.Point(118, 103)
        Me.TextAllSms1.Name = "TextAllSms1"
        Me.TextAllSms1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextAllSms1.Properties.Appearance.Options.UseFont = True
        Me.TextAllSms1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextAllSms1.Properties.MaxLength = 250
        Me.TextAllSms1.Size = New System.Drawing.Size(344, 20)
        Me.TextAllSms1.TabIndex = 32
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl19.Appearance.Options.UseFont = True
        Me.LabelControl19.Location = New System.Drawing.Point(6, 132)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(91, 14)
        Me.LabelControl19.TabIndex = 31
        Me.LabelControl19.Text = "SMS Content - 2"
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl20.Appearance.Options.UseFont = True
        Me.LabelControl20.Location = New System.Drawing.Point(6, 106)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(91, 14)
        Me.LabelControl20.TabIndex = 30
        Me.LabelControl20.Text = "SMS Content - 1"
        '
        'ToggleSwitchAllPunch
        '
        Me.ToggleSwitchAllPunch.Location = New System.Drawing.Point(131, 32)
        Me.ToggleSwitchAllPunch.Name = "ToggleSwitchAllPunch"
        Me.ToggleSwitchAllPunch.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitchAllPunch.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitchAllPunch.Properties.OffText = "No"
        Me.ToggleSwitchAllPunch.Properties.OnText = "Yes"
        Me.ToggleSwitchAllPunch.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitchAllPunch.TabIndex = 24
        '
        'LabelControl21
        '
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl21.Appearance.Options.UseFont = True
        Me.LabelControl21.Location = New System.Drawing.Point(5, 37)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(117, 14)
        Me.LabelControl21.TabIndex = 23
        Me.LabelControl21.Text = "Enable All Punch SMS"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(878, 509)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 32
        Me.SimpleButton1.Text = "Save"
        '
        'GroupControl7
        '
        Me.GroupControl7.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl7.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl7.AppearanceCaption.Options.UseFont = True
        Me.GroupControl7.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl7.Controls.Add(Me.TextOutSMSAfter)
        Me.GroupControl7.Controls.Add(Me.LabelControl17)
        Me.GroupControl7.Controls.Add(Me.TextOutPunchSMSContent2)
        Me.GroupControl7.Controls.Add(Me.TextOutPunchSMSContent1)
        Me.GroupControl7.Controls.Add(Me.LabelControl13)
        Me.GroupControl7.Controls.Add(Me.LabelControl14)
        Me.GroupControl7.Controls.Add(Me.ToggleOUTSMS)
        Me.GroupControl7.Controls.Add(Me.LabelControl15)
        Me.GroupControl7.Location = New System.Drawing.Point(484, 136)
        Me.GroupControl7.Name = "GroupControl7"
        Me.GroupControl7.Size = New System.Drawing.Size(469, 158)
        Me.GroupControl7.TabIndex = 31
        Me.GroupControl7.Text = "OUT Punch SMS"
        '
        'TextOutSMSAfter
        '
        Me.TextOutSMSAfter.Location = New System.Drawing.Point(213, 60)
        Me.TextOutSMSAfter.Name = "TextOutSMSAfter"
        Me.TextOutSMSAfter.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextOutSMSAfter.Properties.Appearance.Options.UseFont = True
        Me.TextOutSMSAfter.Properties.Mask.EditMask = "[0-9]*"
        Me.TextOutSMSAfter.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextOutSMSAfter.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextOutSMSAfter.Properties.MaxLength = 3
        Me.TextOutSMSAfter.Size = New System.Drawing.Size(83, 20)
        Me.TextOutSMSAfter.TabIndex = 35
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl17.Appearance.Options.UseFont = True
        Me.LabelControl17.Location = New System.Drawing.Point(6, 63)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(181, 14)
        Me.LabelControl17.TabIndex = 34
        Me.LabelControl17.Text = "SMS Trigger after Shift End Time"
        '
        'TextOutPunchSMSContent2
        '
        Me.TextOutPunchSMSContent2.Location = New System.Drawing.Point(118, 120)
        Me.TextOutPunchSMSContent2.Name = "TextOutPunchSMSContent2"
        Me.TextOutPunchSMSContent2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextOutPunchSMSContent2.Properties.Appearance.Options.UseFont = True
        Me.TextOutPunchSMSContent2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextOutPunchSMSContent2.Properties.MaxLength = 250
        Me.TextOutPunchSMSContent2.Size = New System.Drawing.Size(344, 20)
        Me.TextOutPunchSMSContent2.TabIndex = 33
        '
        'TextOutPunchSMSContent1
        '
        Me.TextOutPunchSMSContent1.Location = New System.Drawing.Point(118, 94)
        Me.TextOutPunchSMSContent1.Name = "TextOutPunchSMSContent1"
        Me.TextOutPunchSMSContent1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextOutPunchSMSContent1.Properties.Appearance.Options.UseFont = True
        Me.TextOutPunchSMSContent1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextOutPunchSMSContent1.Properties.MaxLength = 250
        Me.TextOutPunchSMSContent1.Size = New System.Drawing.Size(344, 20)
        Me.TextOutPunchSMSContent1.TabIndex = 32
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Location = New System.Drawing.Point(6, 123)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(91, 14)
        Me.LabelControl13.TabIndex = 31
        Me.LabelControl13.Text = "SMS Content - 2"
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl14.Appearance.Options.UseFont = True
        Me.LabelControl14.Location = New System.Drawing.Point(6, 97)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(91, 14)
        Me.LabelControl14.TabIndex = 30
        Me.LabelControl14.Text = "SMS Content - 1"
        '
        'ToggleOUTSMS
        '
        Me.ToggleOUTSMS.Location = New System.Drawing.Point(131, 32)
        Me.ToggleOUTSMS.Name = "ToggleOUTSMS"
        Me.ToggleOUTSMS.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleOUTSMS.Properties.Appearance.Options.UseFont = True
        Me.ToggleOUTSMS.Properties.OffText = "No"
        Me.ToggleOUTSMS.Properties.OnText = "Yes"
        Me.ToggleOUTSMS.Size = New System.Drawing.Size(95, 25)
        Me.ToggleOUTSMS.TabIndex = 24
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Location = New System.Drawing.Point(5, 37)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(92, 14)
        Me.LabelControl15.TabIndex = 23
        Me.LabelControl15.Text = "Enable OUT SMS"
        '
        'GroupControl6
        '
        Me.GroupControl6.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl6.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl6.AppearanceCaption.Options.UseFont = True
        Me.GroupControl6.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl6.Controls.Add(Me.TextInPunchSMSContent2)
        Me.GroupControl6.Controls.Add(Me.TextInPunchSMSContent1)
        Me.GroupControl6.Controls.Add(Me.LabelControl8)
        Me.GroupControl6.Controls.Add(Me.LabelControl10)
        Me.GroupControl6.Controls.Add(Me.ToggleINSMS)
        Me.GroupControl6.Controls.Add(Me.LabelControl12)
        Me.GroupControl6.Location = New System.Drawing.Point(484, 3)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.Size = New System.Drawing.Size(469, 127)
        Me.GroupControl6.TabIndex = 30
        Me.GroupControl6.Text = "In Punch SMS"
        '
        'TextInPunchSMSContent2
        '
        Me.TextInPunchSMSContent2.Location = New System.Drawing.Point(118, 89)
        Me.TextInPunchSMSContent2.Name = "TextInPunchSMSContent2"
        Me.TextInPunchSMSContent2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextInPunchSMSContent2.Properties.Appearance.Options.UseFont = True
        Me.TextInPunchSMSContent2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextInPunchSMSContent2.Properties.MaxLength = 250
        Me.TextInPunchSMSContent2.Size = New System.Drawing.Size(344, 20)
        Me.TextInPunchSMSContent2.TabIndex = 33
        '
        'TextInPunchSMSContent1
        '
        Me.TextInPunchSMSContent1.Location = New System.Drawing.Point(118, 63)
        Me.TextInPunchSMSContent1.Name = "TextInPunchSMSContent1"
        Me.TextInPunchSMSContent1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextInPunchSMSContent1.Properties.Appearance.Options.UseFont = True
        Me.TextInPunchSMSContent1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextInPunchSMSContent1.Properties.MaxLength = 250
        Me.TextInPunchSMSContent1.Size = New System.Drawing.Size(344, 20)
        Me.TextInPunchSMSContent1.TabIndex = 32
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(6, 92)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(91, 14)
        Me.LabelControl8.TabIndex = 31
        Me.LabelControl8.Text = "SMS Content - 2"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(6, 66)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(91, 14)
        Me.LabelControl10.TabIndex = 30
        Me.LabelControl10.Text = "SMS Content - 1"
        '
        'ToggleINSMS
        '
        Me.ToggleINSMS.Location = New System.Drawing.Point(131, 32)
        Me.ToggleINSMS.Name = "ToggleINSMS"
        Me.ToggleINSMS.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleINSMS.Properties.Appearance.Options.UseFont = True
        Me.ToggleINSMS.Properties.OffText = "No"
        Me.ToggleINSMS.Properties.OnText = "Yes"
        Me.ToggleINSMS.Size = New System.Drawing.Size(95, 25)
        Me.ToggleINSMS.TabIndex = 24
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(5, 37)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(79, 14)
        Me.LabelControl12.TabIndex = 23
        Me.LabelControl12.Text = "Enable IN SMS"
        '
        'GroupControl5
        '
        Me.GroupControl5.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl5.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl5.AppearanceCaption.Options.UseFont = True
        Me.GroupControl5.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl5.Controls.Add(Me.TextLateSMSContent2)
        Me.GroupControl5.Controls.Add(Me.TextLateSMSContent1)
        Me.GroupControl5.Controls.Add(Me.LabelControl6)
        Me.GroupControl5.Controls.Add(Me.LabelControl7)
        Me.GroupControl5.Controls.Add(Me.ToggleLateSMS)
        Me.GroupControl5.Controls.Add(Me.LabelControl9)
        Me.GroupControl5.Location = New System.Drawing.Point(3, 417)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(475, 127)
        Me.GroupControl5.TabIndex = 29
        Me.GroupControl5.Text = "Late SMS"
        '
        'TextLateSMSContent2
        '
        Me.TextLateSMSContent2.Location = New System.Drawing.Point(118, 89)
        Me.TextLateSMSContent2.Name = "TextLateSMSContent2"
        Me.TextLateSMSContent2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextLateSMSContent2.Properties.Appearance.Options.UseFont = True
        Me.TextLateSMSContent2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextLateSMSContent2.Properties.MaxLength = 250
        Me.TextLateSMSContent2.Size = New System.Drawing.Size(344, 20)
        Me.TextLateSMSContent2.TabIndex = 33
        '
        'TextLateSMSContent1
        '
        Me.TextLateSMSContent1.Location = New System.Drawing.Point(118, 63)
        Me.TextLateSMSContent1.Name = "TextLateSMSContent1"
        Me.TextLateSMSContent1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextLateSMSContent1.Properties.Appearance.Options.UseFont = True
        Me.TextLateSMSContent1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextLateSMSContent1.Properties.MaxLength = 250
        Me.TextLateSMSContent1.Size = New System.Drawing.Size(344, 20)
        Me.TextLateSMSContent1.TabIndex = 32
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(6, 92)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(91, 14)
        Me.LabelControl6.TabIndex = 31
        Me.LabelControl6.Text = "SMS Content - 2"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(6, 66)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(91, 14)
        Me.LabelControl7.TabIndex = 30
        Me.LabelControl7.Text = "SMS Content - 1"
        '
        'ToggleLateSMS
        '
        Me.ToggleLateSMS.Location = New System.Drawing.Point(131, 32)
        Me.ToggleLateSMS.Name = "ToggleLateSMS"
        Me.ToggleLateSMS.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleLateSMS.Properties.Appearance.Options.UseFont = True
        Me.ToggleLateSMS.Properties.OffText = "No"
        Me.ToggleLateSMS.Properties.OnText = "Yes"
        Me.ToggleLateSMS.Size = New System.Drawing.Size(95, 25)
        Me.ToggleLateSMS.TabIndex = 24
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(5, 37)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(120, 14)
        Me.LabelControl9.TabIndex = 23
        Me.LabelControl9.Text = "Enable Shift Late SMS"
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl1.Controls.Add(Me.LabelControl16)
        Me.GroupControl1.Controls.Add(Me.CheckName)
        Me.GroupControl1.Controls.Add(Me.CheckDate)
        Me.GroupControl1.Controls.Add(Me.TextAbsentSMSContent2)
        Me.GroupControl1.Controls.Add(Me.TextAbsentSMSContent1)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.GroupControl4)
        Me.GroupControl1.Controls.Add(Me.GroupControl3)
        Me.GroupControl1.Controls.Add(Me.ToggleAbsentSMS)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Location = New System.Drawing.Point(3, 118)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(475, 293)
        Me.GroupControl1.TabIndex = 28
        Me.GroupControl1.Text = "Absent SMS"
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl16.Appearance.Options.UseFont = True
        Me.LabelControl16.Location = New System.Drawing.Point(11, 271)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(56, 14)
        Me.LabelControl16.TabIndex = 36
        Me.LabelControl16.Text = "Parameter"
        '
        'CheckName
        '
        Me.CheckName.EditValue = True
        Me.CheckName.Location = New System.Drawing.Point(123, 271)
        Me.CheckName.Name = "CheckName"
        Me.CheckName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckName.Properties.Appearance.Options.UseFont = True
        Me.CheckName.Properties.Caption = "Name"
        Me.CheckName.Size = New System.Drawing.Size(118, 19)
        Me.CheckName.TabIndex = 34
        '
        'CheckDate
        '
        Me.CheckDate.EditValue = True
        Me.CheckDate.Location = New System.Drawing.Point(287, 269)
        Me.CheckDate.Name = "CheckDate"
        Me.CheckDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckDate.Properties.Appearance.Options.UseFont = True
        Me.CheckDate.Properties.Caption = "Date"
        Me.CheckDate.Size = New System.Drawing.Size(118, 19)
        Me.CheckDate.TabIndex = 35
        '
        'TextAbsentSMSContent2
        '
        Me.TextAbsentSMSContent2.Location = New System.Drawing.Point(123, 245)
        Me.TextAbsentSMSContent2.Name = "TextAbsentSMSContent2"
        Me.TextAbsentSMSContent2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextAbsentSMSContent2.Properties.Appearance.Options.UseFont = True
        Me.TextAbsentSMSContent2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextAbsentSMSContent2.Properties.MaxLength = 250
        Me.TextAbsentSMSContent2.Size = New System.Drawing.Size(344, 20)
        Me.TextAbsentSMSContent2.TabIndex = 33
        '
        'TextAbsentSMSContent1
        '
        Me.TextAbsentSMSContent1.Location = New System.Drawing.Point(123, 219)
        Me.TextAbsentSMSContent1.Name = "TextAbsentSMSContent1"
        Me.TextAbsentSMSContent1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextAbsentSMSContent1.Properties.Appearance.Options.UseFont = True
        Me.TextAbsentSMSContent1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextAbsentSMSContent1.Properties.MaxLength = 250
        Me.TextAbsentSMSContent1.Size = New System.Drawing.Size(344, 20)
        Me.TextAbsentSMSContent1.TabIndex = 32
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(11, 248)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(91, 14)
        Me.LabelControl5.TabIndex = 31
        Me.LabelControl5.Text = "SMS Content - 2"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(11, 222)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(91, 14)
        Me.LabelControl4.TabIndex = 30
        Me.LabelControl4.Text = "SMS Content - 1"
        '
        'GroupControl4
        '
        Me.GroupControl4.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl4.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl4.AppearanceCaption.Options.UseFont = True
        Me.GroupControl4.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl4.Controls.Add(Me.TextEditSMSTrigerAfterShiftStartTime)
        Me.GroupControl4.Controls.Add(Me.TextEditPreviousdaySMSTriggerTime)
        Me.GroupControl4.Controls.Add(Me.LabelControl3)
        Me.GroupControl4.Location = New System.Drawing.Point(5, 141)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(331, 72)
        Me.GroupControl4.TabIndex = 29
        '
        'TextEditSMSTrigerAfterShiftStartTime
        '
        Me.TextEditSMSTrigerAfterShiftStartTime.Location = New System.Drawing.Point(210, 38)
        Me.TextEditSMSTrigerAfterShiftStartTime.Name = "TextEditSMSTrigerAfterShiftStartTime"
        Me.TextEditSMSTrigerAfterShiftStartTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditSMSTrigerAfterShiftStartTime.Properties.Appearance.Options.UseFont = True
        Me.TextEditSMSTrigerAfterShiftStartTime.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditSMSTrigerAfterShiftStartTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditSMSTrigerAfterShiftStartTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditSMSTrigerAfterShiftStartTime.Properties.MaxLength = 3
        Me.TextEditSMSTrigerAfterShiftStartTime.Size = New System.Drawing.Size(83, 20)
        Me.TextEditSMSTrigerAfterShiftStartTime.TabIndex = 26
        '
        'TextEditPreviousdaySMSTriggerTime
        '
        Me.TextEditPreviousdaySMSTriggerTime.EditValue = ""
        Me.TextEditPreviousdaySMSTriggerTime.Location = New System.Drawing.Point(211, 38)
        Me.TextEditPreviousdaySMSTriggerTime.Name = "TextEditPreviousdaySMSTriggerTime"
        Me.TextEditPreviousdaySMSTriggerTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditPreviousdaySMSTriggerTime.Properties.Appearance.Options.UseFont = True
        Me.TextEditPreviousdaySMSTriggerTime.Properties.Mask.EditMask = "HH:mm"
        Me.TextEditPreviousdaySMSTriggerTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEditPreviousdaySMSTriggerTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditPreviousdaySMSTriggerTime.Properties.MaxLength = 5
        Me.TextEditPreviousdaySMSTriggerTime.Size = New System.Drawing.Size(72, 20)
        Me.TextEditPreviousdaySMSTriggerTime.TabIndex = 25
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(5, 41)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(169, 14)
        Me.LabelControl3.TabIndex = 24
        Me.LabelControl3.Text = "Previous day SMS Trigger Time"
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl3.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl3.AppearanceCaption.Options.UseFont = True
        Me.GroupControl3.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl3.Controls.Add(Me.CheckPreviousDay)
        Me.GroupControl3.Controls.Add(Me.CheckCurrentDate)
        Me.GroupControl3.Location = New System.Drawing.Point(5, 63)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(236, 72)
        Me.GroupControl3.TabIndex = 28
        Me.GroupControl3.Text = "SMS Trigger For"
        '
        'CheckPreviousDay
        '
        Me.CheckPreviousDay.Location = New System.Drawing.Point(118, 36)
        Me.CheckPreviousDay.Name = "CheckPreviousDay"
        Me.CheckPreviousDay.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckPreviousDay.Properties.Appearance.Options.UseFont = True
        Me.CheckPreviousDay.Properties.Caption = "Previous Day"
        Me.CheckPreviousDay.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckPreviousDay.Properties.RadioGroupIndex = 0
        Me.CheckPreviousDay.Size = New System.Drawing.Size(103, 19)
        Me.CheckPreviousDay.TabIndex = 1
        Me.CheckPreviousDay.TabStop = False
        '
        'CheckCurrentDate
        '
        Me.CheckCurrentDate.EditValue = True
        Me.CheckCurrentDate.Location = New System.Drawing.Point(6, 36)
        Me.CheckCurrentDate.Name = "CheckCurrentDate"
        Me.CheckCurrentDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckCurrentDate.Properties.Appearance.Options.UseFont = True
        Me.CheckCurrentDate.Properties.Caption = "Current Date"
        Me.CheckCurrentDate.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckCurrentDate.Properties.RadioGroupIndex = 0
        Me.CheckCurrentDate.Size = New System.Drawing.Size(106, 19)
        Me.CheckCurrentDate.TabIndex = 0
        '
        'ToggleAbsentSMS
        '
        Me.ToggleAbsentSMS.Location = New System.Drawing.Point(131, 32)
        Me.ToggleAbsentSMS.Name = "ToggleAbsentSMS"
        Me.ToggleAbsentSMS.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleAbsentSMS.Properties.Appearance.Options.UseFont = True
        Me.ToggleAbsentSMS.Properties.OffText = "No"
        Me.ToggleAbsentSMS.Properties.OnText = "Yes"
        Me.ToggleAbsentSMS.Size = New System.Drawing.Size(95, 25)
        Me.ToggleAbsentSMS.TabIndex = 24
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(5, 37)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(106, 14)
        Me.LabelControl2.TabIndex = 23
        Me.LabelControl2.Text = "Enable Absent SMS"
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl2.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl2.Controls.Add(Me.TextKey)
        Me.GroupControl2.Controls.Add(Me.TextSenderId)
        Me.GroupControl2.Controls.Add(Me.LabelControl1)
        Me.GroupControl2.Controls.Add(Me.LabelControl11)
        Me.GroupControl2.Location = New System.Drawing.Point(3, 3)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(475, 109)
        Me.GroupControl2.TabIndex = 27
        Me.GroupControl2.Text = "Account Details"
        '
        'TextKey
        '
        Me.TextKey.Location = New System.Drawing.Point(78, 34)
        Me.TextKey.Name = "TextKey"
        Me.TextKey.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextKey.Properties.Appearance.Options.UseFont = True
        Me.TextKey.Size = New System.Drawing.Size(373, 65)
        Me.TextKey.TabIndex = 26
        '
        'TextSenderId
        '
        Me.TextSenderId.Location = New System.Drawing.Point(-17, 60)
        Me.TextSenderId.Name = "TextSenderId"
        Me.TextSenderId.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextSenderId.Properties.Appearance.Options.UseFont = True
        Me.TextSenderId.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextSenderId.Properties.MaxLength = 250
        Me.TextSenderId.Size = New System.Drawing.Size(84, 20)
        Me.TextSenderId.TabIndex = 25
        Me.TextSenderId.Visible = False
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(18, 85)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(54, 14)
        Me.LabelControl1.TabIndex = 24
        Me.LabelControl1.Text = "Sender Id"
        Me.LabelControl1.Visible = False
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(18, 34)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(32, 14)
        Me.LabelControl11.TabIndex = 22
        Me.LabelControl11.Text = "Link *"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(101, 568)
        Me.MemoEdit1.TabIndex = 3
        '
        'XtraSMSSetting
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraSMSSetting"
        Me.Size = New System.Drawing.Size(1145, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl8.ResumeLayout(False)
        Me.GroupControl8.PerformLayout()
        CType(Me.ToggleMachineWise.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextAllSms2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextAllSms1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitchAllPunch.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl7.ResumeLayout(False)
        Me.GroupControl7.PerformLayout()
        CType(Me.TextOutSMSAfter.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextOutPunchSMSContent2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextOutPunchSMSContent1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleOUTSMS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        Me.GroupControl6.PerformLayout()
        CType(Me.TextInPunchSMSContent2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextInPunchSMSContent1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleINSMS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.TextLateSMSContent2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextLateSMSContent1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleLateSMS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.CheckName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextAbsentSMSContent2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextAbsentSMSContent1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.TextEditSMSTrigerAfterShiftStartTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditPreviousdaySMSTriggerTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.CheckPreviousDay.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckCurrentDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleAbsentSMS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.TextKey.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextSenderId.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextSenderId As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleAbsentSMS As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CheckPreviousDay As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckCurrentDate As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditPreviousdaySMSTriggerTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditSMSTrigerAfterShiftStartTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextAbsentSMSContent2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextAbsentSMSContent1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TextLateSMSContent2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextLateSMSContent1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleLateSMS As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl6 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TextInPunchSMSContent2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextInPunchSMSContent1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleINSMS As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl7 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TextOutPunchSMSContent2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextOutPunchSMSContent1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleOUTSMS As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CheckName As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckDate As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextOutSMSAfter As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl8 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TextAllSms2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextAllSms1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitchAllPunch As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextKey As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents ToggleMachineWise As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl

End Class
