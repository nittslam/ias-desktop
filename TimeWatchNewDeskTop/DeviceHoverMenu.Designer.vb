﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DeviceHoverMenu
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.BtnLogs = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.BtnReal = New DevExpress.XtraEditors.SimpleButton()
        Me.BtnUser = New DevExpress.XtraEditors.SimpleButton()
        Me.ApplicationMenu1 = New DevExpress.XtraBars.Ribbon.ApplicationMenu(Me.components)
        Me.RibbonPage2 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.ApplicationMenu1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BtnLogs
        '
        Me.BtnLogs.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BtnLogs.Appearance.Options.UseFont = True
        Me.BtnLogs.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.BtnLogs.Location = New System.Drawing.Point(5, 5)
        Me.BtnLogs.LookAndFeel.SkinName = "iMaginary"
        Me.BtnLogs.LookAndFeel.UseDefaultLookAndFeel = False
        Me.BtnLogs.Name = "BtnLogs"
        Me.BtnLogs.Size = New System.Drawing.Size(140, 31)
        Me.BtnLogs.TabIndex = 0
        Me.BtnLogs.Text = "Log Management"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.BtnReal)
        Me.PanelControl1.Controls.Add(Me.BtnUser)
        Me.PanelControl1.Controls.Add(Me.BtnLogs)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.LookAndFeel.SkinName = "iMaginary"
        Me.PanelControl1.LookAndFeel.UseDefaultLookAndFeel = False
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(152, 126)
        Me.PanelControl1.TabIndex = 1
        '
        'BtnReal
        '
        Me.BtnReal.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BtnReal.Appearance.Options.UseFont = True
        Me.BtnReal.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.BtnReal.Location = New System.Drawing.Point(5, 79)
        Me.BtnReal.LookAndFeel.SkinName = "iMaginary"
        Me.BtnReal.LookAndFeel.UseDefaultLookAndFeel = False
        Me.BtnReal.Name = "BtnReal"
        Me.BtnReal.Size = New System.Drawing.Size(140, 31)
        Me.BtnReal.TabIndex = 2
        Me.BtnReal.Text = "Real Time"
        '
        'BtnUser
        '
        Me.BtnUser.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BtnUser.Appearance.Options.UseFont = True
        Me.BtnUser.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.BtnUser.Location = New System.Drawing.Point(7, 42)
        Me.BtnUser.LookAndFeel.SkinName = "iMaginary"
        Me.BtnUser.LookAndFeel.UseDefaultLookAndFeel = False
        Me.BtnUser.Name = "BtnUser"
        Me.BtnUser.Size = New System.Drawing.Size(140, 31)
        Me.BtnUser.TabIndex = 1
        Me.BtnUser.Text = "User Management"
        '
        'ApplicationMenu1
        '
        Me.ApplicationMenu1.Name = "ApplicationMenu1"
        '
        'RibbonPage2
        '
        Me.RibbonPage2.Name = "RibbonPage2"
        Me.RibbonPage2.Text = "RibbonPage2"
        '
        'DeviceHoverMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "DeviceHoverMenu"
        Me.Size = New System.Drawing.Size(152, 126)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.ApplicationMenu1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BtnLogs As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents ApplicationMenu1 As DevExpress.XtraBars.Ribbon.ApplicationMenu
    Friend WithEvents RibbonPage2 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents BtnReal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents BtnUser As DevExpress.XtraEditors.SimpleButton

End Class
