﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraVisitorOut
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.TblMachine1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter()
        Me.TblMachineBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblMachineTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter()
        Me.PopupContainerEdit1 = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerControl1 = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colID_NO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLOCATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colbranch = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDeviceType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colA_R = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TextEditSlNo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditInTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditOutTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEditPhone = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.colcommkey = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditSlNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditInTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditOutTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditPhone.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblMachine1TableAdapter1
        '
        Me.TblMachine1TableAdapter1.ClearBeforeFill = True
        '
        'TblMachineBindingSource
        '
        Me.TblMachineBindingSource.DataMember = "tblMachine"
        Me.TblMachineBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblMachineTableAdapter
        '
        Me.TblMachineTableAdapter.ClearBeforeFill = True
        '
        'PopupContainerEdit1
        '
        Me.PopupContainerEdit1.Location = New System.Drawing.Point(117, 76)
        Me.PopupContainerEdit1.Name = "PopupContainerEdit1"
        Me.PopupContainerEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEdit1.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEdit1.Properties.PopupControl = Me.PopupContainerControl1
        Me.PopupContainerEdit1.Size = New System.Drawing.Size(254, 20)
        Me.PopupContainerEdit1.TabIndex = 44
        '
        'PopupContainerControl1
        '
        Me.PopupContainerControl1.Controls.Add(Me.GridControl1)
        Me.PopupContainerControl1.Location = New System.Drawing.Point(6, 102)
        Me.PopupContainerControl1.Name = "PopupContainerControl1"
        Me.PopupContainerControl1.Size = New System.Drawing.Size(340, 175)
        Me.PopupContainerControl1.TabIndex = 45
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.TblMachineBindingSource
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(340, 175)
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID_NO, Me.colLOCATION, Me.colbranch, Me.colDeviceType, Me.colA_R, Me.GridColumn1, Me.colcommkey})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridView1.OptionsSelection.MultiSelect = True
        Me.GridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        '
        'colID_NO
        '
        Me.colID_NO.FieldName = "ID_NO"
        Me.colID_NO.Name = "colID_NO"
        Me.colID_NO.Visible = True
        Me.colID_NO.VisibleIndex = 1
        '
        'colLOCATION
        '
        Me.colLOCATION.FieldName = "LOCATION"
        Me.colLOCATION.Name = "colLOCATION"
        Me.colLOCATION.Visible = True
        Me.colLOCATION.VisibleIndex = 3
        '
        'colbranch
        '
        Me.colbranch.Caption = "GridColumn1"
        Me.colbranch.FieldName = "branch"
        Me.colbranch.Name = "colbranch"
        Me.colbranch.Visible = True
        Me.colbranch.VisibleIndex = 2
        '
        'colDeviceType
        '
        Me.colDeviceType.Caption = "GridColumn1"
        Me.colDeviceType.FieldName = "DeviceType"
        Me.colDeviceType.Name = "colDeviceType"
        '
        'colA_R
        '
        Me.colA_R.FieldName = "A_R"
        Me.colA_R.Name = "colA_R"
        '
        'GridColumn1
        '
        Me.GridColumn1.FieldName = "Purpose"
        Me.GridColumn1.Name = "GridColumn1"
        '
        'TextEditSlNo
        '
        Me.TextEditSlNo.Enabled = False
        Me.TextEditSlNo.Location = New System.Drawing.Point(77, 22)
        Me.TextEditSlNo.Name = "TextEditSlNo"
        Me.TextEditSlNo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditSlNo.Properties.Appearance.Options.UseFont = True
        Me.TextEditSlNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditSlNo.Properties.MaxLength = 25
        Me.TextEditSlNo.Properties.ReadOnly = True
        Me.TextEditSlNo.Size = New System.Drawing.Size(122, 20)
        Me.TextEditSlNo.TabIndex = 47
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(9, 25)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(28, 14)
        Me.LabelControl3.TabIndex = 46
        Me.LabelControl3.Text = "Sl No"
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Location = New System.Drawing.Point(7, 53)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(42, 14)
        Me.LabelControl15.TabIndex = 49
        Me.LabelControl15.Text = "In Time"
        '
        'TextEditInTime
        '
        Me.TextEditInTime.EditValue = "00:10"
        Me.TextEditInTime.Location = New System.Drawing.Point(77, 50)
        Me.TextEditInTime.Name = "TextEditInTime"
        Me.TextEditInTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditInTime.Properties.Appearance.Options.UseFont = True
        Me.TextEditInTime.Properties.Mask.EditMask = "HH:mm"
        Me.TextEditInTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEditInTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditInTime.Properties.MaxLength = 5
        Me.TextEditInTime.Properties.ReadOnly = True
        Me.TextEditInTime.Size = New System.Drawing.Size(72, 20)
        Me.TextEditInTime.TabIndex = 48
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(229, 53)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(52, 14)
        Me.LabelControl1.TabIndex = 51
        Me.LabelControl1.Text = "Out Time"
        '
        'TextEditOutTime
        '
        Me.TextEditOutTime.EditValue = "00:00"
        Me.TextEditOutTime.Location = New System.Drawing.Point(299, 50)
        Me.TextEditOutTime.Name = "TextEditOutTime"
        Me.TextEditOutTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditOutTime.Properties.Appearance.Options.UseFont = True
        Me.TextEditOutTime.Properties.Mask.EditMask = "HH:mm"
        Me.TextEditOutTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEditOutTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditOutTime.Properties.MaxLength = 5
        Me.TextEditOutTime.Size = New System.Drawing.Size(72, 20)
        Me.TextEditOutTime.TabIndex = 50
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(6, 79)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(105, 14)
        Me.LabelControl2.TabIndex = 52
        Me.LabelControl2.Text = "Delete from Device"
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSave.Appearance.Options.UseFont = True
        Me.SimpleButtonSave.Location = New System.Drawing.Point(378, 75)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonSave.TabIndex = 53
        Me.SimpleButtonSave.Text = "Save"
        '
        'TextEditPhone
        '
        Me.TextEditPhone.Enabled = False
        Me.TextEditPhone.Location = New System.Drawing.Point(299, 19)
        Me.TextEditPhone.Name = "TextEditPhone"
        Me.TextEditPhone.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditPhone.Properties.Appearance.Options.UseFont = True
        Me.TextEditPhone.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditPhone.Properties.MaxLength = 25
        Me.TextEditPhone.Properties.ReadOnly = True
        Me.TextEditPhone.Size = New System.Drawing.Size(122, 20)
        Me.TextEditPhone.TabIndex = 54
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(253, 22)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(33, 14)
        Me.LabelControl4.TabIndex = 55
        Me.LabelControl4.Text = "Ph No"
        '
        'colcommkey
        '
        Me.colcommkey.Caption = "GridColumn2"
        Me.colcommkey.FieldName = "commkey"
        Me.colcommkey.Name = "colcommkey"
        '
        'XtraVisitorOut
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(463, 145)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.TextEditPhone)
        Me.Controls.Add(Me.SimpleButtonSave)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.TextEditOutTime)
        Me.Controls.Add(Me.LabelControl15)
        Me.Controls.Add(Me.TextEditInTime)
        Me.Controls.Add(Me.TextEditSlNo)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.PopupContainerControl1)
        Me.Controls.Add(Me.PopupContainerEdit1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraVisitorOut"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditSlNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditInTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditOutTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditPhone.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblMachine1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter
    Friend WithEvents TblMachineBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblMachineTableAdapter As iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter
    Friend WithEvents PopupContainerEdit1 As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents PopupContainerControl1 As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colID_NO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLOCATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colbranch As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDeviceType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colA_R As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TextEditSlNo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditInTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditOutTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEditPhone As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents colcommkey As DevExpress.XtraGrid.Columns.GridColumn
End Class
