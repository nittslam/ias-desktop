﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb

Public Class XtraPayMaintenance
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim leaveFlage As Boolean = True
    Public Sub New()
        InitializeComponent()
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        If Common.servername = "Access" Then
            Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        Else
            TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
        End If
    End Sub

    Private Sub XtraPayMaintenance_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        'If Common.servername = "Access" Then
        '    Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        '    LookUpEdit1.Properties.DataSource = SSSDBDataSet.TblEmployee1
        'Else
        '    TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
        '    Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
        '    LookUpEdit1.Properties.DataSource = SSSDBDataSet.TblEmployee
        'End If
        LookUpEdit1.Properties.DataSource = Common.EmpNonAdmin
        setDefault()
    End Sub
    Private Sub LookUpEdit1_Leave(sender As System.Object, e As System.EventArgs) Handles LookUpEdit1.Leave
        If Not leaveFlage Then Return
        leaveFlage = False
        Dim adap, adap1 As SqlDataAdapter
        Dim adapA, adapA1 As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim ds1 As DataSet
        Dim sSql As String = "select PAYCODE from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
        If LookUpEdit1.EditValue.ToString.Trim <> "" Then
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count = 0 Then
                If LookUpEdit1.EditValue.ToString.Trim <> "" Then
                    XtraMessageBox.Show(ulf, "<size=10>No such Employee</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    LookUpEdit1.Select()
                    setDefault()
                    leaveFlage = True
                    Exit Sub
                End If
            Else
                Dim sSql1 As String = "select EMPNAME, PRESENTCARDNO, (select GroupName from EmployeeGroup where EmployeeGroup.GroupId = TblEmployee.EmployeeGroupId) as EmpGrp, (select DEPARTMENTNAME from tblDepartment where tblDepartment.DEPARTMENTCODE = TblEmployee.DEPARTMENTCODE) as DEPName, (select CATAGORYNAME from tblCatagory WHERE tblCatagory.CAT = TblEmployee.CAT) as CATName, DESIGNATION  from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
                sSql1 = "select EMPNAME, PRESENTCARDNO, " & _
                       "(SELECT COMPANYNAME from tblCompany WHERE tblCompany.COMPANYCODE = TblEmployee.COMPANYCODE) as CompName, " & _
                       "(select GroupName from EmployeeGroup where EmployeeGroup.GroupId = TblEmployee.EmployeeGroupId) as EmpGrp," & _
                       "(select DEPARTMENTNAME from tblDepartment where tblDepartment.DEPARTMENTCODE = TblEmployee.DEPARTMENTCODE) as DEPName, " & _
                       "(select CATAGORYNAME from tblCatagory WHERE tblCatagory.CAT = TblEmployee.CAT) as CATName, " & _
                       "(select GradeName from tblGrade WHERE tblGrade.GradeCode = TblEmployee.GradeCode) as GrdName, DESIGNATION  from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
                If Common.servername = "Access" Then
                    sSql1 = "select T.EMPNAME, T.PRESENTCARDNO, C.COMPANYNAME  as CompName,  T.DESIGNATION, E.GroupName as EmpGrp, D.DEPARTMENTNAME as DEPName, cat.CATAGORYNAME as CATName, grd.GradeName as GrdName  from TblEmployee T,  tblCompany C, EmployeeGroup E, tblDepartment D, tblCatagory cat, tblGrade grd where C.COMPANYCODE = T.COMPANYCODE  and E.GroupId = T.EmployeeGroupId and D.DEPARTMENTCODE = T.DEPARTMENTCODE and cat.CAT = T.CAT and grd.GradeCode = T.GradeCode and T.Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "'"
                    adapA1 = New OleDbDataAdapter(sSql1, Common.con1)
                    ds1 = New DataSet
                    adapA1.Fill(ds1)
                Else
                    adap1 = New SqlDataAdapter(sSql1, Common.con)
                    ds1 = New DataSet
                    adap1.Fill(ds1)
                End If
                lblName.Text = ds1.Tables(0).Rows(0).Item("EMPNAME").ToString
                lblCardNum.Text = ds1.Tables(0).Rows(0).Item("PRESENTCARDNO").ToString
                lblDept.Text = ds1.Tables(0).Rows(0).Item("DEPName").ToString
            End If
        End If
        leaveFlage = True
        'setValues()
    End Sub
    Private Sub setDefault()
        LookUpEdit1.EditValue = ""
        lblName.Text = ""
        lblCardNum.Text = ""
        lblDept.Text = ""
        txtDaysWorked.Text = "00.00"
        txtAbsent.Text = "00.00"
        txtOT.Text = "000.00"
        txtLate.Text = "000.00"
        txtEarly.Text = "000.00"
        txtCL.Text = "00.00"
        txtSL.Text = "00.00"
        txtPLEL.Text = "00.00"
        txtWO.Text = "00.00"
        txtLateDays.Text = "00.00"
        txtEarlyDays.Text = "00.00"
        txtOtherLeave.Text = "00.00"
        txtHolidays.Text = "00.00"
        DateEditFrom.DateTime = Now
        If Common.IsNepali = "Y" Then
            ComboNepaliYear.Visible = True
            ComboNEpaliMonth.Visible = True
            DateEditFrom.Visible = False
            Dim DC As New DateConverter()
            Dim doj As String = DC.ToBS(New Date(DateEditFrom.DateTime.Year, DateEditFrom.DateTime.Month, 1))          
            Dim dojTmp() As String = doj.Split("-")
            ComboNepaliYear.EditValue = dojTmp(0)
            ComboNEpaliMonth.SelectedIndex = dojTmp(1) - 1
           
        Else
            ComboNepaliYear.Visible = False
            ComboNEpaliMonth.Visible = False
            DateEditFrom.Visible = True
        End If
    End Sub
    Private Sub setValues()
        Dim sSql As String
        Dim rsTR As DataSet = New DataSet 'ADODB.Recordset
        Dim iLate As String
        Dim iEarly As String
        Dim iOT As String

        If Common.IsNepali = "Y" Then
            Dim DC As New DateConverter()
            Try
                'DateEditFrom.DateTime = DC.ToAD(New Date(ComboNepaliYear.EditValue, ComboNEpaliMonth.SelectedIndex + 1, 1))
                DateEditFrom.DateTime = DC.ToAD(ComboNepaliYear.EditValue & "-" & ComboNEpaliMonth.SelectedIndex + 1 & "-" & 1)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid Date Of Joining</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNEpaliMonth.Select()
                Exit Sub
            End Try
        End If

        If Common.servername = "Access" Then
            sSql = "Select * From Pay_result" & " Where Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "' And FORMAT(Mon_Year,'MM')=" & DateEditFrom.DateTime.ToString("MM") & " And FORMAT(Mon_Year,'YYYY')=" & DateEditFrom.DateTime.ToString("yyyy")
        Else
            sSql = "Select * From Pay_result" & " Where Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "' And DatePart(mm,Mon_Year)=" & DateEditFrom.DateTime.ToString("MM") & " And DatePart(yy,Mon_Year)=" & DateEditFrom.DateTime.ToString("yyyy")
        End If
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsTR)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsTR)
        End If
        'rsTR = Cn.Execute(sSql)
        If rsTR.Tables(0).Rows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>No attendance data is available for this employee for the specified period.</size>", "<size=9>Error</size>")
            setDefault()
            LookUpEdit1.Select()
            Exit Sub
        Else
            If rsTR.Tables(0).Rows(0).Item("VPRE").ToString.Trim = "" Then : txtDaysWorked.Text = "00.00" : Else : txtDaysWorked.Text = rsTR.Tables(0).Rows(0).Item("VPRE").ToString.Trim : End If
            'txtAbsent.Text = IIf(IsNull(rsTR("VABS")), "00.00", Format(rsTR("VABS"), "00.00"))
            If rsTR.Tables(0).Rows(0).Item("VABS").ToString.Trim = "" Then : txtAbsent.Text = "00.00" : Else : txtAbsent.Text = rsTR.Tables(0).Rows(0).Item("VABS").ToString.Trim : End If
            'iOT = Min2Hr(rsTR("VOT"))
            iOT = rsTR.Tables(0).Rows(0).Item("VOT").ToString.Trim
            'txtOT.Text = IIf(IsNull(iOT), "000.00", Format(iOT, "000.00"))
            If iOT = "" Then : txtOT.Text = "000.00" : Else : txtOT.Text = iOT : End If

            iLate = (rsTR.Tables(0).Rows(0).Item("VT_LATE") / 60) 'Min2Hr(rsTR("VT_LATE"))
            'txtLate.Text = IIf(IsNull(iLate), "000.00", Format(iLate, "000.00"))
            If iLate = "" Then : txtLate.Text = "000.00" : Else : txtLate.Text = iLate : End If

            iEarly = (rsTR.Tables(0).Rows(0).Item("VT_EARLY") / 60) ' Min2Hr(rsTR("VT_EARLY"))
            'txtEarly.Text = IIf(IsNull(iEarly), "000.00", Format(iEarly, "000.00"))
            If iEarly = "" Then : txtEarly.Text = "000.00" : Else : txtEarly.Text = iEarly : End If

            'txtCL.Text = IIf(IsNull(rsTR("VCL")), "00.00", Format(rsTR("VCL"), "00.00"))
            If rsTR.Tables(0).Rows(0).Item("VCL").ToString.Trim = "" Then : txtCL.Text = "00.00" : Else : txtCL.Text = rsTR.Tables(0).Rows(0).Item("VCL").ToString.Trim : End If

            'txtSL.Text = IIf(IsNull(rsTR("VSL")), "00.00", Format(rsTR("VSL"), "00.00"))
            If rsTR.Tables(0).Rows(0).Item("VSL").ToString.Trim = "" Then : txtSL.Text = "00.00" : Else : txtSL.Text = rsTR.Tables(0).Rows(0).Item("VSL").ToString.Trim : End If

            'txtPLEL.Text = IIf(IsNull(rsTR("VPL_EL")), "00.00", Format(rsTR("VPL_EL"), "00.00"))
            If rsTR.Tables(0).Rows(0).Item("VPL_EL").ToString.Trim = "" Then : txtPLEL.Text = "00.00" : Else : txtPLEL.Text = rsTR.Tables(0).Rows(0).Item("VPL_EL").ToString.Trim : End If

            'txtWO.Text = IIf(IsNull(rsTR("VWO")), "00.00", Format(rsTR("VWO"), "00.00"))
            If rsTR.Tables(0).Rows(0).Item("VWO").ToString.Trim = "" Then : txtWO.Text = "00.00" : Else : txtWO.Text = rsTR.Tables(0).Rows(0).Item("VWO").ToString.Trim : End If

            'txtLateDays.Text = IIf(IsNull(rsTR("VLATE")), "00.00", Format(rsTR("VLATE"), "00.00"))
            If rsTR.Tables(0).Rows(0).Item("VLATE").ToString.Trim = "" Then : txtLateDays.Text = "00.00" : Else : txtLateDays.Text = rsTR.Tables(0).Rows(0).Item("VLATE").ToString.Trim : End If

            'txtEarlyDays.Text = IIf(IsNull(rsTR("VEARLY")), "00.00", Format(rsTR("VEARLY"), "00.00"))
            If rsTR.Tables(0).Rows(0).Item("VEARLY").ToString.Trim = "" Then : txtEarlyDays.Text = "00.00" : Else : txtEarlyDays.Text = rsTR.Tables(0).Rows(0).Item("VEARLY").ToString.Trim : End If

            'txtOtherLeave.Text = IIf(IsNull(rsTR("VOTHER_LV")), "00.00", Format(rsTR("VOTHER_LV"), "00.00"))
            If rsTR.Tables(0).Rows(0).Item("VOTHER_LV").ToString.Trim = "" Then : txtOtherLeave.Text = "00.00" : Else : txtOtherLeave.Text = rsTR.Tables(0).Rows(0).Item("VOTHER_LV").ToString.Trim : End If

            'txtHolidays.Text = IIf(IsNull(rsTR("VHLD")), "00.00", Format(rsTR("VHLD"), "00.00"))
            If rsTR.Tables(0).Rows(0).Item("VHLD").ToString.Trim = "" Then : txtHolidays.Text = "00.00" : Else : txtHolidays.Text = rsTR.Tables(0).Rows(0).Item("VHLD").ToString.Trim : End If

        End If
    End Sub

    Private Sub SimpleButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSave.Click
        Dim sSql As String
        'Dim i As Integer
        Try ' On Error GoTo AKHIL
            If Common.IsNepali = "Y" Then
                Dim DC As New DateConverter()
                Try
                    'DateEditFrom.DateTime = DC.ToAD(New Date(ComboNepaliYear.EditValue, ComboNEpaliMonth.SelectedIndex + 1, 1))
                    DateEditFrom.DateTime = DC.ToAD(ComboNepaliYear.EditValue & "-" & ComboNEpaliMonth.SelectedIndex + 1 & "-" & 1)
                Catch ex As Exception
                    XtraMessageBox.Show(ulf, "<size=10>Invalid Date Of Joining</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    ComboNEpaliMonth.Select()
                    Exit Sub
                End Try
            End If

            If Val(txtDaysWorked.Text) + Val(txtAbsent.Text) + Val(txtCL.Text) + Val(txtSL.Text) + Val(txtPLEL.Text) + Val(txtOtherLeave.Text) + Val(txtHolidays.Text) + Val(txtWO.Text) > Date.DaysInMonth(DateEditFrom.DateTime.Year, DateEditFrom.DateTime.Month) Then
                XtraMessageBox.Show(ulf, "<size=10>Total days cannot be greater than the Month Days. Please check</size>", "<size=9>Total days error</size>")
                txtDaysWorked.Select()
                Exit Sub
            End If
            sSql = "select * from PAY_RESULT where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "' And MONTH(Mon_Year)=" & DateEditFrom.DateTime.ToString("MM") & " And YEAR(Mon_Year)=" & DateEditFrom.DateTime.ToString("yyyy")
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            'rsChkdata = Cn.Execute(sSql)
            Dim ds As DataSet = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count = 0 Then
                If Common.IsNepali = "Y" Then
                    XtraMessageBox.Show(ulf, "<size=10>Data not update becouse data not capture on " & ComboNEpaliMonth.EditValue.ToString.Trim & " " & ComboNepaliYear.EditValue.ToString.Trim & "</size>", "<size=9>Error</size>")
                Else
                    XtraMessageBox.Show(ulf, "<size=10>Data not update becouse data not capture on " & DateEditFrom.DateTime.ToString("MMM yyyy") & "</size>", "<size=9>Error</size>")
                End If

                LookUpEdit1.Select()
                setDefault()
                Exit Sub
            End If
            'If MsgBox("Do you want to save changes", vbInformation + vbYesNo) = vbYes Then
            If XtraMessageBox.Show(ulf, "<size=10>Do you want to save changes?</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                          MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                If Common.servername = "Access" Then
                    sSql = "Update pay_result Set VPRE=" & Trim(txtDaysWorked.Text) & "," & _
                    "VABS=" & Trim(txtAbsent.Text) & ",VOT=" & Trim(txtOT.Text) & "," & _
                    "VT_LATE=" & Trim(txtLate.Text) & ",VT_EARLY=" & Trim(txtEarly.Text) & "," & _
                    "VCL=" & Trim(txtCL.Text) & ",VSL=" & Trim(txtSL.Text) & ",VWO=" & Trim(txtWO.Text) & "," & _
                    "VPL_EL=" & Trim(txtPLEL.Text) & ",VLATE=" & Trim(txtLateDays.Text) & "," & _
                    "VEARLY=" & Trim(txtEarlyDays.Text) & ",VOTHER_LV=" & Trim(txtOtherLeave.Text) & "," & _
                    "VHLD=" & Trim(txtHolidays.Text) & " Where Paycode='" & Trim(LookUpEdit1.EditValue.ToString.Trim) & "' And MONTH(Mon_Year)=" & DateEditFrom.DateTime.ToString("MM") & " And YEAR(Mon_Year)=" & DateEditFrom.DateTime.ToString("yyyy")
                Else
                    sSql = "Update pay_result Set VPRE=" & Trim(txtDaysWorked.Text) & "," & _
                    "VABS=" & Trim(txtAbsent.Text) & ",VOT=" & Trim(txtOT.Text) & "," & _
                    "VT_LATE=" & Trim(txtLate.Text) & ",VT_EARLY=" & Trim(txtEarly.Text) & "," & _
                    "VCL=" & Trim(txtCL.Text) & ",VSL=" & Trim(txtSL.Text) & ",VWO=" & Trim(txtWO.Text) & "," & _
                    "VPL_EL=" & Trim(txtPLEL.Text) & ",VLATE=" & Trim(txtLateDays.Text) & "," & _
                    "VEARLY=" & Trim(txtEarlyDays.Text) & ",VOTHER_LV=" & Trim(txtOtherLeave.Text) & "," & _
                    "VHLD=" & Trim(txtHolidays.Text) & " Where Paycode='" & Trim(LookUpEdit1.EditValue.ToString.Trim) & "' And DatePart(mm,Mon_Year)=" & DateEditFrom.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & DateEditFrom.DateTime.ToString("yyyy")


                End If
                'Cn.Execute (sSql), i
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
                XtraMessageBox.Show(ulf, "<size=10>Saved Successfully</size>", "<size=9>Success</size>")
                LookUpEdit1.Select()
                setDefault()
            End If
        Catch
            XtraMessageBox.Show(ulf, "<size=10> " & Err.Number & Chr(13) & Err.Description & "</size>", "<size=9>Error</size>")
        End Try
       
    End Sub

    Private Sub DateEditFrom_Leave(sender As System.Object, e As System.EventArgs) Handles DateEditFrom.Leave
        setValues()
    End Sub

    Private Sub ComboNepaliYear_Leave(sender As System.Object, e As System.EventArgs) Handles ComboNepaliYear.Leave
        setValues()
    End Sub
End Class
