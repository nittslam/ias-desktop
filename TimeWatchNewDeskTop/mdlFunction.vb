Option Strict Off
Option Explicit On
Imports System.Runtime.InteropServices
Imports FK623ATTENDOCXLib
Imports iAS

Module mdlPublic
   
    Public gfrmMain As XtraCommunicationForm
    Public Const gstrNoDevice As String = "No Device"

    '// ========== FingerKeeper Interface Functions =========='
    '// -- connect, disconnect

    Public Declare Ansi Function FK_ConnectComm Lib "FK623Attend" ( _
        ByVal nMachineNo As Int32, _
        ByVal nComPort As Int32, _
        ByVal nBaudRate As Int32, _
        ByVal strTelNumber As String, _
        ByVal nWaitDialTime As Int32, _
        ByVal nLicense As Int32, _
        ByVal nComTimeOut As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_ConnectNet Lib "FK623Attend" ( _
        ByVal nMachineNo As Int32, _
        ByVal strIpAddress As String, _
        ByVal nNetPort As Int32, _
        ByVal nTimeOut As Int32, _
        ByVal nProtocolType As Int32, _
        ByVal nNetPassword As Int32, _
        ByVal nLicense As Int32 _
    ) As Int32

    ''<DllImport("FK623Attend.dll", CharSet:=CharSet.Ansi)>
    'Public Declare Function FK_ConnectNet Lib "FK623Attend" (ByVal anMachineNo As Integer, ByVal astrIpAddress As String, ByVal anNetPort As Integer, ByVal anTimeOut As Integer, ByVal anProtocolType As Integer, ByVal anNetPassword As Integer, ByVal anLicense As Integer) As Integer

    Public Declare Ansi Function FK_ConnectUSB Lib "FK623Attend" ( _
        ByVal nMachineNo As Int32, _
        ByVal nLicense As Int32 _
    ) As Int32

    Public Declare Ansi Sub FK_DisConnect Lib "FK623Attend" (ByVal nHandleIndex As Int32)

    '///-- device setting
    Public Declare Ansi Function FK_EnableDevice Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnableFlag As Int32 _
    ) As Int32

    Public Declare Ansi Sub FK_PowerOnAllDevice Lib "FK623Attend" (ByVal nHandleIndex As Int32)

    Public Declare Ansi Function FK_PowerOffDevice Lib "FK623Attend" (ByVal nHandleIndex As Int32) As Int32

    Public Declare Ansi Function FK_GetDeviceStatus Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nStatusIndex As Int32, _
        ByRef nValue As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_GetDeviceTime Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByRef pnDateTime As Date _
    ) As Int32

    Public Declare Ansi Function FK_SetDeviceTime Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nDateTime As Date _
    ) As Int32

    Public Declare Ansi Function FK_GetDeviceInfo Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nInfoIndex As Int32, _
        ByRef pnValue As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetDeviceInfo Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nInfoIndex As Int32, _
        ByVal nValue As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_GetProductData Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nDataIndex As Int32, _
        ByRef strValue As String _
    ) As Int32

    '// -- log data
    Public Declare Ansi Function FK_LoadSuperLogData Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nReadMark As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_USBLoadSuperLogDataFromFile Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strFilePath As String _
    ) As Int32

    Public Declare Ansi Function FK_GetSuperLogData Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByRef nSEnrollNumber As UInt32, _
        ByRef nGEnrollNumber As UInt32, _
        ByRef nManipulation As Int32, _
        ByRef nBackupNumber As Int32, _
        ByRef dtLogTime As Date _
    ) As Int32

    Public Declare Ansi Function FK_EmptySuperLogData Lib "FK623Attend" (ByVal nHandleIndex As Int32) As Int32

    Public Declare Ansi Function FK_LoadGeneralLogData Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nReadMark As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_USBLoadGeneralLogDataFromFile Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strFilePath As String _
    ) As Int32

    Public Declare Ansi Function FK_GetGeneralLogData Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByRef nEnrollNumber As UInt32, _
        ByRef nVerifyMode As Int32, _
        ByRef nInOutMode As Int32, _
        ByRef dtLogTime As Date _
    ) As Int32

    Public Declare Ansi Function FK_EmptyGeneralLogData Lib "FK623Attend" (ByVal nHandleIndex As Int32) As Int32

    '// -- enroll data
    Public Declare Ansi Function FK_ReadAllUserID Lib "FK623Attend" (ByVal nHandleIndex As Int32) As Int32

    Public Declare Ansi Function FK_GetAllUserID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByRef nEnrollNumber As UInt32, _
        ByRef nBackupNumber As Int32, _
        ByRef nMachinePrivilege As Int32, _
        ByRef nEnable As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_GetEnrollData Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByVal nBackupNumber As Int32, _
        ByRef nMachinePrivilege As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytEnrollData() As Byte, _
        ByRef nPassword As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_PutEnrollData Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByVal nBackupNumber As Int32, _
        ByVal nMachinePrivilege As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytEnrollData() As Byte, _
        ByVal nPassword As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SaveEnrollData Lib "FK623Attend" (ByVal nHandleIndex As Int32) As Int32

    Public Declare Ansi Function FK_DeleteEnrollData Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByVal nBackupNumber As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_EnableUser Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByVal nBackupNumber As Int32, _
        ByVal nEnableFlag As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_ModifyPrivilege Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByVal nBackupNumber As Int32, _
        ByVal nMachinePrivilege As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_BenumbAllManager Lib "FK623Attend" (ByVal nHandleIndex As Int32) As Int32

    Public Declare Ansi Function FK_EmptyEnrollData Lib "FK623Attend" (ByVal nHandleIndex As Int32) As Int32

    Public Declare Ansi Function FK_ClearKeeperData Lib "FK623Attend" (ByVal nHandleIndex As Int32) As Int32


    '// -- enroll data using u-disk file
    Public Declare Ansi Function FK_SetUDiskFileFKModel Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strFKModel As String _
    ) As Int32
    '// old deprecated function
    Public Declare Ansi Function FK_SetUSBModel Lib "FK623Attend" ( _
        ByVal nHandleIndex As Integer, _
        ByVal anModel As Integer _
    ) As Int32


    Public Declare Ansi Function FK_USBReadAllEnrollDataFromFile Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strFilePath As String _
    ) As Int32

    Public Declare Ansi Function FK_USBReadAllEnrollDataCount Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal pnValue As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_USBGetOneEnrollData Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByRef nEnrollNumber As UInt32, _
        ByRef nBackupNumber As Int32, _
        ByRef nMachinePrivilege As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytEnrollData() As Byte, _
        ByRef nPassWord As Int32, _
        ByRef nEnableFlag As Int32, _
        ByRef strEnrollName As String _
    ) As Int32

    Public Declare Ansi Function FK_USBSetOneEnrollData Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByVal nBackupNumber As Int32, _
        ByVal nMachinePrivilege As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytEnrollData() As Byte, _
        ByVal nPassWord As Int32, _
        ByVal nEnableFlag As Int32, _
        ByVal strEnrollName As String _
    ) As Int32

    Public Declare Ansi Function FK_USBWriteAllEnrollDataToFile Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strFilePath As String _
    ) As Int32

    '// -- bell info
    Public Declare Ansi Function FK_GetBellTime Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByRef nBellCount As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytBellInfo() As Byte _
    ) As Int32

    Public Declare Ansi Function FK_SetBellTime Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nBellCount As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytBellInfo() As Byte _
    ) As Int32

    '// -- user name, message
    Public Declare Ansi Function FK_GetUserName Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByRef strUserName As String _
    ) As Int32

    Public Declare Ansi Function FK_SetUserName Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByVal strUserName As String _
    ) As Int32

    Public Declare Ansi Function FK_GetNewsMessage Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nNewsId As Int32, _
        ByRef strNews As String _
    ) As Int32

    Public Declare Ansi Function FK_SetNewsMessage Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nNewsId As Int32, _
        ByVal strNews As String _
    ) As Int32

    Public Declare Ansi Function FK_GetUserNewsID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByRef nNewsId As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetUserNewsID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByVal nNewsId As Int32 _
    ) As Int32

    '// -- access control
    Public Declare Ansi Function FK_GetDoorStatus Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByRef nStatusVal As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetDoorStatus Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nStatusVal As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_GetPassTime Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nPassTimeID As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytPassTimeInfo() As Byte, _
        ByVal PassTimeInfoSize As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetPassTime Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nPassTimeID As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytPassTimeInfo() As Byte, _
        ByVal PassTimeInfoSize As Int32 _
    ) As Int32


    Public Declare Ansi Function FK_GetUserPassTime Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByRef nGroupID As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytUserPassTimeInfo() As Byte, _
        ByVal nUserPassTimeInfoSize As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetUserPassTime Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByVal nGroupID As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytUserPassTimeInfo() As Byte, _
        ByVal nUserPassTimeInfoSize As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_GetGroupPassTime Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nGroupID As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytGroupPassTimeInfo() As Byte, _
        ByVal nGroupPassTimeInfoSize As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetGroupPassTime Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nGroupID As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytGroupPassTimeInfo() As Byte, _
        ByVal nGroupPassTimeInfoSize As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_GetGroupMatch Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytGroupMatchInfo() As Byte, _
        ByVal anGroupMatchInfoSize As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetGroupMatch Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytGroupMatchInfo() As Byte, _
        ByVal nGroupMatchInfoSize As Int32 _
    ) As Int32

    '// -- etc
    Public Declare Ansi Function FK_ConnectGetIP Lib "FK623Attend" (ByRef apnComName As String) As Long
    Public Declare Ansi Function FK_GetAdjustInfo Lib "FK623Attend" (ByVal nHandleIndex As Int32, ByRef dwAdjustedState As Int32, ByRef dwAdjustedMonth As Int32, ByRef dwAdjustedDay As Int32, ByRef dwAdjustedHour As Int32, ByRef dwAdjustedMinute As Int32, ByRef dwRestoredState As Int32, ByRef dwRestoredMonth As Int32, ByRef dwRestoredDay As Int32, ByRef dwRestoredHour As Int32, ByRef dwRestoredMinte As Int32) As Int32
    Public Declare Ansi Function FK_SetAdjustInfo Lib "FK623Attend" (ByVal nHandleIndex As Int32, ByVal dwAdjustedState As Int32, ByVal dwAdjustedMonth As Int32, ByVal dwAdjustedDay As Int32, ByVal dwAdjustedHour As Int32, ByVal dwAdjustedMinute As Int32, ByVal dwRestoredState As Int32, ByVal dwRestoredMonth As Int32, ByVal dwRestoredDay As Int32, ByVal dwRestoredHour As Int32, ByVal dwRestoredMinte As Int32) As Int32

    Public Const NEWS_EXTEND As Short = 2
    Public Const NEWS_STANDARD As Short = 1

    Public Declare Ansi Function FK_GetAccessTime Lib "FK623Attend" (ByVal nHandleIndex As Integer, ByVal anEnrollNumber As Integer, ByRef apnAccessTime As Integer) As Integer
    Public Declare Ansi Function FK_SetAccessTime Lib "FK623Attend" (ByVal nHandleIndex As Integer, ByVal anEnrollNumber As Integer, ByVal anAccessTime As Integer) As Integer
    Public Declare Ansi Function FK_SetFontName Lib "FK623Attend" (ByVal nHandleIndex As Integer, ByVal aStrFontName As String, ByVal anFontType As Integer) As Integer
    Public Declare Ansi Function FK_GetRealTimeInfo Lib "FK623Attend" (ByVal nHandleIndex As Integer, ByRef apGetRealTime As Integer) As Integer
    Public Declare Ansi Function FK_SetRealTimeInfo Lib "FK623Attend" (ByVal nHandleIndex As Integer, ByRef apSetRealTime As Integer) As Integer

    Public Declare Ansi Function FK_GetServerNetInfo Lib "FK623Attend" (ByVal nHandleIndex As Integer, ByRef astrServerIPAddress As String, ByRef apServerPort As Integer, ByRef apServerRequest As Integer) As Integer
    Public Declare Ansi Function FK_SetServerNetInfo Lib "FK623Attend" (ByVal nHandleIndex As Integer, ByVal astrServerIPAddress As String, ByVal anServerPort As Integer, ByVal apServerReques As Integer) As Integer


    '// -- Post & Shift Info (HTML version)
    Public Declare Ansi Function FK_GetOneShiftInfo Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anShiftNumber As Int32, _
        ByRef apShiftSHour As Int32, _
        ByRef apShiftSMinute As Int32, _
        ByRef apShiftEHour As Int32, _
        ByRef apShiftEMinute As Int32, _
        ByRef apstrShiftName As String _
    ) As Int32

    Public Declare Ansi Function FK_SetOneShiftInfo Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anShiftNumber As Int32, _
        ByVal anShiftSHour As Int32, _
        ByVal anShiftSMinute As Int32, _
        ByVal anShiftEHour As Int32, _
        ByVal anShiftEMinute As Int32, _
        ByVal astrShiftName As String _
    ) As Int32

    Public Declare Ansi Function FK_GetOnePostInfo Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anPostNumber As Int32, _
        ByRef apstrPostName As Int32, _
        ByRef apShiftNumber1 As Int32, _
        ByRef apShiftNumber2 As Int32, _
        ByRef apShiftNumber3 As Int32, _
        ByRef apShiftNumber4 As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetOnePostInfo Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anPostNumber As Int32, _
        ByVal astrPostName As String, _
        ByVal anShiftNumber1 As Int32, _
        ByVal anShiftNumber2 As Int32, _
        ByVal anShiftNumber3 As Int32, _
        ByVal anShiftNumber4 As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_GetUserInfo Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anEnrollNumber As UInt32, _
        ByRef apstrUserName As String, _
        ByRef apNewKind As Int32, _
        ByRef apVerifyMode As Int32, _
        ByRef apPostID As Int32, _
        ByRef apShiftNumber1 As Int32, _
        ByRef apShiftNumber2 As Int32, _
        ByRef apShiftNumber3 As Int32, _
        ByRef apShiftNumber4 As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetUserInfo Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anEnrollNumber As UInt32, _
        ByVal astrUserName As String, _
        ByVal anNewKind As Int32, _
        ByVal anVerifyMode As Int32, _
        ByVal anPostID As Int32, _
        ByVal anShiftNumber1 As Int32, _
        ByVal anShiftNumber2 As Int32, _
        ByVal anShiftNumber3 As Int32, _
        ByVal anShiftNumber4 As Int32 _
    ) As Int32

    '// -- Post & Shift Info (EXCEL version)
    Public Declare Ansi Function FK_GetPostShiftInfo Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytPostShiftInfo() As Byte, _
        ByRef apPostShiftInfoLen As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetPostShiftInfo Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytPostShiftInfo() As Byte, _
        ByVal anPostShiftInfoLen As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_GetUserInfoEx Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anEnrollNumber As UInt32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytUserInfo() As Byte, _
        ByRef apUserInfoLen As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetUserInfoEx Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anEnrollNumber As UInt32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytUserInfo() As Byte, _
        ByVal anUserInfoLen As Int32 _
    ) As Int32

    '// -- Photo transfer
    Public Declare Ansi Function FK_GetEnrollPhoto Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anEnrollNumber As UInt32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytPhotoImage() As Byte, _
        ByRef anPhotoLength As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetEnrollPhoto Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anEnrollNumber As UInt32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytPhotoImage() As Byte, _
        ByVal anPhotoLength As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_DeleteEnrollPhoto Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anEnrollNumber As UInt32 _
    ) As Int32

    Public Declare Ansi Function FK_GetLogPhoto Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anEnrollNumber As UInt32, _
        ByVal anYear As Int32, _
        ByVal anMonth As Int32, _
        ByVal anDay As Int32, _
        ByVal anHour As Int32, _
        ByVal anMinute As Int32, _
        ByVal anSec As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytPhotoImage() As Byte, _
        ByRef anPhotoLength As Int32 _
    ) As Int32

    '// Get supported flag of specific enroll data type
    Public Declare Ansi Function FK_IsSupportedEnrollData Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anBackupNumber As Int32, _
        ByRef anSupportFlag As Int32 _
    ) As Int32


    '{ Access Control(Haoshun FK) function
    Public Declare Ansi Function FK_HS_GetTimeZone Lib "FK623Attend" ( _
        ByVal anHandleIndex As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytOneTimeZone() As Byte _
        ) As Int32
    Public Declare Ansi Function FK_HS_SetTimeZone Lib "FK623Attend" ( _
        ByVal anHandleIndex As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytOneTimeZone() As Byte _
        ) As Int32
    Public Declare Ansi Function FK_HS_GetUserWeekPassTime Lib "FK623Attend" ( _
        ByVal anHandleIndex As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytUserWeekPassTime() As Byte _
        ) As Int32
    Public Declare Ansi Function FK_HS_SetUserWeekPassTime Lib "FK623Attend" ( _
        ByVal anHandleIndex As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytUserWeekPassTime() As Byte _
        ) As Int32
    '}

    '// All purpose
    Public Declare Ansi Function FK_ExecCommand Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal astrCommand As String, _
        ByRef astrResult As String _
    ) As Int32



    Public Declare Ansi Function FK_GetIsSupportStringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32 _
    ) As Int32





    Public Declare Ansi Function FK_GetLogDataIsSupportStringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32 _
    ) As Int32
    Public Declare Ansi Function FK_GetUSBEnrollDataIsSupportStringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_GetSuperLogData_StringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByRef strSEnrollNumber As String, _
        ByRef strGEnrollNumber As String, _
        ByRef nManipulation As Int32, _
        ByRef nBackupNumber As Int32, _
        ByRef dtLogTime As Date _
     ) As Int32


    Public Declare Ansi Function FK_GetGeneralLogData_StringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByRef strEnrollNumber As String, _
        ByRef nVerifyMode As Int32, _
        ByRef nInOutMode As Int32, _
        ByRef dtLogTime As Date _
     ) As Int32


    Public Declare Ansi Function FK_EnableUser_StringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strEnrollNumber As String, _
        ByVal nBackupNumber As Int32, _
        ByVal nEnableFlag As Int32 _
    ) As Int32


    Public Declare Ansi Function FK_ModifyPrivilege_StringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strEnrollNumber As String, _
        ByVal nBackupNumber As Int32, _
        ByVal nMachinePrivilege As Int32 _
     ) As Int32


    Public Declare Ansi Function FK_GetAllUserID_StringID Lib "FK623Attend" ( _
       ByVal nHandleIndex As Int32, _
       ByRef strEnrollNumber As String, _
       ByRef nBackupNumber As Int32, _
       ByRef nMachinePrivilege As Int32, _
       ByRef nEnable As Int32 _
   ) As Int32

    Public Declare Ansi Function FK_GetEnrollData_StringID Lib "FK623Attend" ( _
       ByVal nHandleIndex As Int32, _
       ByVal strEnrollNumber As String, _
       ByVal nBackupNumber As Int32, _
       ByRef nMachinePrivilege As Int32, _
       <MarshalAs(UnmanagedType.LPArray)> ByVal bytEnrollData() As Byte, _
       ByRef nPassword As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_PutEnrollData_StringID Lib "FK623Attend" ( _
       ByVal nHandleIndex As Int32, _
       ByVal strEnrollNumber As String, _
       ByVal nBackupNumber As Int32, _
       ByVal nMachinePrivilege As Int32, _
       <MarshalAs(UnmanagedType.LPArray)> ByVal bytEnrollData() As Byte, _
       ByVal nPassword As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_DeleteEnrollData_StringID Lib "FK623Attend" ( _
       ByVal nHandleIndex As Int32, _
       ByVal strEnrollNumber As String, _
       ByVal nBackupNumber As Int32 _
    ) As Int32



    Public Declare Ansi Function FK_GetUserName_StringID Lib "FK623Attend" ( _
      ByVal nHandleIndex As Int32, _
      ByVal strEnrollNumber As String, _
      ByRef strUserName As String _
    ) As Int32


    Public Declare Ansi Function FK_SetUserName_StringID Lib "FK623Attend" ( _
      ByVal nHandleIndex As Int32, _
      ByVal strEnrollNumber As String, _
      ByVal strUserName As String _
    ) As Int32


    Public Declare Ansi Function FK_GetUserPassTime_StringID Lib "FK623Attend" ( _
      ByVal nHandleIndex As Int32, _
      ByVal strEnrollNumber As String, _
      ByRef nGroupID As Int32, _
      <MarshalAs(UnmanagedType.LPArray)> ByVal bytUserPassTimeInfo() As Byte, _
      ByVal nUserPassTimeInfoSize As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetUserPassTime_StringID Lib "FK623Attend" ( _
      ByVal nHandleIndex As Int32, _
      ByVal strEnrollNumber As String, _
      ByVal nGroupID As Int32, _
      <MarshalAs(UnmanagedType.LPArray)> ByVal bytUserPassTimeInfo() As Byte, _
      ByVal nUserPassTimeInfoSize As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_USBGetOneEnrollData_StringID Lib "FK623Attend" ( _
      ByVal nHandleIndex As Int32, _
      ByRef strEnrollNumber As String, _
      ByRef nBackupNumber As Int32, _
      ByRef nMachinePrivilege As Int32, _
      <MarshalAs(UnmanagedType.LPArray)> ByVal bytEnrollData() As Byte, _
      ByRef nPassWord As Int32, _
      ByRef nEnableFlag As Int32, _
      ByRef strEnrollName As String _
    ) As Int32

    Public Declare Ansi Function FK_USBSetOneEnrollData_StringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strEnrollNumber As String, _
        ByVal nBackupNumber As Int32, _
        ByVal nMachinePrivilege As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytEnrollData() As Byte, _
        ByVal nPassWord As Int32, _
        ByVal nEnableFlag As Int32, _
        ByVal strEnrollName As String _
     ) As Int32


    Public Declare Ansi Function FK_GetUserInfoEx_StringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strEnrollNumber As String, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytUserInfo() As Byte, _
        ByRef apUserInfoLen As Int32 _
     ) As Int32

    Public Declare Ansi Function FK_SetUserInfoEx_StringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strEnrollNumber As String, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytUserInfo() As Byte, _
        ByVal anUserInfoLen As Int32 _
     ) As Int32


    Public Declare Ansi Function FK_GetEnrollPhoto_StringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strEnrollNumber As String, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytPhotoImage() As Byte, _
        ByRef anPhotoLength As Int32 _
     ) As Int32


    Public Declare Ansi Function FK_SetEnrollPhoto_StringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strEnrollNumber As String, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytPhotoImage() As Byte, _
        ByVal anPhotoLength As Int32 _
     ) As Int32


    Public Declare Ansi Function FK_DeleteEnrollPhoto_StringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strEnrollNumber As String _
     ) As Int32


    Public Declare Ansi Function FK_GetLogPhoto_StringID Lib "FK623Attend" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strEnrollNumber As String, _
        ByVal anYear As Int32, _
        ByVal anMonth As Int32, _
        ByVal anDay As Int32, _
        ByVal anHour As Int32, _
        ByVal anMinute As Int32, _
        ByVal anSec As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytPhotoImage() As Byte, _
        ByRef anPhotoLength As Int32 _
    ) As Int32



    'nitin for finger validity
    '    [DllImport("FK623Attend.dll", CharSet = CharSet.Ansi)]
    'public static extern int FK_ExtCommand(
    '    int anHandleIndex,
    '    byte[] abytCmdStruct);
    Public Declare Ansi Function FK_ExtCommand Lib "FK623Attend" ( _
    ByVal anHandleIndex As Integer,
    ByVal abytCmdStruct() As Byte
   ) As Int32
    'nitin

    '/******************************************************************/
    '/*                            Structure                           */
    '/******************************************************************/
    '   2010.02.01
    Public Const MAX_REAL_TIME As Short = 4
    ' Insert 2010.06.08
    Public Const FK625_FP1000 As Integer = 2001
    Public Const FK625_FP2000 As Integer = 2002
    Public Const FK625_FP3000 As Integer = 2003
    Public Const FK625_FP5000 As Integer = 2004
    Public Const FK625_FP10000 As Integer = 2005
    Public Const FK625_FP30000 As Integer = 2006
    Public Const FK625_ID30000 As Integer = 2007
    Public Const FK635_FP700 As Integer = 3001
    Public Const FK635_FP3000 As Integer = 3002
    Public Const FK635_FP10000 As Integer = 3003
    Public Const FK635_ID30000 As Integer = 3004
    Public Const FK723_FP1000 As Integer = 4001
    Public Const FK725_FP1000 As Integer = 5001
    Public Const FK725_FP1500 As Integer = 5002
    Public Const FK725_ID5000 As Integer = 5003
    Public Const FK725_ID30000 As Integer = 5004
    Public Const FK735_FP500 As Integer = 6001
    Public Const FK735_FP3000 As Integer = 6002
    Public Const FK735_ID30000 As Integer = 6003
    Public Const FK925_FP3000 As Integer = 7001
    Public Const FK935_FP3000 As Integer = 8001

    Structure REALTIMEINFO
        Dim Valid As Byte
        Dim AckTime As Byte
        Dim WaitTime As Byte
        Dim Reserve As Byte
        Dim SendPos As Integer
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_REAL_TIME)> Dim Hour_Renamed() As Byte
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_REAL_TIME)> Dim Minute_Renamed() As Byte
    End Structure ' 16 Byte

    Public Const MAX_BELLCOUNT_DAY As Short = 24
    Structure BELLINFO
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_BELLCOUNT_DAY)> Dim mValid() As Byte
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_BELLCOUNT_DAY)> Dim mHour() As Byte
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_BELLCOUNT_DAY)> Dim mMinute() As Byte

        Public Sub Initialize()
            ReDim mValid(MAX_BELLCOUNT_DAY - 1)
            ReDim mHour(MAX_BELLCOUNT_DAY - 1)
            ReDim mMinute(MAX_BELLCOUNT_DAY - 1)
        End Sub
    End Structure '72byte

    Public Const MAX_PASSCTRLGROUP_COUNT As Short = 50
    Public Const MAX_PASSCTRL_COUNT As Short = 7
    '--- Pass Control Time ---
    Structure PASSTIME
        Dim StartHour As Byte
        Dim StartMinute As Byte
        Dim EndHour As Byte
        Dim EndMinute As Byte

        Public Sub Initialize()
            StartHour = 0
            StartMinute = 0
            EndHour = 0
            EndMinute = 0
        End Sub
    End Structure '4byte

    '--- Pass Control Time Infomation ---
    Structure PASSCTRLTIME
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_PASSCTRL_COUNT)> _
        Dim mPassCtrlTime() As PASSTIME

        Public Sub Initialize()
            ReDim mPassCtrlTime(MAX_PASSCTRL_COUNT - 1)
            Dim vnii As Integer

            For vnii = 0 To MAX_PASSCTRL_COUNT - 1
                mPassCtrlTime(vnii).Initialize()
            Next
        End Sub
    End Structure '28byte

    Public Const MAX_USERPASSINFO_COUNT As Short = 3
    Structure USERPASSINFO
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_USERPASSINFO_COUNT)> _
        Dim UserPassID() As Byte

        Public Sub Initialize()
            ReDim UserPassID(MAX_USERPASSINFO_COUNT - 1)
        End Sub
    End Structure '3byte

    Public Const MAX_GROUPPASSINFO_COUNT As Short = 3
    Structure GROUPPASSINFO
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_GROUPPASSINFO_COUNT)> _
        Dim GroupPassID() As Byte

        Public Sub Initialize()
            ReDim GroupPassID(MAX_GROUPPASSINFO_COUNT - 1)
        End Sub
    End Structure '3byte

    Public Const MAX_GROUPMATCHINFO_COUNT As Short = 10
    Structure GROUPMATCHINFO
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_GROUPMATCHINFO_COUNT)> _
        Dim GroupMatch() As Short

        Public Sub Initialize()
            ReDim GroupMatch(MAX_GROUPMATCHINFO_COUNT - 1)
        End Sub
    End Structure '20byte

    '//--- Post & Shift Info ---
    Public Const MAX_SHIFT_COUNT As Short = 24
    Public Const MAX_POST_COUNT As Short = 16
    Public Const MAX_DAY_IN_MONTH As Short = 31
    Public Const NAME_BYTE_COUNT As Short = 128

    Public Const USER_ID_LEN As Short = 16

    Structure SHIFT_TIME_SLOT
        Dim AMStartH As Byte ' AM time(hour)
        Dim AMStartM As Byte ' AM min(minute)
        Dim AMEndH As Byte ' AM time(hour)
        Dim AMEndM As Byte ' AM min(minute)
        Dim PMStartH As Byte ' PM time(hour)
        Dim PMStartM As Byte ' PM min(minute)
        Dim PMEndH As Byte ' PM time(hour)
        Dim PMEndM As Byte ' PM min(minute)
        Dim OVStartH As Byte ' OV time(hour)
        Dim OVStartM As Byte ' OV min(minute)
        Dim OVEndH As Byte ' OV time(hour)
        Dim OVEndM As Byte ' OV min(minute)
    End Structure '12byte

    Structure POST_NAME
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_BYTE_COUNT)> Dim PostName() As Byte

        Public Sub Initialize()
            ReDim PostName(NAME_BYTE_COUNT - 1)
        End Sub
    End Structure '14byte

    Public Const VER2_POST_SHIFT_INFO As Short = 2
    Public Const SIZE_POST_SHIFT_INFO_V2 As Short = 2476
    Structure POST_SHIFT_INFO
        Dim Size As Int32       '// 0, 4
        Dim Ver As Int32        '// 4, 4
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_SHIFT_COUNT)> Dim ShiftTime() As SHIFT_TIME_SLOT    '// 8, 288 (=12*24)
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_POST_COUNT)> Dim PostInfo() As POST_NAME            '// 296, 2048 (=128*16)
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_BYTE_COUNT)> Dim CompanyName() As Byte             '// 2344, 128
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> Dim Reserved() As Byte                              '// 2472, 4

        Public Sub Initialize()
            Dim k As Integer

            ReDim ShiftTime(MAX_SHIFT_COUNT - 1)
            ReDim PostInfo(MAX_POST_COUNT - 1)
            ReDim CompanyName(NAME_BYTE_COUNT - 1)
            ReDim Reserved(4 - 1)

            For k = 0 To MAX_POST_COUNT - 1
                PostInfo(k).Initialize()
            Next
        End Sub
    End Structure

    Public Const VER2_USER_INFO As Short = 2
    Public Const SIZE_USER_INFO_V2 As Short = 184


    Structure USER_SHIFT_INFO
        Dim Size As Int32               '// 0, 4
        Dim Ver As Int32                '// 4, 4
        Dim UserId As UInt32             '// 8, 4
        Dim Reserved As Int32           '// 12, 4
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_BYTE_COUNT)> Dim UserName() As Byte    '// 16, 128
        Dim PostId As Int32             '// 144, 4
        Dim YearAssigned As Int16       '// 148, 2
        Dim MonthAssigned As Int16      '// 150, 2
        Dim StartWeekdayOfMonth As Byte '// 152, 1
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_DAY_IN_MONTH)> Dim ShiftId() As Byte ' =31

        Public Sub Initialize()
            ReDim UserName(NAME_BYTE_COUNT - 1)
            ReDim ShiftId(MAX_DAY_IN_MONTH - 1)
        End Sub
    End Structure

    Public Const VER3_USER_INFO As Short = 3
    Public Const SIZE_USER_INFO_V3 As Short = 196
    Structure USER_SHIFT_INFO_STRING_ID
        Dim Size As Int32               '// 0, 4
        Dim Ver As Int32                '// 4, 4
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=USER_ID_LEN)> Dim UserId() As Byte    '// 16, 128
        Dim Reserved As Int32           '// 12, 4
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_BYTE_COUNT)> Dim UserName() As Byte    '// 16, 128
        Dim PostId As Int32             '// 144, 4
        Dim YearAssigned As Int16       '// 148, 2
        Dim MonthAssigned As Int16      '// 150, 2
        Dim StartWeekdayOfMonth As Byte '// 152, 1
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_DAY_IN_MONTH)> Dim ShiftId() As Byte ' =31

        Public Sub Initialize()
            ReDim UserId(USER_ID_LEN - 1)
            ReDim UserName(NAME_BYTE_COUNT - 1)
            ReDim ShiftId(MAX_DAY_IN_MONTH - 1)
        End Sub
    End Structure


    Public Const TIME_SLOT_COUNT As Integer = 6
    Public Const TIME_ZONE_COUNT As Integer = 255
    Public Const SIZE_TIME_ZONE_STRUCT As Integer = 32
    Public Const SIZE_USER_WEEK_PASS_TIME_STRUCT As Integer = 16

    '{ Pass Time (HaoShun)
    Structure TIME_SLOT
        Dim StartHour As Byte
        Dim StartMinute As Byte
        Dim EndHour As Byte
        Dim EndMinute As Byte
    End Structure    ' size = 4 bytes

    Structure TIME_ZONE
        Dim Size As Int32            ' 0, 4
        Dim TimeZoneId As Int32      ' 4, 4
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=TIME_SLOT_COUNT)> _
        Dim TimeSlots() As TIME_SLOT ' 8, 24 (= 4*6)

        Public Sub Init()
            Size = SIZE_TIME_ZONE_STRUCT
            TimeZoneId = 0
            ReDim TimeSlots(TIME_SLOT_COUNT - 1)
        End Sub
    End Structure    ' size = 32 bytes

    Structure USER_WEEK_PASS_TIME
        Dim Size As Int32            ' 0, 4
        Dim UserId As Int32          ' 4, 4
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=7)> _
        Dim WeekPassTime() As Byte   ' 8, 7
        Dim Reserved As Byte         ' 15, 1

        Public Sub Init()
            Size = SIZE_USER_WEEK_PASS_TIME_STRUCT
            UserId = 0
            ReDim WeekPassTime(6)
        End Sub
    End Structure    'size = 16 bytes
    '} Pass Time (HaoShun)

    '/******************************************************************/
    '/*                            Constant                            */
    '/******************************************************************/
    '//=============== Protocol Type ===============//
    Public Const PROTOCOL_TCPIP As Integer = 0                ' TCP/IP
    Public Const PROTOCOL_UDP As Integer = 1                  ' UDP

    '//=============== Backup Number Constant ===============//
    Public Const BACKUP_FP_0 As Integer = 0     ' Finger 0
    Public Const BACKUP_FP_1 As Integer = 1     ' Finger 1
    Public Const BACKUP_FP_2 As Integer = 2     ' Finger 2
    Public Const BACKUP_FP_3 As Integer = 3     ' Finger 3
    Public Const BACKUP_FP_4 As Integer = 4     ' Finger 4
    Public Const BACKUP_FP_5 As Integer = 5     ' Finger 5
    Public Const BACKUP_FP_6 As Integer = 6     ' Finger 6
    Public Const BACKUP_FP_7 As Integer = 7     ' Finger 7
    Public Const BACKUP_FP_8 As Integer = 8     ' Finger 8
    Public Const BACKUP_FP_9 As Integer = 9     ' Finger 9
    Public Const BACKUP_PSW As Integer = 10     ' Password
    Public Const BACKUP_CARD As Integer = 11    ' Card
    Public Const BACKUP_FACE As Integer = 12    ' Face
    Public Const BACKUP_VEIN_0 As Integer = 20 ' Vein 0


    '//=============== Manipulation of SuperLogData ===============//
    Public Const LOG_ENROLL_USER As Integer = 3               ' Enroll-User
    Public Const LOG_ENROLL_MANAGER As Integer = 4            ' Enroll-Manager
    Public Const LOG_ENROLL_DELFP As Integer = 5              ' FP Delete
    Public Const LOG_ENROLL_DELPASS As Integer = 6            ' Pass Delete
    Public Const LOG_ENROLL_DELCARD As Integer = 7            ' Card Delete
    Public Const LOG_LOG_ALLDEL As Integer = 8                ' LogAll Delete
    Public Const LOG_SETUP_SYS As Integer = 9                 ' Setup Sys
    Public Const LOG_SETUP_TIME As Integer = 10               ' Setup Time
    Public Const LOG_SETUP_LOG As Integer = 11                ' Setup Log
    Public Const LOG_SETUP_COMM As Integer = 12               ' Setup Comm
    Public Const LOG_PASSTIME As Integer = 13                 ' Pass Time Set
    Public Const LOG_SETUP_DOOR As Integer = 14               ' Door Set Log



    '//=============== VerifyMode of GeneralLogData ===============//
    Public Const LOG_FPVERIFY As Integer = 1               ' Fp Verify
    Public Const LOG_PASSVERIFY As Integer = 2             ' Pass Verify
    Public Const LOG_CARDVERIFY As Integer = 3             ' Card Verify
    Public Const LOG_FPPASS_VERIFY As Integer = 4          ' Pass+Fp Verify
    Public Const LOG_FPCARD_VERIFY As Integer = 5          ' Card+Fp Verify
    Public Const LOG_PASSFP_VERIFY As Integer = 6          ' Pass+Fp Verify
    Public Const LOG_CARDFP_VERIFY As Integer = 7          ' Card+Fp Verify
    Public Const LOG_CARDPASS_VERIFY As Integer = 9        ' Card+Pass Verify

    Public Const LOG_FACEVERIFY As Integer = 20            ' Face Verify
    Public Const LOG_FACECARDVERIFY As Integer = 21        ' Face+Card Verify
    Public Const LOG_FACEPASSVERIFY As Integer = 22        ' Face+Pass Verify
    Public Const LOG_CARDFACEVERIFY As Integer = 23        ' Card+Face Verify
    Public Const LOG_PASSFACEVERIFY As Integer = 24        ' Pass+Face Verify


    '//=============== DoorMode of GeneralLogData ===============//
    Public Const LOG_CLOSE_DOOR As Integer = 1                    'Door Close
    Public Const LOG_OPEN_HAND As Integer = 2                     ' Hand Open
    Public Const LOG_PROG_OPEN As Integer = 3                     'Open by PC
    Public Const LOG_PROG_CLOSE As Integer = 4                    'Close by PC
    Public Const LOG_OPEN_IREGAL As Integer = 5                   'Illegal Open
    Public Const LOG_CLOSE_IREGAL As Integer = 6                  'Illegal Close
    Public Const LOG_OPEN_COVER As Integer = 7                    'Cover Open
    Public Const LOG_CLOSE_COVER As Integer = 8                   'Cover Close
    Public Const LOG_OPEN_DOOR As Integer = 9                     'Door Open
    Public Const LOG_OPEN_DOOR_THREAT As Integer = 10             'Door Open
    '//=============== Verify Kind of Device ===============//
    Public Const VK_NONE As Integer = 0
    Public Const VK_FP As Integer = 1
    Public Const VK_PASS As Integer = 2
    Public Const VK_CARD As Integer = 3
    Public Const VK_FACE As Integer = 4
    Public Const VK_VEIN As Integer = 5
    Public Const VK_IRIS As Integer = 6
    '//=============== IOMode of GeneralLogData ===============//
    'Public Const LOG_IOMODE_IN = 0
    'Public Const LOG_IOMODE_OUT = 1
    'Public Const LOG_IOMODE_OVER_IN = 2    ' = LOG_IOMODE_IO
    'Public Const LOG_IOMODE_OVER_OUT = 3

    Public Const LOG_MODE_IO As Short = 0 'General
    Public Const LOG_MODE_ATTEND As Short = 1 'Normal Attend
    Public Const LOG_MODE_OVERTIME_ATTEND As Short = 2 'OverTime Attend
    Public Const LOG_MODE_ABNORMAL_ATTEND As Short = 3 'In after leaving a while
    Public Const LOG_MODE_LEAVE As Short = 4 'Normal Leave
    Public Const LOG_MODE_OVERTIME_LEAVE As Short = 5 'OverTime Leave
    Public Const LOG_MODE_ABNORMAL_LEAVE As Short = 6 'Abnormal Leave

    '//=============== Machine Privilege ===============//
    Public Const MP_NONE As Integer = 0                       ' General user
    Public Const MP_MANAGER_1 As Integer = 1             ' Manager1 : Super Manager(MP_ALL = 1(for in version)) -+ add by lih 2013-12-25 20:15
    Public Const MP_MANAGER_2 As Integer = 2             ' Manager2 : Operator +- modified by lih 2014-04-23 15:32
    Public Const MP_MANAGER_3 As Integer = 3             ' Manager3 : Capable to register user ++ add by lih 2013-12-25 20:15

    '//=============== Index of  GetDeviceStatus ===============//
    Public Const GET_MANAGERS As Integer = 1
    Public Const GET_USERS As Integer = 2
    Public Const GET_FPS As Integer = 3
    Public Const GET_PSWS As Integer = 4
    Public Const GET_SLOGS As Integer = 5
    Public Const GET_GLOGS As Integer = 6
    Public Const GET_ASLOGS As Integer = 7
    Public Const GET_AGLOGS As Integer = 8
    Public Const GET_CARDS As Integer = 9
    Public Const GET_FACES As Integer = 10

    '//=============== Index of  GetDeviceInfo ===============//
    Public Const DI_MANAGERS As Integer = 1                   ' Numbers of Manager
    Public Const DI_MACHINENUM As Integer = 2                 ' Device ID
    Public Const DI_LANGAUGE As Integer = 3                   ' Language
    Public Const DI_POWEROFF_TIME As Integer = 4              ' Auto-PowerOff Time
    Public Const DI_LOCK_CTRL As Integer = 5                  ' Lock Control
    Public Const DI_GLOG_WARNING As Integer = 6               ' General-Log Warning
    Public Const DI_SLOG_WARNING As Integer = 7               ' Super-Log Warning
    Public Const DI_VERIFY_INTERVALS As Integer = 8           ' Verify Interval Time
    Public Const DI_RSCOM_BPS As Integer = 9                  ' Comm Buadrate
    Public Const DI_DATE_SEPARATE As Integer = 10             ' Date Separate Symbol
    Public Const DI_VERIFY_KIND As Integer = 24               ' Verify Kind Symbol

    '//=============== Baudrate = value of DI_RSCOM_BPS ===============//
    Public Const BPS_9600 As Integer = 3
    Public Const BPS_19200 As Integer = 4
    Public Const BPS_38400 As Integer = 5
    Public Const BPS_57600 As Integer = 6
    Public Const BPS_115200 As Integer = 7

    '//=============== Product Data Index ===============//
    Public Const PRODUCT_SERIALNUMBER As Short = 1     ' Serial Number
    Public Const PRODUCT_BACKUPNUMBER As Short = 2     ' Backup Number
    Public Const PRODUCT_CODE As Short = 3             ' Product code
    Public Const PRODUCT_NAME As Short = 4             ' Product name
    Public Const PRODUCT_WEB As Short = 5              ' Product web
    Public Const PRODUCT_DATE As Short = 6             ' Product date
    Public Const PRODUCT_SENDTO As Short = 7           ' Product sendto

    '//=============== Door Status ===============//
    Public Const DOOR_CONROLRESET As Integer = 0
    Public Const DOOR_OPEND As Integer = 1
    Public Const DOOR_CLOSED As Integer = 2
    Public Const DOOR_COMMNAD As Integer = 3

    '//=============== Error code ===============//
    Public Const RUN_SUCCESS As Integer = 1
    Public Const RUNERR_NOSUPPORT As Integer = 0
    Public Const RUNERR_UNKNOWNERROR As Integer = -1
    Public Const RUNERR_NO_OPEN_COMM As Integer = -2
    Public Const RUNERR_WRITE_FAIL As Integer = -3
    Public Const RUNERR_READ_FAIL As Integer = -4
    Public Const RUNERR_INVALID_PARAM As Integer = -5
    Public Const RUNERR_NON_CARRYOUT As Integer = -6
    Public Const RUNERR_DATAARRAY_END As Integer = -7
    Public Const RUNERR_DATAARRAY_NONE As Integer = -8
    Public Const RUNERR_MEMORY As Integer = -9
    Public Const RUNERR_MIS_PASSWORD As Integer = -10
    Public Const RUNERR_MEMORYOVER As Integer = -11
    Public Const RUNERR_DATADOUBLE As Integer = -12
    Public Const RUNERR_MANAGEROVER As Integer = -14
    Public Const RUNERR_FPDATAVERSION As Integer = -15

    Public Function ReturnResultPrint(ByRef anResultCode As Integer) As String
        Select Case anResultCode
            Case RUN_SUCCESS
                ReturnResultPrint = "Successful!"
            Case RUNERR_NOSUPPORT
                ReturnResultPrint = "No support"
            Case RUNERR_UNKNOWNERROR
                ReturnResultPrint = "Unknown error"
            Case RUNERR_NO_OPEN_COMM
                ReturnResultPrint = "No Open Comm"
            Case RUNERR_WRITE_FAIL
                ReturnResultPrint = "Write Error"
            Case RUNERR_READ_FAIL
                ReturnResultPrint = "Read Error"
            Case RUNERR_INVALID_PARAM
                ReturnResultPrint = "Parameter Error"
            Case RUNERR_NON_CARRYOUT
                ReturnResultPrint = "execution of command failed"
            Case RUNERR_DATAARRAY_END
                ReturnResultPrint = "End of data"
            Case RUNERR_DATAARRAY_NONE
                ReturnResultPrint = "Nonexistence data"
            Case RUNERR_MEMORY
                ReturnResultPrint = "Memory Allocating Error"
            Case RUNERR_MIS_PASSWORD
                ReturnResultPrint = "License Error"
            Case RUNERR_MEMORYOVER
                ReturnResultPrint = "full enrolldata & can`t put enrolldata"
            Case RUNERR_DATADOUBLE
                ReturnResultPrint = "this ID is already  existed."
            Case RUNERR_MANAGEROVER
                ReturnResultPrint = "full manager & can`t put manager."
            Case RUNERR_FPDATAVERSION
                ReturnResultPrint = "mistake fp data version."
            Case Else
                ReturnResultPrint = "Unknown error"
        End Select
    End Function

    Public Function ConvertByteArrayToStructure(ByVal aByteArray() As Byte, ByVal aType As System.Type) As Object
        Dim vObject As Object
        Dim vptr As IntPtr
        Dim vnSize As Integer

        On Error GoTo errl_ConvertByteArrayToStructure
        vnSize = Marshal.SizeOf(aType)
        If aByteArray.Length < vnSize Then
            ConvertByteArrayToStructure = Nothing
            Exit Function
        End If

        vptr = Marshal.AllocHGlobal(vnSize)
        Marshal.Copy(aByteArray, 0, vptr, vnSize)
        vObject = Marshal.PtrToStructure(vptr, aType)
        Marshal.FreeHGlobal(vptr)
        ConvertByteArrayToStructure = vObject
        Exit Function

errl_ConvertByteArrayToStructure:
        ConvertByteArrayToStructure = Nothing
    End Function

    Public Sub ConvertStructureToByteArray(ByVal aStruct As Object, ByRef aByteArray() As Byte)
        Dim vptr As IntPtr
        Dim vnSize As Integer

        On Error GoTo errl_ConvertStructureToByteArray
        vptr = IntPtr.Zero
        vnSize = Marshal.SizeOf(aStruct)
        If aByteArray.Length < vnSize Then Exit Sub
        vptr = Marshal.AllocHGlobal(vnSize)
        Marshal.StructureToPtr(aStruct, vptr, False)
        Marshal.Copy(vptr, aByteArray, 0, vnSize)
        Marshal.FreeHGlobal(vptr)
        Exit Sub

errl_ConvertStructureToByteArray:
    End Sub
    Public Sub ConvertStructureToByteArray_ATF686n(ByVal aStruct As Object, ByVal aByteArray() As Byte)
        Try
            Dim vptr As IntPtr = IntPtr.Zero
            Dim vnSize As Integer = Marshal.SizeOf(aStruct)
            If aByteArray.Length < vnSize Then
                Return
            End If
            vptr = Marshal.AllocHGlobal(vnSize)
            Marshal.StructureToPtr(aStruct, vptr, False)
            Marshal.Copy(vptr, aByteArray, 0, vnSize)
            Marshal.FreeHGlobal(vptr)
            Return
        Catch e1 As System.Exception
            MessageBox.Show("Fail to convert structure to byte array", "ComDllsSample_CS", MessageBoxButtons.OK)
            Return
        End Try
    End Sub
    '----------------------------------------------------------------
    ' Convert string to byte array
    '   !!note : string in vb are Unicode string
    ' The result buffer is ended by two zero

    Sub StringToByteArrayUtf16(ByRef aByteAry() As Byte, ByVal aStr As String)
        Dim bytRet() As Byte
        Dim nBytesToCopy As Integer

        Array.Clear(aByteAry, 0, aByteAry.Length)
        bytRet = System.Text.Encoding.GetEncoding("utf-16").GetBytes(aStr)
        nBytesToCopy = bytRet.Length
        If nBytesToCopy > aByteAry.Length - 2 Then nBytesToCopy = aByteAry.Length - 2
        Array.Copy(bytRet, aByteAry, nBytesToCopy)
    End Sub

    '----------------------------------------------------------------
    ' Convert byte array to string
    '   !!note : string in vb are Unicode string
    Function ByteArrayUtf16ToString(ByVal aByteAry() As Byte) As String
        Try
            ByteArrayUtf16ToString = System.Text.Encoding.GetEncoding("utf-16").GetString(aByteAry, 0, aByteAry.Length)
            ByteArrayUtf16ToString = ByteArrayUtf16ToString.TrimEnd(Chr(0))
        Catch ex As Exception
            ByteArrayUtf16ToString = ""
        End Try
    End Function

    Function GetHexString(ByVal aByteVal As Byte) As String
        GetHexString = Hex(aByteVal)
        If Len(GetHexString) <> 2 Then GetHexString = "0" & GetHexString
    End Function

    Function StringToUTF16HexString(ByVal astrVal As String) As String
        Dim bytUtf16() As Byte
        Dim k As Int32

        bytUtf16 = System.Text.Encoding.GetEncoding("utf-16").GetBytes(astrVal)

        StringToUTF16HexString = ""
        For k = LBound(bytUtf16) To UBound(bytUtf16)
            StringToUTF16HexString = StringToUTF16HexString & GetHexString(bytUtf16(k))
        Next k
    End Function

    Function GetByteVal_1(ByVal astrHex_1 As String) As Byte
        If Len(astrHex_1) = 1 Then
            If astrHex_1 >= "0" And astrHex_1 <= "9" Then
                GetByteVal_1 = Asc(astrHex_1) - Asc("0")
            ElseIf astrHex_1 = "A" Or astrHex_1 = "a" Then
                GetByteVal_1 = 10
            ElseIf astrHex_1 = "B" Or astrHex_1 = "b" Then
                GetByteVal_1 = 11
            ElseIf astrHex_1 = "C" Or astrHex_1 = "c" Then
                GetByteVal_1 = 12
            ElseIf astrHex_1 = "D" Or astrHex_1 = "d" Then
                GetByteVal_1 = 13
            ElseIf astrHex_1 = "E" Or astrHex_1 = "e" Then
                GetByteVal_1 = 14
            ElseIf astrHex_1 = "F" Or astrHex_1 = "f" Then
                GetByteVal_1 = 15
            End If
        Else
            GetByteVal_1 = 0
        End If
    End Function

    Function GetByteVal(ByVal astrHex As String) As Byte
        If Len(astrHex) = 1 Then
            GetByteVal = GetByteVal_1(astrHex)
        ElseIf Len(astrHex) = 2 Then
            GetByteVal = GetByteVal_1(Mid(astrHex, 1, 1)) * 16 + GetByteVal_1(Mid(astrHex, 2, 1))
        Else
            GetByteVal = 0
        End If
    End Function

    Function UTF16HexStringToString(ByVal astrUtf16Hex As String) As String
        Dim bytUtf16() As Byte
        Dim k As Int32, lenStr As Int32
        Dim strHex As String

        UTF16HexStringToString = ""
        lenStr = Len(astrUtf16Hex)
        If lenStr = 0 Then Exit Function
        If (lenStr Mod 2) <> 0 Then Exit Function

        ReDim bytUtf16(lenStr / 2 - 1)
        For k = 0 To lenStr / 2 - 1
            strHex = Mid(astrUtf16Hex, k * 2 + 1, 2)
            bytUtf16(k) = GetByteVal(strHex)
        Next k

        UTF16HexStringToString = System.Text.Encoding.GetEncoding("utf-16").GetString(bytUtf16)
    End Function

    Function GetInt(asVal As String) As Integer
        Try
            Return Convert.ToInt32(asVal)
        Catch
            Return 0
        End Try
    End Function

    Public Function GetStringVerifyMode(anVerifyMode As Long) As String
        Dim vbyteKind(3) As Byte
        Dim nIndex, vFirstKind, vSecondKind As Byte
        Dim vRet As String

        vbyteKind = BitConverter.GetBytes(anVerifyMode)

        '..SetLong(vbyteKind, 0, anVerifyMode)

        vRet = ""
        For nIndex = 0 To 3

            vFirstKind = vbyteKind(3 - nIndex)

            vSecondKind = vbyteKind(3 - nIndex)

            vFirstKind = vFirstKind And -16

            vSecondKind = vSecondKind And 15

            vFirstKind = vFirstKind >> 4

            If vFirstKind = 0 Then Exit For

            If nIndex > 0 Then vRet = vRet + "+"

            Select Case vFirstKind
                Case VK_FP
                    vRet = vRet + "FP"
                Case VK_PASS
                    vRet = vRet + "PASS"
                Case VK_CARD
                    vRet = vRet + "CARD"
                Case VK_FACE
                    vRet = vRet + "FACE"
                Case VK_VEIN
                    vRet = vRet + "VEIN"
                Case VK_IRIS
                    vRet = vRet + "IRIS"

            End Select

            If vSecondKind = 0 Then Exit For

            vRet = vRet + "+"

            Select Case vSecondKind
                Case VK_FP
                    vRet = vRet + "FP"
                Case VK_PASS
                    vRet = vRet + "PASS"
                Case VK_CARD
                    vRet = vRet + "CARD"
                Case VK_FACE
                    vRet = vRet + "FACE"
                Case VK_VEIN
                    vRet = vRet + "VEIN"
                Case VK_IRIS
                    vRet = vRet + "IRIS"
            End Select

        Next

        If vRet = "" Then vRet = "--"

        GetStringVerifyMode = vRet

    End Function

    Public Sub GetIoModeAndDoorMode(nIoMode As Long, ByRef vIoMode As Long, ByRef vDoorMode As Long)
        Dim vbyteKind(4) As Byte
        Dim vbyteDoorMode(4) As Byte
        Dim nIndex As Integer

        vbyteKind = BitConverter.GetBytes(nIoMode)
        vIoMode = vbyteKind(0)
        For nIndex = 0 To 2
            vbyteDoorMode(nIndex) = vbyteKind(nIndex + 1)
        Next
        vbyteDoorMode(3) = 0
        vDoorMode = BitConverter.ToInt32(vbyteDoorMode, 0)

    End Sub


End Module