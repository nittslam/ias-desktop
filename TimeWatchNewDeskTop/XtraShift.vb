﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraEditors.Repository
Imports System.Data.SqlClient
Imports System.Data.OleDb

Public Class XtraShift
    'Dim res_man As ResourceManager     'declare Resource manager to access to specific cultureinfo
    'Dim cul As CultureInfo     'declare culture info
    'Dim servername As String
    'Dim ConnectionString As String
    Dim ulf As UserLookAndFeel
    Public Sub New()
        InitializeComponent()
        'Dim fs As FileStream = New FileStream("db.txt", FileMode.Open, FileAccess.Read)
        'Dim sr As StreamReader = New StreamReader(fs)
        'Dim str As String
        'Dim str1() As String
        'Do While sr.Peek <> -1
        '    str = sr.ReadLine
        '    str1 = str.Split(",")
        '    servername = str1(0)
        'Loop
        'sr.Close()
        'fs.Close()

        If Common.servername = "Access" Then
            'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            Me.TblShiftMaster1TableAdapter1.Fill(Me.SSSDBDataSet.tblShiftMaster1)
            GridControl1.DataSource = SSSDBDataSet.tblShiftMaster1
        Else
            'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
            TblShiftMasterTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            Me.TblShiftMasterTableAdapter.Fill(Me.SSSDBDataSet.tblShiftMaster)
            GridControl1.DataSource = SSSDBDataSet.tblShiftMaster
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub
    Private Sub XtraShift_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        'res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraShift).Assembly)
        'cul = CultureInfo.CreateSpecificCulture("en")

        GridView1.Columns.Item(0).Caption = Common.res_man.GetString("shiftcode", Common.cul)
        GridView1.Columns.Item(1).Caption = Common.res_man.GetString("shiftstart", Common.cul)
        GridView1.Columns.Item(2).Caption = Common.res_man.GetString("shiftend", Common.cul)
        GridView1.Columns.Item(3).Caption = Common.res_man.GetString("shifthour", Common.cul)

        GridView1.Columns.Item(4).Caption = Common.res_man.GetString("lunchstart", Common.cul)
        GridView1.Columns.Item(5).Caption = Common.res_man.GetString("lunchduration", Common.cul)
        GridView1.Columns.Item(6).Caption = Common.res_man.GetString("lunchend", Common.cul)

        GridView1.Columns.Item(8).Caption = Common.res_man.GetString("overtimedeductafter", Common.cul)
        GridView1.Columns.Item(9).Caption = Common.res_man.GetString("overtimestertsfter", Common.cul)
        GridView1.Columns.Item(10).Caption = Common.res_man.GetString("overtimededuction", Common.cul)
        GridView1.Columns.Item(11).Caption = Common.res_man.GetString("lunchdeduction", Common.cul)
        GridView1.Columns.Item(12).Caption = Common.res_man.GetString("shiftpos", Common.cul)


        'Dim de As New RepositoryItemTimeEdit()
        'de.Mask.EditMask = "HH:mm"
        'de.Mask.UseMaskAsDisplayFormat = True
        'colLUNCHDURATION.ColumnEdit = de

        If Common.SftDel <> "Y" Then
            GridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = False
        Else
            GridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = True
        End If
    End Sub
    Private Sub GridView1_InitNewRow(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles GridView1.InitNewRow
        Dim view As GridView = CType(sender, GridView)
        view.SetRowCellValue(e.RowHandle, "LastModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
        view.SetRowCellValue(e.RowHandle, "LastModifiedBy", "admin")

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            view.SetRowCellValue(e.RowHandle, "SHIFTPOSITION", "DAY")
        End If

    End Sub
    Private Sub GridView1_RowDeleted(sender As System.Object, e As DevExpress.Data.RowDeletedEventArgs) Handles GridView1.RowDeleted
        Me.TblShiftMasterTableAdapter.Update(Me.SSSDBDataSet.tblShiftMaster)
        Me.TblShiftMaster1TableAdapter1.Update(Me.SSSDBDataSet.tblShiftMaster1)
        XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))
    End Sub
    Private Sub GridView1_RowUpdated(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles GridView1.RowUpdated
        Me.TblShiftMasterTableAdapter.Update(Me.SSSDBDataSet.tblShiftMaster)
        Me.TblShiftMaster1TableAdapter1.Update(Me.SSSDBDataSet.tblShiftMaster1)
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            Common.LogPost("Shift Add; ") 'COMPANYCODE= '" & CellId)
        Else
            Common.LogPost("Shift Update;") ' COMPANYCODE= '" & CellId)
        End If
    End Sub
    Private Sub GridControl1_EmbeddedNavigator_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControl1.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                Me.Validate()
                e.Handled = True
                'MsgBox("Your records have been saved and updated successfully!")
            Else
                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter
                Dim ds As DataSet = New DataSet
                Dim sSql As String = "select * from tblShiftMaster"
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows.Count = 1 Then
                    Me.Validate()
                    e.Handled = True
                    XtraMessageBox.Show(ulf, "<size=10>Minimum one record is necessary.</size>", "<size=9>Error</size>")
                End If

                Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
                Dim CellId As String = row("SHIFT").ToString.Trim

                'Dim adap As SqlDataAdapter
                'Dim adapA As OleDbDataAdapter
                'Dim ds As DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter("select count(*) from EmployeeGroup where SHIFT = '" & CellId & "'", Common.con1)
                    ds = New DataSet
                    adapA.Fill(ds)
                    If ds.Tables(0).Rows(0).Item(0).ToString > 0 Then
                        XtraMessageBox.Show(ulf, "<size=10>Shift already assigned to Employee Group. Cannot delete.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Me.Validate()
                        e.Handled = True
                        Exit Sub
                    End If
                Else
                    adap = New SqlDataAdapter("select count(*) from EmployeeGroup where SHIFT = '" & CellId & "'", Common.con)
                    ds = New DataSet
                    adap.Fill(ds)
                    If ds.Tables(0).Rows(0).Item(0).ToString > 0 Then
                        XtraMessageBox.Show(ulf, "<size=10>Shift already assigned to Employee Group. Cannot delete.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Me.Validate()
                        e.Handled = True
                        Exit Sub
                    End If
                End If
                Common.LogPost("Shift Delete; Shift Code: " & CellId)
            End If
        End If
    End Sub
    Private Sub GridView1_ValidateRow(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles GridView1.ValidateRow
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Dim a As DateTime

        Dim CellId As String = row("SHIFTPOSITION").ToString.Trim
        If CellId = "" Then
            e.Valid = False
            e.ErrorText = "<size=10>" & Common.res_man.GetString("shiftpos", Common.cul) & " " & Common.res_man.GetString("cannot_be_empty", Common.cul) & ","
        End If
        CellId = row("SHIFT").ToString.Trim
        If CellId = "" Then
            e.Valid = False
            e.ErrorText = "<size=10>" & Common.res_man.GetString("shiftcode", Common.cul) & " " & Common.res_man.GetString("cannot_be_empty", Common.cul) & ","
        End If


        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter("select SHIFT from tblShiftMaster where SHIFT = '" & row(0).ToString & "'", Common.con1)
                ds = New DataSet
                adapA.Fill(ds)
                If ds.Tables(0).Rows.Count > 0 Then
                    e.Valid = False
                    e.ErrorText = "<size=10>Duplicate shift Code ,"
                End If
            Else
                adap = New SqlDataAdapter("select SHIFT from tblShiftMaster where SHIFT = '" & row(0).ToString & "'", Common.con)
                ds = New DataSet
                adap.Fill(ds)
                If ds.Tables(0).Rows.Count > 0 Then
                    e.Valid = False
                    e.ErrorText = "<size=10>Duplicate shift Code ,"
                End If
            End If
        End If



        If row("STARTTIME").ToString = "" Or row("ENDTIME").ToString = "" Or row("LUNCHTIME").ToString = "" Or row("LUNCHDURATION").ToString = "" Or row("OTSTARTAFTER").ToString = "" Or row("OTDEDUCTHRS").ToString = "" Or row("OTDEDUCTAFTER").ToString = "" Or row("LUNCHDEDUCTION").ToString = "" Or row("ShiftEarly").ToString = "" Or row("ShiftLate").ToString = "" Then
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("emptyfieldmsgshift", Common.cul) & vbCrLf & Common.res_man.GetString("wantcontinue", Common.cul) & "</size>", _
                                   "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                If row("STARTTIME").ToString = "" Then
                    row("STARTTIME") = Convert.ToDateTime("1899-12-30 00:00:00")
                Else
                    a = Convert.ToDateTime(row("STARTTIME").ToString().Trim)
                    row("STARTTIME") = "1899-12-30 " & a.ToString("HH:mm:ss")
                End If

                If row("ENDTIME").ToString = "" Then
                    row("ENDTIME") = Convert.ToDateTime("1899-12-30 00:00:00")
                Else
                    a = Convert.ToDateTime(row("ENDTIME").ToString().Trim)
                    row("ENDTIME") = "1899-12-30 " & a.ToString("HH:mm:ss")
                End If

                If row("LUNCHTIME").ToString = "" Then
                    row("LUNCHTIME") = Convert.ToDateTime("1899-12-30 00:00:00")
                Else
                    a = Convert.ToDateTime(row("LUNCHTIME").ToString().Trim)
                    row("LUNCHTIME") = "1899-12-30 " & a.ToString("HH:mm:ss")
                End If


                If row("LUNCHENDTIME").ToString = "" Then
                    row("LUNCHENDTIME") = Convert.ToDateTime("1899-12-30 00:00:00")
                Else
                    a = Convert.ToDateTime(row("LUNCHENDTIME").ToString().Trim)
                    row("LUNCHENDTIME") = "1899-12-30 " & a.ToString("HH:mm:ss")
                End If


                If row("LUNCHDURATION").ToString = "" Then
                    row("LUNCHDURATION") = 0
                Else
                    a = Convert.ToDateTime(row("LUNCHDURATION").ToString().Trim)
                    row("LUNCHDURATION") = (a.Hour * 60) + a.Minute
                End If


                If row("SHIFTDURATION").ToString = "" Then
                    row("SHIFTDURATION") = 0
                Else
                    a = Convert.ToDateTime(row("SHIFTDURATION").ToString().Trim)
                    row("SHIFTDURATION") = (a.Hour * 60) + a.Minute
                End If






                If row("OTSTARTAFTER").ToString = "" Then
                    row("OTSTARTAFTER") = 0
                Else
                    a = Convert.ToDateTime(row("OTSTARTAFTER").ToString().Trim)
                    row("OTSTARTAFTER") = (a.Hour * 60) + a.Minute
                End If

                If row("OTDEDUCTHRS").ToString = "" Then
                    row("OTDEDUCTHRS") = 0
                Else
                    a = Convert.ToDateTime(row("OTDEDUCTHRS").ToString().Trim)
                    row("OTDEDUCTHRS") = (a.Hour * 60) + a.Minute
                End If

                If row("LUNCHDEDUCTION").ToString = "" Then
                    row("LUNCHDEDUCTION") = 0
                Else
                    a = Convert.ToDateTime(row("LUNCHDEDUCTION").ToString().Trim)
                    row("LUNCHDEDUCTION") = (a.Hour * 60) + a.Minute
                End If

                If row("OTDEDUCTAFTER").ToString = "" Then
                    row("OTDEDUCTAFTER") = 0
                Else
                    a = Convert.ToDateTime(row("OTDEDUCTAFTER").ToString().Trim)
                    row("OTDEDUCTAFTER") = (a.Hour * 60) + a.Minute
                End If

                If row("ShiftEarly").ToString = "" Then
                    row("ShiftEarly") = 0
                Else
                    a = Convert.ToDateTime(row("ShiftEarly").ToString().Trim)
                    row("ShiftEarly") = (a.Hour * 60) + a.Minute
                End If
                If row("ShiftLate").ToString = "" Then
                    row("ShiftLate") = 0
                Else
                    a = Convert.ToDateTime(row("ShiftLate").ToString().Trim)
                    row("ShiftLate") = (a.Hour * 60) + a.Minute
                End If
            Else
                GridView1.CancelUpdateCurrentRow()
                GridView1.CloseEditForm()
            End If
            'e.Valid = False
            'e.ErrorText = Common.res_man.GetString("emptyfieldmsgshift", Common.cul) & ","
        Else
            If row("STARTTIME").ToString = "" Then
                row("STARTTIME") = Convert.ToDateTime("1899-12-30 00:00:00")
            Else
                a = Convert.ToDateTime(row("STARTTIME").ToString().Trim)
                row("STARTTIME") = "1899-12-30 " & a.ToString("HH:mm:ss")
            End If

            If row("ENDTIME").ToString = "" Then
                row("ENDTIME") = Convert.ToDateTime("1899-12-30 00:00:00")
            Else
                a = Convert.ToDateTime(row("ENDTIME").ToString().Trim)
                row("ENDTIME") = "1899-12-30 " & a.ToString("HH:mm:ss")
            End If

            If row("LUNCHTIME").ToString = "" Then
                row("LUNCHTIME") = Convert.ToDateTime("1899-12-30 00:00:00")
            Else
                a = Convert.ToDateTime(row("LUNCHTIME").ToString().Trim)
                row("LUNCHTIME") = "1899-12-30 " & a.ToString("HH:mm:ss")
            End If


            If row("LUNCHENDTIME").ToString = "" Then
                row("LUNCHENDTIME") = Convert.ToDateTime("1899-12-30 00:00:00")
            Else
                a = Convert.ToDateTime(row("LUNCHENDTIME").ToString().Trim)
                row("LUNCHENDTIME") = "1899-12-30 " & a.ToString("HH:mm:ss")
            End If


            If row("LUNCHDURATION").ToString = "" Then
                row("LUNCHDURATION") = 0
            Else
                a = Convert.ToDateTime(row("LUNCHDURATION").ToString().Trim)
                row("LUNCHDURATION") = (a.Hour * 60) + a.Minute
            End If


            If row("SHIFTDURATION").ToString = "" Then
                row("SHIFTDURATION") = 0
            Else
                a = Convert.ToDateTime(row("SHIFTDURATION").ToString().Trim)
                row("SHIFTDURATION") = (a.Hour * 60) + a.Minute
            End If

            'MsgBox(row("OTSTARTAFTER").ToString)
            If row("OTSTARTAFTER").ToString = "" Then
                row("OTSTARTAFTER") = 0
            Else
                a = Convert.ToDateTime(row("OTSTARTAFTER").ToString().Trim)
                row("OTSTARTAFTER") = (a.Hour * 60) + a.Minute
            End If

            If row("OTDEDUCTHRS").ToString = "" Then
                row("OTDEDUCTHRS") = 0
            Else
                a = Convert.ToDateTime(row("OTDEDUCTHRS").ToString().Trim)
                row("OTDEDUCTHRS") = (a.Hour * 60) + a.Minute
            End If

            If row("LUNCHDEDUCTION").ToString = "" Then
                row("LUNCHDEDUCTION") = 0
            Else
                a = Convert.ToDateTime(row("LUNCHDEDUCTION").ToString().Trim)
                row("LUNCHDEDUCTION") = (a.Hour * 60) + a.Minute
            End If

            If row("OTDEDUCTAFTER").ToString = "" Then
                row("OTDEDUCTAFTER") = 0
            Else
                a = Convert.ToDateTime(row("OTDEDUCTAFTER").ToString().Trim)
                row("OTDEDUCTAFTER") = (a.Hour * 60) + a.Minute
            End If


            If row("ShiftEarly").ToString = "" Then
                row("ShiftEarly") = 0
            Else
                a = Convert.ToDateTime(row("ShiftEarly").ToString().Trim)
                row("ShiftEarly") = (a.Hour * 60) + a.Minute
            End If
            If row("ShiftLate").ToString = "" Then
                row("ShiftLate") = 0
            Else
                a = Convert.ToDateTime(row("ShiftLate").ToString().Trim)
                row("ShiftLate") = (a.Hour * 60) + a.Minute
            End If

        End If


        Try
        Dim startlunch As DateTime = Convert.ToDateTime(row("LUNCHTIME").ToString().Trim)
        Dim startshift As DateTime = Convert.ToDateTime(row("STARTTIME").ToString().Trim)
        Dim endshift As DateTime = Convert.ToDateTime(row("ENDTIME").ToString().Trim)
        If startshift > endshift Then
            endshift = endshift.AddDays(1)
            If startlunch < startshift Then
                startlunch = startlunch.AddDays(1)
            End If
            'MsgBox(startshift.ToString & "  " & endshift.ToString & "  " & startlunch.ToString)
        End If
        'MsgBox(startlunch.ToString)
        If startlunch.ToString("yyyy-MM-dd HH:mm:ss") = "1899-12-30 00:00:00" Then
        Else
            If startlunch < startshift Or startlunch > endshift Then
                e.Valid = False
                    e.ErrorText = "<size=10>" & Common.res_man.GetString("lunchstart", Common.cul) & " " & Common.res_man.GetString("notinrange", Common.cul) & ","
            End If
        End If
        Catch ex As Exception

        End Try
        Dim view As GridView = CType(sender, GridView)
        view.SetRowCellValue(e.RowHandle, "LastModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
        view.SetRowCellValue(e.RowHandle, "LastModifiedBy", "admin")
    End Sub
    Private Sub GridView1_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView1.CustomColumnDisplayText
        Try
            Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "SHIFTDURATION" Then
                'MsgBox(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "SHIFTDURATION").ToString().Trim)
                'e.DisplayText = Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ENDTIME").ToString().Trim).Subtract(Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "STARTTIME").ToString().Trim)).ToString("t").Substring(0, 5)
                e.DisplayText = (Convert.ToDouble(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "SHIFTDURATION").ToString().Trim) \ 60).ToString("00") & ":" & (Convert.ToDouble(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "SHIFTDURATION").ToString().Trim) Mod 60).ToString("00")
            End If
            If e.Column.FieldName = "LUNCHDURATION" Then
                e.DisplayText = (Convert.ToDouble(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "LUNCHDURATION").ToString().Trim) \ 60).ToString("00") & ":" & (Convert.ToDouble(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "LUNCHDURATION").ToString().Trim) Mod 60).ToString("00")
            End If

            If e.Column.FieldName = "OTSTARTAFTER" Then
                e.DisplayText = (Convert.ToDouble(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "OTSTARTAFTER").ToString().Trim) \ 60).ToString("00") & ":" & (Convert.ToDouble(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "OTSTARTAFTER").ToString().Trim) Mod 60).ToString("00")
            End If

            If e.Column.FieldName = "OTDEDUCTHRS" Then
                e.DisplayText = (Convert.ToDouble(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "OTDEDUCTHRS").ToString().Trim) \ 60).ToString("00") & ":" & (Convert.ToDouble(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "OTDEDUCTHRS").ToString().Trim) Mod 60).ToString("00")
            End If

            If e.Column.FieldName = "LUNCHDEDUCTION" Then
                e.DisplayText = (Convert.ToDouble(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "LUNCHDEDUCTION").ToString().Trim) \ 60).ToString("00") & ":" & (Convert.ToDouble(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "LUNCHDEDUCTION").ToString().Trim) Mod 60).ToString("00")
            End If

            If e.Column.FieldName = "OTDEDUCTAFTER" Then
                e.DisplayText = (Convert.ToDouble(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "OTDEDUCTAFTER").ToString().Trim) \ 60).ToString("00") & ":" & (Convert.ToDouble(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "OTDEDUCTAFTER").ToString().Trim) Mod 60).ToString("00")
            End If

            If e.Column.FieldName = "ShiftEarly" Then
                e.DisplayText = (Convert.ToDouble(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ShiftEarly").ToString().Trim) \ 60).ToString("00") & ":" & (Convert.ToDouble(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ShiftEarly").ToString().Trim) Mod 60).ToString("00")
            End If
            If e.Column.FieldName = "ShiftLate" Then
                e.DisplayText = (Convert.ToDouble(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ShiftLate").ToString().Trim) \ 60).ToString("00") & ":" & (Convert.ToDouble(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ShiftLate").ToString().Trim) Mod 60).ToString("00")
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub GridView1_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView1.EditFormShowing
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            If Common.SftAdd <> "Y" Then
                e.Allow = False
                Exit Sub
            End If
        Else
            If Common.SftModi <> "Y" Then
                e.Allow = False
                Exit Sub
            End If
        End If

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Try
            'row("SHIFTDURATION") = Convert.ToDateTime(row("ENDTIME").ToString().Trim).Subtract(Convert.ToDateTime(row("STARTTIME").ToString().Trim)).ToString("t").Substring(0, 5)
            row("SHIFTDURATION") = (row("SHIFTDURATION").ToString().Trim \ 60).ToString("00") & ":" & (row("SHIFTDURATION").ToString().Trim Mod 60).ToString("00")
            Dim view As DevExpress.XtraGrid.Views.Grid.GridView = CType(GridControl1.FocusedView, GridView)
            Dim ldur As String = (row("LUNCHDURATION").ToString().Trim \ 60).ToString("00") & ":" & (row("LUNCHDURATION").ToString().Trim Mod 60).ToString("00")
            view.SetRowCellValue(view.FocusedRowHandle, "LUNCHDURATION", ldur)

            row("OTSTARTAFTER") = (row("OTSTARTAFTER").ToString().Trim \ 60).ToString("00") & ":" & (row("OTSTARTAFTER").ToString().Trim Mod 60).ToString("00")
            row("OTDEDUCTHRS") = (row("OTDEDUCTHRS").ToString().Trim \ 60).ToString("00") & ":" & (row("OTDEDUCTHRS").ToString().Trim Mod 60).ToString("00")
            row("OTDEDUCTAFTER") = (row("OTDEDUCTAFTER").ToString().Trim \ 60).ToString("00") & ":" & (row("OTDEDUCTAFTER").ToString().Trim Mod 60).ToString("00")
            row("LUNCHDEDUCTION") = (row("LUNCHDEDUCTION").ToString().Trim \ 60).ToString("00") & ":" & (row("LUNCHDEDUCTION").ToString().Trim Mod 60).ToString("00")

            row("ShiftEarly") = (row("ShiftEarly").ToString().Trim \ 60).ToString("00") & ":" & (row("ShiftEarly").ToString().Trim Mod 60).ToString("00")
            row("ShiftLate") = (row("ShiftLate").ToString().Trim \ 60).ToString("00") & ":" & (row("ShiftLate").ToString().Trim Mod 60).ToString("00")

        Catch

        End Try
    End Sub
    Private Sub GridView1_CellValueChanged(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles GridView1.CellValueChanged
        Dim view As DevExpress.XtraGrid.Views.Grid.GridView = CType(GridControl1.FocusedView, GridView)
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

        'If e.Column.FieldName = "OTDEDUCTAFTER" Then
        '    'MsgBox(row("OTDEDUCTAFTER"))
        '    view.SetRowCellValue(view.FocusedRowHandle, "OTDEDUCTAFTER", row("OTDEDUCTAFTER"))
        'End If
        Try
            If e.Column.FieldName = "STARTTIME" Then
                Dim startshift As DateTime = Convert.ToDateTime(row("STARTTIME").ToString().Trim)
                Dim endshift As DateTime = Convert.ToDateTime(row("ENDTIME").ToString().Trim)
                Dim a As String
                If startshift > endshift Then
                    a = Convert.ToDateTime(row("ENDTIME").ToString().Trim).AddDays(1).Subtract(Convert.ToDateTime(row("STARTTIME").ToString().Trim)).ToString("t")
                Else
                    a = Convert.ToDateTime(row("ENDTIME").ToString().Trim).Subtract(Convert.ToDateTime(row("STARTTIME").ToString().Trim)).ToString("t")
                End If
                view.SetRowCellValue(view.FocusedRowHandle, "SHIFTDURATION", a.Substring(0, 5))
            End If
            If e.Column.FieldName = "ENDTIME" Then
                Dim startshift As DateTime = Convert.ToDateTime(row("STARTTIME").ToString().Trim) '.ToString("yyyy-MM-dd HH:mm:ss")
                Dim endshift As DateTime = Convert.ToDateTime(row("ENDTIME").ToString().Trim) '.ToString("yyyy-MM-dd HH:mm:ss")
                'MsgBox("startshift " & startshift.ToString & vbCrLf & "endshift " & endshift.ToString)
                Dim a As String
                If startshift > endshift Then
                    a = Convert.ToDateTime(row("ENDTIME").ToString().Trim).AddDays(1).Subtract(Convert.ToDateTime(row("STARTTIME").ToString().Trim)).ToString("t")
                Else
                    a = Convert.ToDateTime(row("ENDTIME").ToString().Trim).Subtract(Convert.ToDateTime(row("STARTTIME").ToString().Trim)).ToString("t")
                End If
                view.SetRowCellValue(view.FocusedRowHandle, "SHIFTDURATION", a.Substring(0, 5))
            End If
        Catch ex As Exception

        End Try
        Try
            If e.Column.FieldName = "LUNCHTIME" Then
                Dim startlunch As DateTime = Convert.ToDateTime(row("LUNCHTIME").ToString().Trim)
                'Dim startshift As DateTime = Convert.ToDateTime(row("STARTTIME").ToString().Trim)
                'Dim endshift As DateTime = Convert.ToDateTime(row("ENDTIME").ToString().Trim)
                'If startshift > endshift Then
                '    endshift = endshift.AddDays(1)
                '    If startlunch < startshift Then
                '        startlunch.AddDays(1)
                '    End If
                '    'MsgBox(startshift.ToString & "  " & endshift.ToString & "  " & startlunch.ToString)

                'End If
                ''MsgBox(startlunch.ToString)
                'If startlunch < startshift Or startlunch > endshift Then
                '    Dim a As DateTime = Convert.ToDateTime(Now.ToString("yyyy-MM-dd ") & "00:00:00")
                '    'MsgBox(a.ToString)
                '    view.SetRowCellValue(view.FocusedRowHandle, "LUNCHTIME", a)
                'End If

                Dim lunchdur As DateTime = Convert.ToDateTime(row("LUNCHDURATION").ToString().Trim)
                Dim lunchmin As Integer = (lunchdur.Hour * 60) + (lunchdur.Minute)
                Dim endlunch As DateTime = startlunch.AddMinutes(lunchmin)
                view.SetRowCellValue(view.FocusedRowHandle, "LUNCHENDTIME", endlunch)
            End If
            If e.Column.FieldName = "LUNCHDURATION" Then
                Dim startlunch As DateTime = Convert.ToDateTime(row("LUNCHTIME").ToString().Trim)
                Dim lunchdur As DateTime = Convert.ToDateTime(row("LUNCHDURATION").ToString().Trim)
                Dim lunchmin As Integer = (lunchdur.Hour * 60) + (lunchdur.Minute)
                Dim endlunch As DateTime = startlunch.AddMinutes(lunchmin)
                view.SetRowCellValue(view.FocusedRowHandle, "LUNCHENDTIME", endlunch)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub GridView1_EditFormPrepared(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormPreparedEventArgs) Handles GridView1.EditFormPrepared
        For Each control As Control In e.Panel.Controls
            For Each button As Control In control.Controls
                If (button.Text = "Update") Then
                    button.Text = Common.res_man.GetString("save", Common.cul)
                End If
                If (button.Text = "Cancel") Then
                    button.Text = Common.res_man.GetString("cancel", Common.cul)
                End If
            Next
        Next
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            e.BindableControls(colSHIFT).Enabled = True 'e.RowHandle Mod 2 = 0
        Else
            e.BindableControls(colSHIFT).Enabled = False
        End If
    End Sub
    Private Sub GridView1_ShowingPopupEditForm(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.ShowingPopupEditFormEventArgs) Handles GridView1.ShowingPopupEditForm
        For Each control As Control In e.EditForm.Controls
            Common.SetFont(control, 9)
        Next control
        e.EditForm.StartPosition = FormStartPosition.CenterParent
    End Sub
End Class
