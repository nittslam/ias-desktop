﻿Imports System.IO
Imports System.Resources
Imports System.Globalization
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors

Public Class XtraCompany
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Public Sub New()
        InitializeComponent()
        If Common.servername = "Access" Then
            ''ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            'Me.TblCompany1TableAdapter1.Fill(Me.SSSDBDataSet.tblCompany1)
            'GridControl1.DataSource = SSSDBDataSet.tblCompany1
        Else
            ''ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
            'TblCompanyTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            'Me.TblCompanyTableAdapter.Fill(Me.SSSDBDataSet.tblCompany)
            'GridControl1.DataSource = SSSDBDataSet.tblCompany
        End If

        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        SplitContainerControl1.Width = SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = (SplitContainerControl1.Parent.Width) * 85 / 100
        'not to do in all pages
        Common.splitforMasterMenuWidth = SplitContainerControl1.Width
        Common.SplitterPosition = SplitContainerControl1.SplitterPosition

        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub
    Private Sub XtraCompany_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'DevExpress.XtraGrid.Localization.GridLocalizer.Active = New MyLocalizer 'for e.error text for grid validation
        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        'Me.Width = My.Computer.Screen.WorkingArea.Width
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        SplitContainerControl1.Width = SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = (SplitContainerControl1.Parent.Width) * 85 / 100
        'not to do in all pages
        Common.splitforMasterMenuWidth = SplitContainerControl1.Width
        Common.SplitterPosition = SplitContainerControl1.SplitterPosition

        'Common.res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraCompany).Assembly)
        'Common.cul = CultureInfo.CreateSpecificCulture("en")

        GridView1.Columns.Item(0).Caption = Common.res_man.GetString("companycode", Common.cul)
        GridView1.Columns.Item(1).Caption = Common.res_man.GetString("companyname", Common.cul)
        GridView1.Columns.Item(2).Caption = Common.res_man.GetString("companyaddress", Common.cul)
        GridView1.Columns.Item(3).Caption = Common.res_man.GetString("companyshortname", Common.cul)
        GridView1.Columns.Item(4).Caption = Common.res_man.GetString("compan", Common.cul)
        GridView1.Columns.Item(5).Caption = Common.res_man.GetString("comtan", Common.cul)
        GridView1.Columns.Item("LCNO").Caption = Common.res_man.GetString("comlc", Common.cul)
        GridView1.Columns.Item("PFNO").Caption = Common.res_man.GetString("compf", Common.cul)
        Common.loadCompany()
        GridControl1.DataSource = Common.CompanyNonAdmin

        If Common.CompDel <> "Y" Then
            GridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = False
        Else
            GridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = True
        End If
        'LoadCompGrid()
    End Sub
    Private Sub LoadCompGrid()
        If Common.USERTYPE = "H" Then
            Dim com() As String = Common.auth_comp.Split(",")
            Dim ls As New List(Of String)()
            For x As Integer = 0 To com.Length - 1
                ls.Add(com(x).Trim)
            Next
            'select PAYCODE from TblEmployee where COMPANYCODE IN ('01')  and ACTIVE = 'Y'
            Dim gridselet As String = "select * from tblCompany where COMPANYCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
            If Common.servername = "Access" Then
                Dim dataAdapter As New OleDbDataAdapter(gridselet, Common.con1)
                Dim WTDataTable As New DataTable("tblCompany")
                dataAdapter.Fill(WTDataTable)
                GridControl1.DataSource = WTDataTable
            Else
                Dim dataAdapter As New SqlClient.SqlDataAdapter(gridselet, Common.con)
                Dim WTDataTable As New DataTable("tblCompany")
                dataAdapter.Fill(WTDataTable)
                GridControl1.DataSource = WTDataTable
            End If
        End If
    End Sub
    Private Sub GridView1_InitNewRow(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles GridView1.InitNewRow
        Dim view As GridView = CType(sender, GridView)
        view.SetRowCellValue(e.RowHandle, "LastModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
        view.SetRowCellValue(e.RowHandle, "LastModifiedBy", "admin")
    End Sub
    Private Sub GridView1_RowDeleted(sender As System.Object, e As DevExpress.Data.RowDeletedEventArgs) Handles GridView1.RowDeleted
        Me.TblCompanyTableAdapter.Update(Me.SSSDBDataSet.tblCompany)
        Me.TblCompany1TableAdapter1.Update(Me.SSSDBDataSet.tblCompany1)
        'XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))
    End Sub
    Private Sub GridView1_RowUpdated(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles GridView1.RowUpdated
        Me.TblCompanyTableAdapter.Update(Me.SSSDBDataSet.tblCompany)
        Me.TblCompany1TableAdapter1.Update(Me.SSSDBDataSet.tblCompany1)
        'Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        'Dim CellId As String = row("COMPANYCODE").ToString.Trim
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            Common.LogPost("Company Add; ") 'COMPANYCODE= '" & CellId)
        Else
            Common.LogPost("Company Update;") ' COMPANYCODE= '" & CellId)
        End If
    End Sub
    Private Sub GridView1_ValidateRow(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles GridView1.ValidateRow
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Dim CellId As String = row(0).ToString.Trim
        If CellId = "" Then
            e.Valid = False
            e.ErrorText = "<size=10>" & Common.res_man.GetString("companycode", Common.cul) & " " & Common.res_man.GetString("cannot_be_empty", Common.cul) & ","
        End If
        Dim cellname As String = row(1).ToString.Trim
        If cellname = "" Then
            e.Valid = False
            'XtraMessageBox.Show(ulf, "<size=10>Company Name cannot empty</size>", "<size=9>Error</size>", MessageBoxButtons.OK, MessageBoxIcon.Error)
            e.ErrorText = "<size=10>" & Common.res_man.GetString("companyname", Common.cul) & " " & Common.res_man.GetString("cannot_be_empty", Common.cul) & "</size>" & ","
        End If
        Dim ssql As String
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet
            ds = New DataSet
            ssql = "select COMPANYCODE from tblCompany where COMPANYCODE = '" & row(0).ToString & "'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                e.Valid = False
                e.ErrorText = "<size=10>Duplicate Company Code ,"
                Exit Sub
            End If
        End If
        'If Common.USERTYPE = "H" Then
        '
        'Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            'insert
            ssql = "insert into tblCompany (COMPANYNAME, COMPANYADDRESS, SHORTNAME, PANNUM, TANNUMBER, LCNO, PFNO, GSTIN, LastModifiedBy, LastModifiedDate, COMPANYCODE) values ('" & row("COMPANYNAME").ToString & "', '" & row("COMPANYADDRESS").ToString & "', '" & row("SHORTNAME").ToString & "', '" & row("PANNUM").ToString & "', '" & row("TANNUMBER").ToString & "', '" & row("LCNO").ToString & "', '" & row("PFNO").ToString & "', '" & row("GSTIN").ToString & "', '" & Common.USER_R & "', '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "', '" & row("COMPANYCODE").ToString & "')"
        Else
            'update
            ssql = "update tblCompany SET COMPANYNAME='" & row("COMPANYNAME").ToString & "', COMPANYADDRESS='" & row("COMPANYADDRESS").ToString & "', SHORTNAME='" & row("SHORTNAME").ToString & "', PANNUM='" & row("PANNUM").ToString & "', TANNUMBER='" & row("TANNUMBER").ToString & "', LCNO='" & row("LCNO").ToString & "', PFNO='" & row("PFNO").ToString & "', GSTIN='" & row("GSTIN").ToString & "', LastModifiedBy='" & Common.USER_R & "', LastModifiedDate='" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "' where COMPANYCODE = '" & row("COMPANYCODE").ToString & "'"
        End If
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(ssql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(ssql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        'LoadCompGrid()
        'End If
        Dim view As GridView = CType(sender, GridView)
        view.SetRowCellValue(e.RowHandle, "LastModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
        view.SetRowCellValue(e.RowHandle, "LastModifiedBy", "admin")
    End Sub
    Private Sub GridControl1_EmbeddedNavigator_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControl1.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                Me.Validate()
                e.Handled = True
                'MsgBox("Your records have been saved and updated successfully!")
            Else
                Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
                Dim CellId As String = row("COMPANYCODE").ToString.Trim

                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter
                Dim ds As DataSet
                Dim sSql As String = "select count(*) from TblEmployee where COMPANYCODE = '" & CellId & "'"
                ds = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows(0).Item(0).ToString > 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Company already assigned to Employee. Cannot delete.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Me.Validate()
                    e.Handled = True
                    Exit Sub
                Else
                    Try
                        sSql = "Delete from tblCompany where COMPANYCODE='" & CellId & "'"
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            If Common.con.State <> ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If
                    Catch ex As Exception

                    End Try
                    'LoadCompGrid()
                    Common.LogPost("Company Delete; COMPANYCODE:" & CellId)
                End If
                XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))
            End If
        End If
    End Sub
    Private Sub GridView1_EditFormPrepared(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormPreparedEventArgs) Handles GridView1.EditFormPrepared
         For Each control As Control In e.Panel.Controls
            For Each button As Control In control.Controls
                If (button.Text = "Update") Then
                    button.Text = Common.res_man.GetString("save", Common.cul)
                End If
                If (button.Text = "Cancel") Then
                    button.Text = Common.res_man.GetString("cancel", Common.cul)
                End If
            Next
        Next

        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            e.BindableControls(colCOMPANYCODE).Enabled = True 'e.RowHandle Mod 2 = 0
        Else
            e.BindableControls(colCOMPANYCODE).Enabled = False
        End If
    End Sub
    Private Sub GridView1_ShowingPopupEditForm(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.ShowingPopupEditFormEventArgs) Handles GridView1.ShowingPopupEditForm
        For Each control As Control In e.EditForm.Controls
            Common.SetFont(control, 9)
        Next control
        e.EditForm.StartPosition = FormStartPosition.CenterParent
        'Common.loadCompany()
        'GridControl1.DataSource = Common.CompanyNonAdmin
    End Sub
    Private Sub GridView1_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView1.EditFormShowing
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            If Common.CompAdd <> "Y" Then
                e.Allow = False
                Exit Sub
            End If
        Else
            If Common.CompModi <> "Y" Then
                e.Allow = False
                Exit Sub
            End If
        End If
      
    End Sub
    Private Sub BarButtonItem1_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem1.ItemClick
        XtraCompanyLogoUpload.ShowDialog()
    End Sub
End Class
