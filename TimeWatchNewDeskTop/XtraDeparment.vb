﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports System.Data.SqlClient
Imports System.Data.OleDb

Public Class XtraDeparment
    'Dim res_man As ResourceManager     'declare Resource manager to access to specific cultureinfo
    'Dim cul As CultureInfo     'declare culture info
    'Dim servername As String
    'Dim ConnectionString As String
    Dim ulf As UserLookAndFeel
    Public Sub New()
        InitializeComponent()
        If Common.servername = "Access" Then
            'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            Me.TblDepartment1TableAdapter1.Fill(Me.SSSDBDataSet.tblDepartment1)
            GridControl1.DataSource = SSSDBDataSet.tblDepartment1
        Else
            'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
            TblDepartmentTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            Me.TblDepartmentTableAdapter.Fill(Me.SSSDBDataSet.tblDepartment)
            GridControl1.DataSource = SSSDBDataSet.tblDepartment
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub
    Private Sub XtraDeparment_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        'res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraDeparment).Assembly)
        'cul = CultureInfo.CreateSpecificCulture("en")

        GridView1.Columns.Item(0).Caption = Common.res_man.GetString("deparmentcode", Common.cul)
        GridView1.Columns.Item(1).Caption = Common.res_man.GetString("departmentname", Common.cul)
        GridView1.Columns.Item(2).Caption = Common.res_man.GetString("departmenthead", Common.cul)
        GridView1.Columns.Item(3).Caption = Common.res_man.GetString("email", Common.cul)

        If Common.DeptDel <> "Y" Then
            GridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = False
        Else
            GridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = True
        End If

    End Sub
    Private Sub GridView1_InitNewRow(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles GridView1.InitNewRow
        Dim view As GridView = CType(sender, GridView)
        view.SetRowCellValue(e.RowHandle, "LastModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
        view.SetRowCellValue(e.RowHandle, "LastModifiedBy", "admin")
    End Sub
    Private Sub GridView1_RowDeleted(sender As System.Object, e As DevExpress.Data.RowDeletedEventArgs) Handles GridView1.RowDeleted
        Me.TblDepartmentTableAdapter.Update(Me.SSSDBDataSet.tblDepartment)
        Me.TblDepartment1TableAdapter1.Update(Me.SSSDBDataSet.tblDepartment1)
        XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))
    End Sub
    Private Sub GridView1_RowUpdated(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles GridView1.RowUpdated
        Me.TblDepartmentTableAdapter.Update(Me.SSSDBDataSet.tblDepartment)
        Me.TblDepartment1TableAdapter1.Update(Me.SSSDBDataSet.tblDepartment1)
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            Common.LogPost("Department Add; ") 'COMPANYCODE= '" & CellId)
        Else
            Common.LogPost("Department Update;") ' COMPANYCODE= '" & CellId)
        End If
    End Sub
    Private Sub GridView1_ValidateRow(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles GridView1.ValidateRow
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Dim CellId As String = row(0).ToString.Trim
        If CellId = "" Then
            e.Valid = False
            e.ErrorText = "<size=10>" & Common.res_man.GetString("deparmentcode", Common.cul) & " " & Common.res_man.GetString("cannot_be_empty", Common.cul) & ","
        End If
        Dim cellname As String = row(1).ToString.Trim
        If cellname = "" Then
            e.Valid = False
            e.ErrorText = "<size=10>" & Common.res_man.GetString("departmentname", Common.cul) & " " & Common.res_man.GetString("cannot_be_empty", Common.cul) & ","
        End If
        Dim cellemail As String = row(3).ToString
        If cellemail.Length > 0 Then
            If ValidateEmail(cellemail) = False Then
                e.Valid = False
                e.ErrorText = "<size=10>" & Common.res_man.GetString("invalidemail", Common.cul) & ","
            End If
        Else

        End If
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet = New DataSet
            Dim sSql As String = "select DEPARTMENTCODE from tblDepartment where DEPARTMENTCODE = '" & row(0).ToString & "'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                e.Valid = False
                e.ErrorText = "<size=10>Duplicate Department Code ,"
            End If
        End If

        Dim view As GridView = CType(sender, GridView)
        view.SetRowCellValue(e.RowHandle, "LastModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
        view.SetRowCellValue(e.RowHandle, "LastModifiedBy", "admin")
    End Sub
    Private Sub GridControl1_EmbeddedNavigator_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControl1.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "<size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                Me.Validate()
                e.Handled = True
                'MsgBox("Your records have been saved and updated successfully!")
            Else
                Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
                Dim CellId As String = row("DEPARTMENTCODE").ToString.Trim

                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter
                Dim ds As DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter("select count(*) from TblEmployee where DEPARTMENTCODE = '" & CellId & "'", Common.con1)
                    ds = New DataSet
                    adapA.Fill(ds)
                    If ds.Tables(0).Rows(0).Item(0).ToString > 0 Then
                        XtraMessageBox.Show(ulf, "<size=10>Department already assigned to Employee. Cannot delete.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Me.Validate()
                        e.Handled = True
                        Exit Sub
                    End If
                Else
                    adap = New SqlDataAdapter("select count(*) from TblEmployee where DEPARTMENTCODE = '" & CellId & "'", Common.con)
                    ds = New DataSet
                    adap.Fill(ds)
                    If ds.Tables(0).Rows(0).Item(0).ToString > 0 Then
                        XtraMessageBox.Show(ulf, "<size=10>Department already assigned to Employee. Cannot delete.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Me.Validate()
                        e.Handled = True
                        Exit Sub
                    End If
                End If
                Common.LogPost("Department Delete; Department ID: " & CellId)
            End If
        End If
    End Sub
    Private Sub GridView1_EditFormPrepared(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormPreparedEventArgs) Handles GridView1.EditFormPrepared
        For Each control As Control In e.Panel.Controls
            For Each button As Control In control.Controls
                If (button.Text = "Update") Then
                    button.Text = Common.res_man.GetString("save", Common.cul)
                End If
                If (button.Text = "Cancel") Then
                    button.Text = Common.res_man.GetString("cancel", Common.cul)
                End If
            Next
        Next
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            e.BindableControls(colDEPARTMENTCODE).Enabled = True 'e.RowHandle Mod 2 = 0
        Else
            e.BindableControls(colDEPARTMENTCODE).Enabled = False
        End If
    End Sub
    Private Sub GridView1_ShowingPopupEditForm(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.ShowingPopupEditFormEventArgs) Handles GridView1.ShowingPopupEditForm
        For Each control As Control In e.EditForm.Controls
            Common.SetFont(control, 9)
        Next control
        e.EditForm.StartPosition = FormStartPosition.CenterParent
    End Sub
    Public Function ValidateEmail(ByVal strCheck As String) As Boolean
        Try
            Dim vEmailAddress As New System.Net.Mail.MailAddress(strCheck)
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
    Private Sub GridView1_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView1.EditFormShowing
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            If Common.DeptAdd <> "Y" Then
                e.Allow = False
                Exit Sub
            End If
        Else
            If Common.DeptModi <> "Y" Then
                e.Allow = False
                Exit Sub
            End If
        End If
    End Sub
End Class
