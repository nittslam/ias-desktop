﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Base
Imports System.Data.SqlClient
Imports System.Data.OleDb

Public Class XtraVisitorGrid
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Public Shared VISIT_NO As String
    Public Sub New()
        InitializeComponent()
        If Common.servername = "Access" Then
            'Me.TblVisitorTransaction1TableAdapter1.Fill(Me.SSSDBDataSet.tblVisitorTransaction1)
            'GridControl1.DataSource = SSSDBDataSet.tblVisitorTransaction1
        Else
            TblVisitorTransactionTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            'Me.TblVisitorTransactionTableAdapter.Fill(Me.SSSDBDataSet.tblVisitorTransaction)
            'GridControl1.DataSource = SSSDBDataSet.tblVisitorTransaction
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub
    Private Sub XtraVisitorGrid_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width
        If Common.servername = "Access" Then
            Me.TblVisitorTransaction1TableAdapter1.Fill(Me.SSSDBDataSet.tblVisitorTransaction1)
            GridControl1.DataSource = SSSDBDataSet.tblVisitorTransaction1
        Else
            Me.TblVisitorTransactionTableAdapter.Fill(Me.SSSDBDataSet.tblVisitorTransaction)
            GridControl1.DataSource = SSSDBDataSet.tblVisitorTransaction
        End If
    End Sub

    Private Sub BarButtonItem3_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem3.ItemClick
        Dim mstrFile_Name As String = My.Application.Info.DirectoryPath & "\Reports\TimeWatch_Visitor.xlsx"
        GridControl1.ExportToXlsx(mstrFile_Name)
        Process.Start(mstrFile_Name)
    End Sub
    Private Sub GridView1_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView1.EditFormShowing
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Try
            VISIT_NO = row("VISIT_NO").ToString.Trim
        Catch ex As Exception
            VISIT_NO = ""
        End Try
        e.Allow = False
        XtraVisitorOut.ShowDialog()
        If Common.servername = "Access" Then
            Me.TblVisitorTransaction1TableAdapter1.Fill(Me.SSSDBDataSet.tblVisitorTransaction1)
            GridControl1.DataSource = SSSDBDataSet.tblVisitorTransaction1
        Else
            Me.TblVisitorTransactionTableAdapter.Fill(Me.SSSDBDataSet.tblVisitorTransaction)
            GridControl1.DataSource = SSSDBDataSet.tblVisitorTransaction
        End If
    End Sub
End Class
