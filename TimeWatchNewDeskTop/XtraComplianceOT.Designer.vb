﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraComplianceOT
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.mskOtperDay = New DevExpress.XtraEditors.TextEdit()
        Me.mskOtperWeek = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.mskOtMonthly = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.btnSave = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.mskOtperDay.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mskOtperWeek.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mskOtMonthly.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(63, 14)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "OT Per Day"
        '
        'mskOtperDay
        '
        Me.mskOtperDay.Location = New System.Drawing.Point(96, 10)
        Me.mskOtperDay.Name = "mskOtperDay"
        Me.mskOtperDay.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.mskOtperDay.Properties.Appearance.Options.UseFont = True
        Me.mskOtperDay.Properties.Mask.EditMask = "[0-9]*"
        Me.mskOtperDay.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.mskOtperDay.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.mskOtperDay.Properties.MaxLength = 2
        Me.mskOtperDay.Size = New System.Drawing.Size(100, 20)
        Me.mskOtperDay.TabIndex = 1
        '
        'mskOtperWeek
        '
        Me.mskOtperWeek.Location = New System.Drawing.Point(96, 36)
        Me.mskOtperWeek.Name = "mskOtperWeek"
        Me.mskOtperWeek.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.mskOtperWeek.Properties.Appearance.Options.UseFont = True
        Me.mskOtperWeek.Properties.Mask.EditMask = "[0-9]*"
        Me.mskOtperWeek.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.mskOtperWeek.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.mskOtperWeek.Properties.MaxLength = 2
        Me.mskOtperWeek.Size = New System.Drawing.Size(100, 20)
        Me.mskOtperWeek.TabIndex = 3
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(12, 38)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(76, 14)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "OT Per Week"
        '
        'mskOtMonthly
        '
        Me.mskOtMonthly.Location = New System.Drawing.Point(96, 62)
        Me.mskOtMonthly.Name = "mskOtMonthly"
        Me.mskOtMonthly.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.mskOtMonthly.Properties.Appearance.Options.UseFont = True
        Me.mskOtMonthly.Properties.Mask.EditMask = "[0-9]*"
        Me.mskOtMonthly.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.mskOtMonthly.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.mskOtMonthly.Properties.MaxLength = 2
        Me.mskOtMonthly.Size = New System.Drawing.Size(100, 20)
        Me.mskOtMonthly.TabIndex = 5
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(12, 64)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(78, 14)
        Me.LabelControl3.TabIndex = 4
        Me.LabelControl3.Text = "OT Per Month"
        '
        'btnSave
        '
        Me.btnSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.btnSave.Appearance.Options.UseFont = True
        Me.btnSave.Location = New System.Drawing.Point(42, 88)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 25)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "Save"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(123, 88)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 25)
        Me.SimpleButton1.TabIndex = 7
        Me.SimpleButton1.Text = "Cancel"
        '
        'XtraComplianceOT
        '
        Me.AcceptButton = Me.btnSave
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(244, 149)
        Me.ControlBox = False
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.mskOtMonthly)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.mskOtperWeek)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.mskOtperDay)
        Me.Controls.Add(Me.LabelControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraComplianceOT"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "OT SetUp"
        CType(Me.mskOtperDay.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mskOtperWeek.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mskOtMonthly.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents mskOtperDay As DevExpress.XtraEditors.TextEdit
    Friend WithEvents mskOtperWeek As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents mskOtMonthly As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
End Class
