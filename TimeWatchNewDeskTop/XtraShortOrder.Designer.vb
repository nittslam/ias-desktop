﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraShortOrder
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.CheckCatName = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckCatagoryCard = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckCatagoryPaycode = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckSectionName = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckSectionCard = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckSectionPay = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckDeptName = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckDeptCardNum = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckDeptPayCode = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEmpName = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckCardNum = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckPayCode = New DevExpress.XtraEditors.CheckEdit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.CheckCatName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckCatagoryCard.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckCatagoryPaycode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckSectionName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckSectionCard.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckSectionPay.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckDeptName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckDeptCardNum.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckDeptPayCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEmpName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckCardNum.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckPayCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl1.Controls.Add(Me.SimpleButton2)
        Me.PanelControl1.Controls.Add(Me.SimpleButton1)
        Me.PanelControl1.Controls.Add(Me.CheckCatName)
        Me.PanelControl1.Controls.Add(Me.CheckCatagoryCard)
        Me.PanelControl1.Controls.Add(Me.CheckCatagoryPaycode)
        Me.PanelControl1.Controls.Add(Me.CheckSectionName)
        Me.PanelControl1.Controls.Add(Me.CheckSectionCard)
        Me.PanelControl1.Controls.Add(Me.CheckSectionPay)
        Me.PanelControl1.Controls.Add(Me.CheckDeptName)
        Me.PanelControl1.Controls.Add(Me.CheckDeptCardNum)
        Me.PanelControl1.Controls.Add(Me.CheckDeptPayCode)
        Me.PanelControl1.Controls.Add(Me.CheckEmpName)
        Me.PanelControl1.Controls.Add(Me.CheckCardNum)
        Me.PanelControl1.Controls.Add(Me.CheckPayCode)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(228, 286)
        Me.PanelControl1.TabIndex = 0
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Location = New System.Drawing.Point(93, 239)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton2.TabIndex = 36
        Me.SimpleButton2.Text = "Cancel"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(12, 239)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 35
        Me.SimpleButton1.Text = "Ok"
        '
        'CheckCatName
        '
        Me.CheckCatName.Location = New System.Drawing.Point(12, 212)
        Me.CheckCatName.Name = "CheckCatName"
        Me.CheckCatName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckCatName.Properties.Appearance.Options.UseFont = True
        Me.CheckCatName.Properties.Caption = "Catagory + Name"
        Me.CheckCatName.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckCatName.Properties.RadioGroupIndex = 0
        Me.CheckCatName.Size = New System.Drawing.Size(200, 19)
        Me.CheckCatName.TabIndex = 34
        Me.CheckCatName.TabStop = False
        '
        'CheckCatagoryCard
        '
        Me.CheckCatagoryCard.Location = New System.Drawing.Point(12, 187)
        Me.CheckCatagoryCard.Name = "CheckCatagoryCard"
        Me.CheckCatagoryCard.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckCatagoryCard.Properties.Appearance.Options.UseFont = True
        Me.CheckCatagoryCard.Properties.Caption = "Catagory + Card Number"
        Me.CheckCatagoryCard.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckCatagoryCard.Properties.RadioGroupIndex = 0
        Me.CheckCatagoryCard.Size = New System.Drawing.Size(200, 19)
        Me.CheckCatagoryCard.TabIndex = 33
        Me.CheckCatagoryCard.TabStop = False
        '
        'CheckCatagoryPaycode
        '
        Me.CheckCatagoryPaycode.Location = New System.Drawing.Point(12, 162)
        Me.CheckCatagoryPaycode.Name = "CheckCatagoryPaycode"
        Me.CheckCatagoryPaycode.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckCatagoryPaycode.Properties.Appearance.Options.UseFont = True
        Me.CheckCatagoryPaycode.Properties.Caption = "Catagory + Paycode"
        Me.CheckCatagoryPaycode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckCatagoryPaycode.Properties.RadioGroupIndex = 0
        Me.CheckCatagoryPaycode.Size = New System.Drawing.Size(200, 19)
        Me.CheckCatagoryPaycode.TabIndex = 32
        Me.CheckCatagoryPaycode.TabStop = False
        '
        'CheckSectionName
        '
        Me.CheckSectionName.Location = New System.Drawing.Point(176, 171)
        Me.CheckSectionName.Name = "CheckSectionName"
        Me.CheckSectionName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckSectionName.Properties.Appearance.Options.UseFont = True
        Me.CheckSectionName.Properties.Caption = "Section + Name"
        Me.CheckSectionName.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckSectionName.Properties.RadioGroupIndex = 0
        Me.CheckSectionName.Size = New System.Drawing.Size(200, 19)
        Me.CheckSectionName.TabIndex = 31
        Me.CheckSectionName.TabStop = False
        Me.CheckSectionName.Visible = False
        '
        'CheckSectionCard
        '
        Me.CheckSectionCard.Location = New System.Drawing.Point(176, 146)
        Me.CheckSectionCard.Name = "CheckSectionCard"
        Me.CheckSectionCard.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckSectionCard.Properties.Appearance.Options.UseFont = True
        Me.CheckSectionCard.Properties.Caption = "Section + Card Number"
        Me.CheckSectionCard.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckSectionCard.Properties.RadioGroupIndex = 0
        Me.CheckSectionCard.Size = New System.Drawing.Size(200, 19)
        Me.CheckSectionCard.TabIndex = 30
        Me.CheckSectionCard.TabStop = False
        Me.CheckSectionCard.Visible = False
        '
        'CheckSectionPay
        '
        Me.CheckSectionPay.Location = New System.Drawing.Point(176, 121)
        Me.CheckSectionPay.Name = "CheckSectionPay"
        Me.CheckSectionPay.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckSectionPay.Properties.Appearance.Options.UseFont = True
        Me.CheckSectionPay.Properties.Caption = "Section + Paycode"
        Me.CheckSectionPay.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckSectionPay.Properties.RadioGroupIndex = 0
        Me.CheckSectionPay.Size = New System.Drawing.Size(200, 19)
        Me.CheckSectionPay.TabIndex = 29
        Me.CheckSectionPay.TabStop = False
        Me.CheckSectionPay.Visible = False
        '
        'CheckDeptName
        '
        Me.CheckDeptName.Location = New System.Drawing.Point(12, 137)
        Me.CheckDeptName.Name = "CheckDeptName"
        Me.CheckDeptName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckDeptName.Properties.Appearance.Options.UseFont = True
        Me.CheckDeptName.Properties.Caption = "Department + Name"
        Me.CheckDeptName.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckDeptName.Properties.RadioGroupIndex = 0
        Me.CheckDeptName.Size = New System.Drawing.Size(200, 19)
        Me.CheckDeptName.TabIndex = 28
        Me.CheckDeptName.TabStop = False
        '
        'CheckDeptCardNum
        '
        Me.CheckDeptCardNum.Location = New System.Drawing.Point(12, 112)
        Me.CheckDeptCardNum.Name = "CheckDeptCardNum"
        Me.CheckDeptCardNum.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckDeptCardNum.Properties.Appearance.Options.UseFont = True
        Me.CheckDeptCardNum.Properties.Caption = "Department + Card Number"
        Me.CheckDeptCardNum.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckDeptCardNum.Properties.RadioGroupIndex = 0
        Me.CheckDeptCardNum.Size = New System.Drawing.Size(200, 19)
        Me.CheckDeptCardNum.TabIndex = 27
        Me.CheckDeptCardNum.TabStop = False
        '
        'CheckDeptPayCode
        '
        Me.CheckDeptPayCode.Location = New System.Drawing.Point(12, 87)
        Me.CheckDeptPayCode.Name = "CheckDeptPayCode"
        Me.CheckDeptPayCode.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckDeptPayCode.Properties.Appearance.Options.UseFont = True
        Me.CheckDeptPayCode.Properties.Caption = "Department + Paycode"
        Me.CheckDeptPayCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckDeptPayCode.Properties.RadioGroupIndex = 0
        Me.CheckDeptPayCode.Size = New System.Drawing.Size(200, 19)
        Me.CheckDeptPayCode.TabIndex = 26
        Me.CheckDeptPayCode.TabStop = False
        '
        'CheckEmpName
        '
        Me.CheckEmpName.Location = New System.Drawing.Point(12, 62)
        Me.CheckEmpName.Name = "CheckEmpName"
        Me.CheckEmpName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEmpName.Properties.Appearance.Options.UseFont = True
        Me.CheckEmpName.Properties.Caption = "Employee Name"
        Me.CheckEmpName.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEmpName.Properties.RadioGroupIndex = 0
        Me.CheckEmpName.Size = New System.Drawing.Size(200, 19)
        Me.CheckEmpName.TabIndex = 25
        Me.CheckEmpName.TabStop = False
        '
        'CheckCardNum
        '
        Me.CheckCardNum.Location = New System.Drawing.Point(12, 37)
        Me.CheckCardNum.Name = "CheckCardNum"
        Me.CheckCardNum.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckCardNum.Properties.Appearance.Options.UseFont = True
        Me.CheckCardNum.Properties.Caption = "Card Number"
        Me.CheckCardNum.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckCardNum.Properties.RadioGroupIndex = 0
        Me.CheckCardNum.Size = New System.Drawing.Size(200, 19)
        Me.CheckCardNum.TabIndex = 24
        Me.CheckCardNum.TabStop = False
        '
        'CheckPayCode
        '
        Me.CheckPayCode.EditValue = True
        Me.CheckPayCode.Location = New System.Drawing.Point(12, 12)
        Me.CheckPayCode.Name = "CheckPayCode"
        Me.CheckPayCode.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckPayCode.Properties.Appearance.Options.UseFont = True
        Me.CheckPayCode.Properties.Caption = "Pay Code"
        Me.CheckPayCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckPayCode.Properties.RadioGroupIndex = 0
        Me.CheckPayCode.Size = New System.Drawing.Size(200, 19)
        Me.CheckPayCode.TabIndex = 23
        '
        'XtraShortOrder
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(228, 286)
        Me.ControlBox = False
        Me.Controls.Add(Me.PanelControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraShortOrder"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Select Sorting Option"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.CheckCatName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckCatagoryCard.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckCatagoryPaycode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckSectionName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckSectionCard.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckSectionPay.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckDeptName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckDeptCardNum.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckDeptPayCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEmpName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckCardNum.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckPayCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents CheckCatName As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckCatagoryCard As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckCatagoryPaycode As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckSectionName As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckSectionCard As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckSectionPay As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckDeptName As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckDeptCardNum As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckDeptPayCode As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEmpName As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckCardNum As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckPayCode As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
End Class
