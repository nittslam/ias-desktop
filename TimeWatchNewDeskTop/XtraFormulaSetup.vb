﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraEditors.Repository
Imports System.Data.SqlClient
Imports System.Data.OleDb
Public Class XtraFormulaSetup
    Dim ulf As UserLookAndFeel
    Public Shared FormulaId As String
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Public Sub New()
        InitializeComponent()
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        If Common.servername = "Access" Then
            Me.PaY_FORMULA1TableAdapter1.Fill(Me.SSSDBDataSet.PAY_FORMULA1)
            GridControl1.DataSource = SSSDBDataSet.PAY_FORMULA1
        Else
            PAY_FORMULATableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.PAY_FORMULATableAdapter.Fill(Me.SSSDBDataSet.PAY_FORMULA)
            GridControl1.DataSource = SSSDBDataSet.PAY_FORMULA
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub
    Private Sub XtraFormulaSetup_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        If Common.servername = "Access" Then
            Me.PaY_FORMULA1TableAdapter1.Fill(Me.SSSDBDataSet.PAY_FORMULA1)
            GridControl1.DataSource = SSSDBDataSet.PAY_FORMULA1
        Else
            Me.PAY_FORMULATableAdapter.Fill(Me.SSSDBDataSet.PAY_FORMULA)
            GridControl1.DataSource = SSSDBDataSet.PAY_FORMULA
        End If
        colFORM.Width = GridControl1.Width - colCODE.Width - 20
    End Sub
    Private Sub GridView1_InitNewRow(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles GridView1.InitNewRow
        Dim view As GridView = CType(sender, GridView)
        view.SetRowCellValue(e.RowHandle, "LastModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
        view.SetRowCellValue(e.RowHandle, "LastModifiedBy", "admin")
    End Sub
    Private Sub GridView1_RowDeleted(sender As System.Object, e As DevExpress.Data.RowDeletedEventArgs) Handles GridView1.RowDeleted
        Me.PAY_FORMULATableAdapter.Update(Me.SSSDBDataSet.PAY_FORMULA)
        Me.PaY_FORMULA1TableAdapter1.Update(Me.SSSDBDataSet.PAY_FORMULA1)
        XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))
    End Sub
    Private Sub GridView1_RowUpdated(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles GridView1.RowUpdated
        Me.PAY_FORMULATableAdapter.Update(Me.SSSDBDataSet.PAY_FORMULA)
        Me.PaY_FORMULA1TableAdapter1.Update(Me.SSSDBDataSet.PAY_FORMULA1)
    End Sub
    Private Sub GridControl1_EmbeddedNavigator_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControl1.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                Me.Validate()
                e.Handled = True
                'MsgBox("Your records have been saved and updated successfully!")
            Else
                Me.Validate()
                e.Handled = True
                Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
                Dim CellId As String = row("CODE").ToString.Trim
                Dim adapA As OleDbDataAdapter
                Dim adap As SqlDataAdapter
                Dim ds As DataSet = New DataSet
                Dim sSql As String
                sSql = "select * from PAY_SETUP where VDT_1 = '" & CellId & "' or  " & _
                    "VDT_2 = '" & CellId & "' or " & _
                    "VDT_3 = '" & CellId & "' or " & _
                    "VDT_4 = '" & CellId & "' or " & _
                    "VDT_5 = '" & CellId & "' or " & _
                    "VDT_6 = '" & CellId & "' or " & _
                    "VDT_7 = '" & CellId & "' or " & _
                    "VDT_8 = '" & CellId & "' or " & _
                    "VDT_9 = '" & CellId & "' or " & _
                    "VDT_10 = '" & CellId & "' or " & _
                    "VIT_1='" & CellId & "' or " & _
                    "VIT_2='" & CellId & "' or " & _
                    "VIT_3='" & CellId & "' or " & _
                    "VIT_4='" & CellId & "' or " & _
                    "VIT_5='" & CellId & "' or " & _
                    "VIT_6='" & CellId & "' or " & _
                    "VIT_7='" & CellId & "' or " & _
                    "VIT_8='" & CellId & "' or " & _
                    "VIT_9='" & CellId & "' or " & _
                    "VIT_10='" & CellId & "'"
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows.Count > 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Cannot delete because it has already assigned to Pay Setup</size>", "<size=9>Error<size/>")
                    Exit Sub
                End If

                sSql = "select PAYCODE from Pay_Master WHERE VOT_FLAG = '" & CellId & "' and VTDS_F = '" & CellId & "'"
                ds = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows.Count > 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Cannot delete because it has already Employee Setup</size>", "<size=9>Error<size/>")
                    Exit Sub
                End If

                sSql = "delete from PAY_FORMULA WHERE CODE='" & CellId & "'"
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
                If Common.servername = "Access" Then
                    Me.PaY_FORMULA1TableAdapter1.Fill(Me.SSSDBDataSet.PAY_FORMULA1)
                    GridControl1.DataSource = SSSDBDataSet.PAY_FORMULA1
                Else
                    Me.PAY_FORMULATableAdapter.Fill(Me.SSSDBDataSet.PAY_FORMULA)
                    GridControl1.DataSource = SSSDBDataSet.PAY_FORMULA
                End If
                XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))
            End If
        End If
    End Sub

    Private Sub GridView1_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView1.EditFormShowing
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Try
            FormulaId = row("CODE").ToString.Trim
        Catch ex As Exception
            FormulaId = ""
        End Try
        e.Allow = False
        XtraFormulaSetupEdit.ShowDialog()
        If Common.servername = "Access" Then   'to refresh the grid
            Me.PaY_FORMULA1TableAdapter1.Fill(Me.SSSDBDataSet.PAY_FORMULA1)
        Else
            Me.PAY_FORMULATableAdapter.Fill(Me.SSSDBDataSet.PAY_FORMULA)
        End If
    End Sub
End Class
