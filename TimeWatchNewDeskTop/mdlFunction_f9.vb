﻿Imports System.Runtime.InteropServices
Class mdlPublic_f9
    Public Declare Ansi Function FK_ConnectComm Lib "ComMain" ( _
     ByVal nMachineNo As Int32, _
     ByVal nComPort As Int32, _
     ByVal nBaudRate As Int32, _
     ByVal strTelNumber As String, _
     ByVal nWaitDialTime As Int32, _
     ByVal nLicense As Int32, _
     ByVal nComTimeOut As Int32 _
 ) As Int32

    Public Declare Ansi Function FK_ConnectNet Lib "ComMain" ( _
        ByVal nMachineNo As Int32, _
        ByVal strIpAddress As String, _
        ByVal nNetPort As Int32, _
        ByVal nTimeOut As Int32, _
        ByVal nProtocolType As Int32, _
        ByVal nNetPassword As Int32, _
        ByVal nLicense As Int32 _
    ) As Int32

    ''<DllImport("ComMain.dll", CharSet:=CharSet.Ansi)>
    'Public Declare Function FK_ConnectNet Lib "ComMain" (ByVal anMachineNo As Integer, ByVal astrIpAddress As String, ByVal anNetPort As Integer, ByVal anTimeOut As Integer, ByVal anProtocolType As Integer, ByVal anNetPassword As Integer, ByVal anLicense As Integer) As Integer

    Public Declare Ansi Function FK_ConnectUSB Lib "ComMain" ( _
        ByVal nMachineNo As Int32, _
        ByVal nLicense As Int32 _
    ) As Int32

    Public Declare Ansi Sub FK_DisConnect Lib "ComMain" (ByVal nHandleIndex As Int32)

    '///-- device setting
    Public Declare Ansi Function FK_EnableDevice Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnableFlag As Int32 _
    ) As Int32

    Public Declare Ansi Sub FK_PowerOnAllDevice Lib "ComMain" (ByVal nHandleIndex As Int32)

    Public Declare Ansi Function FK_PowerOffDevice Lib "ComMain" (ByVal nHandleIndex As Int32) As Int32

    Public Declare Ansi Function FK_GetDeviceStatus Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nStatusIndex As Int32, _
        ByRef nValue As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_GetDeviceTime Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByRef pnDateTime As Date _
    ) As Int32

    Public Declare Ansi Function FK_SetDeviceTime Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nDateTime As Date _
    ) As Int32

    Public Declare Ansi Function FK_GetDeviceInfo Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nInfoIndex As Int32, _
        ByRef pnValue As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetDeviceInfo Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nInfoIndex As Int32, _
        ByVal nValue As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_GetProductData Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nDataIndex As Int32, _
        ByRef strValue As String _
    ) As Int32

    '// -- log data
    Public Declare Ansi Function FK_LoadSuperLogData Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nReadMark As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_USBLoadSuperLogDataFromFile Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strFilePath As String _
    ) As Int32

    Public Declare Ansi Function FK_GetSuperLogData Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByRef nSEnrollNumber As UInt32, _
        ByRef nGEnrollNumber As UInt32, _
        ByRef nManipulation As Int32, _
        ByRef nBackupNumber As Int32, _
        ByRef dtLogTime As Date _
    ) As Int32

    Public Declare Ansi Function FK_EmptySuperLogData Lib "ComMain" (ByVal nHandleIndex As Int32) As Int32

    Public Declare Ansi Function FK_LoadGeneralLogData Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nReadMark As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_USBLoadGeneralLogDataFromFile Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strFilePath As String _
    ) As Int32

    Public Declare Ansi Function FK_GetGeneralLogData Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByRef nEnrollNumber As UInt32, _
        ByRef nVerifyMode As Int32, _
        ByRef nInOutMode As Int32, _
        ByRef dtLogTime As Date _
    ) As Int32

    Public Declare Ansi Function FK_EmptyGeneralLogData Lib "ComMain" (ByVal nHandleIndex As Int32) As Int32

    '// -- enroll data
    Public Declare Ansi Function FK_ReadAllUserID Lib "ComMain" (ByVal nHandleIndex As Int32) As Int32

    Public Declare Ansi Function FK_GetAllUserID Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByRef nEnrollNumber As UInt32, _
        ByRef nBackupNumber As Int32, _
        ByRef nMachinePrivilege As Int32, _
        ByRef nEnable As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_GetEnrollData Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByVal nBackupNumber As Int32, _
        ByRef nMachinePrivilege As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytEnrollData() As Byte, _
        ByRef nPassword As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_PutEnrollData Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByVal nBackupNumber As Int32, _
        ByVal nMachinePrivilege As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytEnrollData() As Byte, _
        ByVal nPassword As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SaveEnrollData Lib "ComMain" (ByVal nHandleIndex As Int32) As Int32

    Public Declare Ansi Function FK_DeleteEnrollData Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByVal nBackupNumber As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_EnableUser Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByVal nBackupNumber As Int32, _
        ByVal nEnableFlag As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_ModifyPrivilege Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByVal nBackupNumber As Int32, _
        ByVal nMachinePrivilege As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_BenumbAllManager Lib "ComMain" (ByVal nHandleIndex As Int32) As Int32

    Public Declare Ansi Function FK_EmptyEnrollData Lib "ComMain" (ByVal nHandleIndex As Int32) As Int32

    Public Declare Ansi Function FK_ClearKeeperData Lib "ComMain" (ByVal nHandleIndex As Int32) As Int32


    '// -- enroll data using u-disk file
    Public Declare Ansi Function FK_SetUDiskFileFKModel Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strFKModel As String _
    ) As Int32
    '// old deprecated function
    Public Declare Ansi Function FK_SetUSBModel Lib "ComMain" ( _
        ByVal nHandleIndex As Integer, _
        ByVal anModel As Integer _
    ) As Int32


    Public Declare Ansi Function FK_USBReadAllEnrollDataFromFile Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strFilePath As String _
    ) As Int32

    Public Declare Ansi Function FK_USBReadAllEnrollDataCount Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal pnValue As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_USBGetOneEnrollData Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByRef nEnrollNumber As UInt32, _
        ByRef nBackupNumber As Int32, _
        ByRef nMachinePrivilege As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytEnrollData() As Byte, _
        ByRef nPassWord As Int32, _
        ByRef nEnableFlag As Int32, _
        ByRef strEnrollName As String _
    ) As Int32

    Public Declare Ansi Function FK_USBSetOneEnrollData Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByVal nBackupNumber As Int32, _
        ByVal nMachinePrivilege As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytEnrollData() As Byte, _
        ByVal nPassWord As Int32, _
        ByVal nEnableFlag As Int32, _
        ByVal strEnrollName As String _
    ) As Int32

    Public Declare Ansi Function FK_USBWriteAllEnrollDataToFile Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strFilePath As String _
    ) As Int32

    '// -- bell info
    Public Declare Ansi Function FK_GetBellTime Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByRef nBellCount As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytBellInfo() As Byte _
    ) As Int32

    Public Declare Ansi Function FK_SetBellTime Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nBellCount As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytBellInfo() As Byte _
    ) As Int32

    '// -- user name, message
    Public Declare Ansi Function FK_GetUserName Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByRef strUserName As String _
    ) As Int32

    Public Declare Ansi Function FK_SetUserName Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByVal strUserName As String _
    ) As Int32

    Public Declare Ansi Function FK_GetNewsMessage Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nNewsId As Int32, _
        ByRef strNews As String _
    ) As Int32

    Public Declare Ansi Function FK_SetNewsMessage Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nNewsId As Int32, _
        ByVal strNews As String _
    ) As Int32

    Public Declare Ansi Function FK_GetUserNewsID Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByRef nNewsId As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetUserNewsID Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByVal nNewsId As Int32 _
    ) As Int32

    '// -- access control
    Public Declare Ansi Function FK_GetDoorStatus Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByRef nStatusVal As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetDoorStatus Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nStatusVal As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_GetPassTime Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nPassTimeID As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytPassTimeInfo() As Byte, _
        ByVal PassTimeInfoSize As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetPassTime Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nPassTimeID As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytPassTimeInfo() As Byte, _
        ByVal PassTimeInfoSize As Int32 _
    ) As Int32


    Public Declare Ansi Function FK_GetUserPassTime Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByRef nGroupID As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytUserPassTimeInfo() As Byte, _
        ByVal nUserPassTimeInfoSize As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetUserPassTime Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nEnrollNumber As UInt32, _
        ByVal nGroupID As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytUserPassTimeInfo() As Byte, _
        ByVal nUserPassTimeInfoSize As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_GetGroupPassTime Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nGroupID As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytGroupPassTimeInfo() As Byte, _
        ByVal nGroupPassTimeInfoSize As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetGroupPassTime Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal nGroupID As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytGroupPassTimeInfo() As Byte, _
        ByVal nGroupPassTimeInfoSize As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_GetGroupMatch Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytGroupMatchInfo() As Byte, _
        ByVal anGroupMatchInfoSize As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetGroupMatch Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytGroupMatchInfo() As Byte, _
        ByVal nGroupMatchInfoSize As Int32 _
    ) As Int32

    '// -- etc
    Public Declare Ansi Function FK_ConnectGetIP Lib "ComMain" (ByRef apnComName As String) As Long
    Public Declare Ansi Function FK_GetAdjustInfo Lib "ComMain" (ByVal nHandleIndex As Int32, ByRef dwAdjustedState As Int32, ByRef dwAdjustedMonth As Int32, ByRef dwAdjustedDay As Int32, ByRef dwAdjustedHour As Int32, ByRef dwAdjustedMinute As Int32, ByRef dwRestoredState As Int32, ByRef dwRestoredMonth As Int32, ByRef dwRestoredDay As Int32, ByRef dwRestoredHour As Int32, ByRef dwRestoredMinte As Int32) As Int32
    Public Declare Ansi Function FK_SetAdjustInfo Lib "ComMain" (ByVal nHandleIndex As Int32, ByVal dwAdjustedState As Int32, ByVal dwAdjustedMonth As Int32, ByVal dwAdjustedDay As Int32, ByVal dwAdjustedHour As Int32, ByVal dwAdjustedMinute As Int32, ByVal dwRestoredState As Int32, ByVal dwRestoredMonth As Int32, ByVal dwRestoredDay As Int32, ByVal dwRestoredHour As Int32, ByVal dwRestoredMinte As Int32) As Int32

    Public Const NEWS_EXTEND As Short = 2
    Public Const NEWS_STANDARD As Short = 1

    Public Declare Ansi Function FK_GetAccessTime Lib "ComMain" (ByVal nHandleIndex As Integer, ByVal anEnrollNumber As Integer, ByRef apnAccessTime As Integer) As Integer
    Public Declare Ansi Function FK_SetAccessTime Lib "ComMain" (ByVal nHandleIndex As Integer, ByVal anEnrollNumber As Integer, ByVal anAccessTime As Integer) As Integer
    Public Declare Ansi Function FK_SetFontName Lib "ComMain" (ByVal nHandleIndex As Integer, ByVal aStrFontName As String, ByVal anFontType As Integer) As Integer
    Public Declare Ansi Function FK_GetRealTimeInfo Lib "ComMain" (ByVal nHandleIndex As Integer, ByRef apGetRealTime As Integer) As Integer
    Public Declare Ansi Function FK_SetRealTimeInfo Lib "ComMain" (ByVal nHandleIndex As Integer, ByRef apSetRealTime As Integer) As Integer

    Public Declare Ansi Function FK_GetServerNetInfo Lib "ComMain" (ByVal nHandleIndex As Integer, ByRef astrServerIPAddress As String, ByRef apServerPort As Integer, ByRef apServerRequest As Integer) As Integer
    Public Declare Ansi Function FK_SetServerNetInfo Lib "ComMain" (ByVal nHandleIndex As Integer, ByVal astrServerIPAddress As String, ByVal anServerPort As Integer, ByVal apServerReques As Integer) As Integer


    '// -- Post & Shift Info (HTML version)
    Public Declare Ansi Function FK_GetOneShiftInfo Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anShiftNumber As Int32, _
        ByRef apShiftSHour As Int32, _
        ByRef apShiftSMinute As Int32, _
        ByRef apShiftEHour As Int32, _
        ByRef apShiftEMinute As Int32, _
        ByRef apstrShiftName As String _
    ) As Int32

    Public Declare Ansi Function FK_SetOneShiftInfo Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anShiftNumber As Int32, _
        ByVal anShiftSHour As Int32, _
        ByVal anShiftSMinute As Int32, _
        ByVal anShiftEHour As Int32, _
        ByVal anShiftEMinute As Int32, _
        ByVal astrShiftName As String _
    ) As Int32

    Public Declare Ansi Function FK_GetOnePostInfo Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anPostNumber As Int32, _
        ByRef apstrPostName As Int32, _
        ByRef apShiftNumber1 As Int32, _
        ByRef apShiftNumber2 As Int32, _
        ByRef apShiftNumber3 As Int32, _
        ByRef apShiftNumber4 As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetOnePostInfo Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anPostNumber As Int32, _
        ByVal astrPostName As String, _
        ByVal anShiftNumber1 As Int32, _
        ByVal anShiftNumber2 As Int32, _
        ByVal anShiftNumber3 As Int32, _
        ByVal anShiftNumber4 As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_GetUserInfo Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anEnrollNumber As UInt32, _
        ByRef apstrUserName As String, _
        ByRef apNewKind As Int32, _
        ByRef apVerifyMode As Int32, _
        ByRef apPostID As Int32, _
        ByRef apShiftNumber1 As Int32, _
        ByRef apShiftNumber2 As Int32, _
        ByRef apShiftNumber3 As Int32, _
        ByRef apShiftNumber4 As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetUserInfo Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anEnrollNumber As UInt32, _
        ByVal astrUserName As String, _
        ByVal anNewKind As Int32, _
        ByVal anVerifyMode As Int32, _
        ByVal anPostID As Int32, _
        ByVal anShiftNumber1 As Int32, _
        ByVal anShiftNumber2 As Int32, _
        ByVal anShiftNumber3 As Int32, _
        ByVal anShiftNumber4 As Int32 _
    ) As Int32

    '// -- Post & Shift Info (EXCEL version)
    Public Declare Ansi Function FK_GetPostShiftInfo Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytPostShiftInfo() As Byte, _
        ByRef apPostShiftInfoLen As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetPostShiftInfo Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytPostShiftInfo() As Byte, _
        ByVal anPostShiftInfoLen As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_GetUserInfoEx Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anEnrollNumber As UInt32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytUserInfo() As Byte, _
        ByRef apUserInfoLen As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetUserInfoEx Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anEnrollNumber As UInt32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytUserInfo() As Byte, _
        ByVal anUserInfoLen As Int32 _
    ) As Int32

    '// -- Photo transfer
    Public Declare Ansi Function FK_GetEnrollPhoto Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anEnrollNumber As UInt32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytPhotoImage() As Byte, _
        ByRef anPhotoLength As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetEnrollPhoto Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anEnrollNumber As UInt32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytPhotoImage() As Byte, _
        ByVal anPhotoLength As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_DeleteEnrollPhoto Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anEnrollNumber As UInt32 _
    ) As Int32

    Public Declare Ansi Function FK_GetLogPhoto Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anEnrollNumber As UInt32, _
        ByVal anYear As Int32, _
        ByVal anMonth As Int32, _
        ByVal anDay As Int32, _
        ByVal anHour As Int32, _
        ByVal anMinute As Int32, _
        ByVal anSec As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytPhotoImage() As Byte, _
        ByRef anPhotoLength As Int32 _
    ) As Int32

    '// Get supported flag of specific enroll data type
    Public Declare Ansi Function FK_IsSupportedEnrollData Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal anBackupNumber As Int32, _
        ByRef anSupportFlag As Int32 _
    ) As Int32


    '{ Access Control(Haoshun FK) function
    Public Declare Ansi Function FK_HS_GetTimeZone Lib "ComMain" ( _
        ByVal anHandleIndex As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytOneTimeZone() As Byte _
        ) As Int32
    Public Declare Ansi Function FK_HS_SetTimeZone Lib "ComMain" ( _
        ByVal anHandleIndex As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytOneTimeZone() As Byte _
        ) As Int32
    Public Declare Ansi Function FK_HS_GetUserWeekPassTime Lib "ComMain" ( _
        ByVal anHandleIndex As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytUserWeekPassTime() As Byte _
        ) As Int32
    Public Declare Ansi Function FK_HS_SetUserWeekPassTime Lib "ComMain" ( _
        ByVal anHandleIndex As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytUserWeekPassTime() As Byte _
        ) As Int32
    '}

    '// All purpose
    Public Declare Ansi Function FK_ExecCommand Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal astrCommand As String, _
        ByRef astrResult As String _
    ) As Int32



    Public Declare Ansi Function FK_GetIsSupportStringID Lib "ComMain" ( _
        ByVal nHandleIndex As Int32 _
    ) As Int32





    Public Declare Ansi Function FK_GetLogDataIsSupportStringID Lib "ComMain" ( _
        ByVal nHandleIndex As Int32 _
    ) As Int32
    Public Declare Ansi Function FK_GetUSBEnrollDataIsSupportStringID Lib "ComMain" ( _
        ByVal nHandleIndex As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_GetSuperLogData_StringID Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByRef strSEnrollNumber As String, _
        ByRef strGEnrollNumber As String, _
        ByRef nManipulation As Int32, _
        ByRef nBackupNumber As Int32, _
        ByRef dtLogTime As Date _
     ) As Int32


    Public Declare Ansi Function FK_GetGeneralLogData_StringID Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByRef strEnrollNumber As String, _
        ByRef nVerifyMode As Int32, _
        ByRef nInOutMode As Int32, _
        ByRef dtLogTime As Date _
     ) As Int32


    Public Declare Ansi Function FK_EnableUser_StringID Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strEnrollNumber As String, _
        ByVal nBackupNumber As Int32, _
        ByVal nEnableFlag As Int32 _
    ) As Int32


    Public Declare Ansi Function FK_ModifyPrivilege_StringID Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strEnrollNumber As String, _
        ByVal nBackupNumber As Int32, _
        ByVal nMachinePrivilege As Int32 _
     ) As Int32


    Public Declare Ansi Function FK_GetAllUserID_StringID Lib "ComMain" ( _
       ByVal nHandleIndex As Int32, _
       ByRef strEnrollNumber As String, _
       ByRef nBackupNumber As Int32, _
       ByRef nMachinePrivilege As Int32, _
       ByRef nEnable As Int32 _
   ) As Int32

    Public Declare Ansi Function FK_GetEnrollData_StringID Lib "ComMain" ( _
       ByVal nHandleIndex As Int32, _
       ByVal strEnrollNumber As String, _
       ByVal nBackupNumber As Int32, _
       ByRef nMachinePrivilege As Int32, _
       <MarshalAs(UnmanagedType.LPArray)> ByVal bytEnrollData() As Byte, _
       ByRef nPassword As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_PutEnrollData_StringID Lib "ComMain" ( _
       ByVal nHandleIndex As Int32, _
       ByVal strEnrollNumber As String, _
       ByVal nBackupNumber As Int32, _
       ByVal nMachinePrivilege As Int32, _
       <MarshalAs(UnmanagedType.LPArray)> ByVal bytEnrollData() As Byte, _
       ByVal nPassword As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_DeleteEnrollData_StringID Lib "ComMain" ( _
       ByVal nHandleIndex As Int32, _
       ByVal strEnrollNumber As String, _
       ByVal nBackupNumber As Int32 _
    ) As Int32



    Public Declare Ansi Function FK_GetUserName_StringID Lib "ComMain" ( _
      ByVal nHandleIndex As Int32, _
      ByVal strEnrollNumber As String, _
      ByRef strUserName As String _
    ) As Int32


    Public Declare Ansi Function FK_SetUserName_StringID Lib "ComMain" ( _
      ByVal nHandleIndex As Int32, _
      ByVal strEnrollNumber As String, _
      ByVal strUserName As String _
    ) As Int32


    Public Declare Ansi Function FK_GetUserPassTime_StringID Lib "ComMain" ( _
      ByVal nHandleIndex As Int32, _
      ByVal strEnrollNumber As String, _
      ByRef nGroupID As Int32, _
      <MarshalAs(UnmanagedType.LPArray)> ByVal bytUserPassTimeInfo() As Byte, _
      ByVal nUserPassTimeInfoSize As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_SetUserPassTime_StringID Lib "ComMain" ( _
      ByVal nHandleIndex As Int32, _
      ByVal strEnrollNumber As String, _
      ByVal nGroupID As Int32, _
      <MarshalAs(UnmanagedType.LPArray)> ByVal bytUserPassTimeInfo() As Byte, _
      ByVal nUserPassTimeInfoSize As Int32 _
    ) As Int32

    Public Declare Ansi Function FK_USBGetOneEnrollData_StringID Lib "ComMain" ( _
      ByVal nHandleIndex As Int32, _
      ByRef strEnrollNumber As String, _
      ByRef nBackupNumber As Int32, _
      ByRef nMachinePrivilege As Int32, _
      <MarshalAs(UnmanagedType.LPArray)> ByVal bytEnrollData() As Byte, _
      ByRef nPassWord As Int32, _
      ByRef nEnableFlag As Int32, _
      ByRef strEnrollName As String _
    ) As Int32

    Public Declare Ansi Function FK_USBSetOneEnrollData_StringID Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strEnrollNumber As String, _
        ByVal nBackupNumber As Int32, _
        ByVal nMachinePrivilege As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal bytEnrollData() As Byte, _
        ByVal nPassWord As Int32, _
        ByVal nEnableFlag As Int32, _
        ByVal strEnrollName As String _
     ) As Int32


    Public Declare Ansi Function FK_GetUserInfoEx_StringID Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strEnrollNumber As String, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytUserInfo() As Byte, _
        ByRef apUserInfoLen As Int32 _
     ) As Int32

    Public Declare Ansi Function FK_SetUserInfoEx_StringID Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strEnrollNumber As String, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytUserInfo() As Byte, _
        ByVal anUserInfoLen As Int32 _
     ) As Int32


    Public Declare Ansi Function FK_GetEnrollPhoto_StringID Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strEnrollNumber As String, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytPhotoImage() As Byte, _
        ByRef anPhotoLength As Int32 _
     ) As Int32


    Public Declare Ansi Function FK_SetEnrollPhoto_StringID Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strEnrollNumber As String, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytPhotoImage() As Byte, _
        ByVal anPhotoLength As Int32 _
     ) As Int32


    Public Declare Ansi Function FK_DeleteEnrollPhoto_StringID Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strEnrollNumber As String _
     ) As Int32


    Public Declare Ansi Function FK_GetLogPhoto_StringID Lib "ComMain" ( _
        ByVal nHandleIndex As Int32, _
        ByVal strEnrollNumber As String, _
        ByVal anYear As Int32, _
        ByVal anMonth As Int32, _
        ByVal anDay As Int32, _
        ByVal anHour As Int32, _
        ByVal anMinute As Int32, _
        ByVal anSec As Int32, _
        <MarshalAs(UnmanagedType.LPArray)> ByVal abytPhotoImage() As Byte, _
        ByRef anPhotoLength As Int32 _
    ) As Int32



    'nitin for finger validity
    '    [DllImport("ComMain.dll", CharSet = CharSet.Ansi)]
    'public static extern int FK_ExtCommand(
    '    int anHandleIndex,
    '    byte[] abytCmdStruct);
    Public Declare Ansi Function FK_ExtCommand Lib "ComMain" ( _
    ByVal anHandleIndex As Integer,
    ByVal abytCmdStruct() As Byte
   ) As Int32
    'nitin
End Class