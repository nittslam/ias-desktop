﻿Imports System.Resources
Imports System.Globalization
Imports System.Data.OleDb

Public Class XtraAdminMenu
    Public Sub New()
        InitializeComponent()
    End Sub
    Private Sub XtraMasterMenu_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        NavigationPane1.Width = NavigationPane1.Parent.Width
        NavigationPage1.Width = NavigationPage1.Parent.Width
        Me.Width = Me.Parent.Width
        Me.Height = Me.Parent.Height
        'MsgBox(NavigationPane1.Width - NavigationPage1.Width)
        Common.NavHeight = NavigationPage1.Height
        Common.NavWidth = NavigationPage1.Width
        If Common.UserPrevilege = "Y" Then
            NavigationPage8.PageVisible = True
        Else
            NavigationPage8.PageVisible = False
        End If
        If Common.TimeOfficeSetup = "Y" Then
            NavigationPage1.PageVisible = True
        Else
            NavigationPage1.PageVisible = False
            NavigationPane1.SelectedPageIndex = 1
        End If
    End Sub
    Private Sub NavigationPane1_SelectedPageIndexChanged(sender As System.Object, e As System.EventArgs) Handles NavigationPane1.SelectedPageIndexChanged
        If NavigationPane1.SelectedPageIndex = 0 Then
            NavigationPage1.Controls.Clear()
            'Dim form As UserControl = New XtraTimeOfficePolicy '
            Dim form As UserControl = New XtraTimeOfficePolicyNew
            form.Dock = DockStyle.Fill
            NavigationPage1.Controls.Add(form)
            form.Show()
        ElseIf NavigationPane1.SelectedPageIndex = 1 Then
            NavigationPage2.Controls.Clear()
            Dim form As UserControl = New XtraDataProcess '
            form.Dock = DockStyle.Fill
            NavigationPage2.Controls.Add(form)
            form.Show()
            'ElseIf NavigationPane1.SelectedPageIndex = 2 Then
            '    NavigationPage3.Controls.Clear()
            '    Dim form As UserControl = New XtraDeparment
            '    form.Dock = DockStyle.Fill
            '    NavigationPage3.Controls.Add(form)
            '    form.Show()
        ElseIf NavigationPane1.SelectedPageIndex = 3 Then
            NavigationPage4.Controls.Clear()
            Dim form As UserControl = New XtraDBSetting
            form.Dock = DockStyle.Fill
            NavigationPage4.Controls.Add(form)
            form.Show()
        ElseIf NavigationPane1.SelectedPageIndex = 4 Then
            NavigationPage5.Controls.Clear()
            Dim form As UserControl = New XtraSMSSetting
            form.Dock = DockStyle.Fill
            NavigationPage5.Controls.Add(form)
            form.Show()
        ElseIf NavigationPane1.SelectedPageIndex = 5 Then
            NavigationPage6.Controls.Clear()
            Dim form As UserControl = New XtraBulkSMS
            form.Dock = DockStyle.Fill
            NavigationPage6.Controls.Add(form)
            form.Show()
        ElseIf NavigationPane1.SelectedPageIndex = 6 Then
            NavigationPage7.Controls.Clear()
            Dim form As UserControl = New XtraReportEmailSetting
            form.Dock = DockStyle.Fill
            NavigationPage7.Controls.Add(form)
            form.Show()
        ElseIf NavigationPane1.SelectedPageIndex = 7 Then
            NavigationPage8.Controls.Clear()
            Dim form As UserControl = New XtraUserMgmt
            form.Dock = DockStyle.Fill
            NavigationPage8.Controls.Add(form)
            form.Show()
        ElseIf NavigationPane1.SelectedPageIndex = 8 Then
            NavigationPage9.Controls.Clear()
            Dim form As UserControl = New XtraDBBackUpSetting
            form.Dock = DockStyle.Fill
            NavigationPage9.Controls.Add(form)
            form.Show()
        ElseIf NavigationPane1.SelectedPageIndex = 9 Then
            NavigationPage10.Controls.Clear()
            Dim form As UserControl = New XtraParallelSetting
            form.Dock = DockStyle.Fill
            NavigationPage10.Controls.Add(form)
            form.Show()
        ElseIf NavigationPane1.SelectedPageIndex = 10 Then
            XtraAppInfo.ShowDialog()
        End If
    End Sub
    'Private Sub temp()
    '    Dim adapter As New OleDbDataAdapter("SELECT * FROM Customers", conn)
    '    Dim cmdBuilder As New OleDbCommandBuilder(adapter)
    '    adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
    '    adapter.Fill(ds, "Customers")
    '    row = ds.Tables("Customers").NewRow()
    '    row("CustomersId") = "TstID"
    '    row("ContactName") = "Lana Jackob"
    '    row("CompanyName") = "Mindcracker Inc. "
    '    ds.Tables("customers").Rows.Add(row)
    '    adapter.Update(ds, "Customers")
    '    dataGrid1.DataSource = ds.DefaultViewManager
    'End Sub
End Class
