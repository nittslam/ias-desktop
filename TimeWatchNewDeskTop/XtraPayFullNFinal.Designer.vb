﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraPayFullNFinal
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraPayFullNFinal))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.txtEarningEarn10 = New DevExpress.XtraEditors.TextEdit()
        Me.lbl10 = New DevExpress.XtraEditors.LabelControl()
        Me.txtEarningEarn9 = New DevExpress.XtraEditors.TextEdit()
        Me.lbl9 = New DevExpress.XtraEditors.LabelControl()
        Me.txtEarningEarn8 = New DevExpress.XtraEditors.TextEdit()
        Me.lbl8 = New DevExpress.XtraEditors.LabelControl()
        Me.txtEarningEarn7 = New DevExpress.XtraEditors.TextEdit()
        Me.lbl7 = New DevExpress.XtraEditors.LabelControl()
        Me.txtEarningEarn6 = New DevExpress.XtraEditors.TextEdit()
        Me.lbl6 = New DevExpress.XtraEditors.LabelControl()
        Me.txtEarningEarn5 = New DevExpress.XtraEditors.TextEdit()
        Me.lbl5 = New DevExpress.XtraEditors.LabelControl()
        Me.txtEarningEarn4 = New DevExpress.XtraEditors.TextEdit()
        Me.lbl4 = New DevExpress.XtraEditors.LabelControl()
        Me.txtEarningEarn3 = New DevExpress.XtraEditors.TextEdit()
        Me.lbl3 = New DevExpress.XtraEditors.LabelControl()
        Me.txtEarningEarn2 = New DevExpress.XtraEditors.TextEdit()
        Me.lbl2 = New DevExpress.XtraEditors.LabelControl()
        Me.txtEarningEarn1 = New DevExpress.XtraEditors.TextEdit()
        Me.lbl1 = New DevExpress.XtraEditors.LabelControl()
        Me.txtEarningMed = New DevExpress.XtraEditors.TextEdit()
        Me.Label7 = New DevExpress.XtraEditors.LabelControl()
        Me.txtEarningConv = New DevExpress.XtraEditors.TextEdit()
        Me.Label3 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.PopupContainerEditLeave = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerControlLeave = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlLeave = New DevExpress.XtraGrid.GridControl()
        Me.GridViewLeave = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemTimeEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.txtAdvance = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.txtLoan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.txtNoticeMonth = New DevExpress.XtraEditors.TextEdit()
        Me.txtNoticePay = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.txtLeave = New DevExpress.XtraEditors.TextEdit()
        Me.txtLeaveInCash = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.txtGratuity = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.txtNetPay = New DevExpress.XtraEditors.TextEdit()
        Me.prvMonth = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.btnApply = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdOk = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.txtapplicablefrom = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboNepaliYear = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNEpaliMonth = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LookUpEdit1 = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.txtPayableMonth = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.lblName = New DevExpress.XtraEditors.LabelControl()
        Me.lblDept = New DevExpress.XtraEditors.LabelControl()
        Me.lblCardNum = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.TblEmployeeTableAdapter = New iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter()
        Me.TblEmployeeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblEmployee1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.txtEarningEarn10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningEarn9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningEarn8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningEarn7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningEarn6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningEarn5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningEarn4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningEarn3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningEarn2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningEarn1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningMed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningConv.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.PopupContainerEditLeave.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlLeave, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlLeave.SuspendLayout()
        CType(Me.GridControlLeave, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewLeave, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAdvance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLoan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNoticeMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNoticePay.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLeave.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLeaveInCash.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGratuity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNetPay.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.prvMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel1.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtapplicablefrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtapplicablefrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPayableMonth.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPayableMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PanelControl1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1250, 568)
        Me.SplitContainerControl1.SplitterPosition = 1165
        Me.SplitContainerControl1.TabIndex = 2
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.GroupControl3)
        Me.PanelControl1.Controls.Add(Me.GroupControl2)
        Me.PanelControl1.Controls.Add(Me.SidePanel1)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl1.Location = New System.Drawing.Point(0, 113)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(1165, 455)
        Me.PanelControl1.TabIndex = 40
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.GroupControl3.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl3.AppearanceCaption.Options.UseFont = True
        Me.GroupControl3.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl3.Controls.Add(Me.txtEarningEarn10)
        Me.GroupControl3.Controls.Add(Me.lbl10)
        Me.GroupControl3.Controls.Add(Me.txtEarningEarn9)
        Me.GroupControl3.Controls.Add(Me.lbl9)
        Me.GroupControl3.Controls.Add(Me.txtEarningEarn8)
        Me.GroupControl3.Controls.Add(Me.lbl8)
        Me.GroupControl3.Controls.Add(Me.txtEarningEarn7)
        Me.GroupControl3.Controls.Add(Me.lbl7)
        Me.GroupControl3.Controls.Add(Me.txtEarningEarn6)
        Me.GroupControl3.Controls.Add(Me.lbl6)
        Me.GroupControl3.Controls.Add(Me.txtEarningEarn5)
        Me.GroupControl3.Controls.Add(Me.lbl5)
        Me.GroupControl3.Controls.Add(Me.txtEarningEarn4)
        Me.GroupControl3.Controls.Add(Me.lbl4)
        Me.GroupControl3.Controls.Add(Me.txtEarningEarn3)
        Me.GroupControl3.Controls.Add(Me.lbl3)
        Me.GroupControl3.Controls.Add(Me.txtEarningEarn2)
        Me.GroupControl3.Controls.Add(Me.lbl2)
        Me.GroupControl3.Controls.Add(Me.txtEarningEarn1)
        Me.GroupControl3.Controls.Add(Me.lbl1)
        Me.GroupControl3.Controls.Add(Me.txtEarningMed)
        Me.GroupControl3.Controls.Add(Me.Label7)
        Me.GroupControl3.Controls.Add(Me.txtEarningConv)
        Me.GroupControl3.Controls.Add(Me.Label3)
        Me.GroupControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl3.Location = New System.Drawing.Point(594, 2)
        Me.GroupControl3.LookAndFeel.SkinName = "iMaginary"
        Me.GroupControl3.LookAndFeel.UseDefaultLookAndFeel = False
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(569, 417)
        Me.GroupControl3.TabIndex = 5
        Me.GroupControl3.Text = "Reibursment Details"
        '
        'txtEarningEarn10
        '
        Me.txtEarningEarn10.Location = New System.Drawing.Point(276, 325)
        Me.txtEarningEarn10.Name = "txtEarningEarn10"
        Me.txtEarningEarn10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningEarn10.Properties.Appearance.Options.UseFont = True
        Me.txtEarningEarn10.Properties.Mask.EditMask = "######.##"
        Me.txtEarningEarn10.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningEarn10.Properties.MaxLength = 9
        Me.txtEarningEarn10.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningEarn10.TabIndex = 12
        '
        'lbl10
        '
        Me.lbl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbl10.Appearance.Options.UseFont = True
        Me.lbl10.Location = New System.Drawing.Point(19, 328)
        Me.lbl10.Name = "lbl10"
        Me.lbl10.Size = New System.Drawing.Size(21, 14)
        Me.lbl10.TabIndex = 97
        Me.lbl10.Text = "e10"
        '
        'txtEarningEarn9
        '
        Me.txtEarningEarn9.Location = New System.Drawing.Point(276, 299)
        Me.txtEarningEarn9.Name = "txtEarningEarn9"
        Me.txtEarningEarn9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningEarn9.Properties.Appearance.Options.UseFont = True
        Me.txtEarningEarn9.Properties.Mask.EditMask = "######.##"
        Me.txtEarningEarn9.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningEarn9.Properties.MaxLength = 9
        Me.txtEarningEarn9.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningEarn9.TabIndex = 11
        '
        'lbl9
        '
        Me.lbl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbl9.Appearance.Options.UseFont = True
        Me.lbl9.Location = New System.Drawing.Point(19, 302)
        Me.lbl9.Name = "lbl9"
        Me.lbl9.Size = New System.Drawing.Size(14, 14)
        Me.lbl9.TabIndex = 95
        Me.lbl9.Text = "e9"
        '
        'txtEarningEarn8
        '
        Me.txtEarningEarn8.Location = New System.Drawing.Point(276, 273)
        Me.txtEarningEarn8.Name = "txtEarningEarn8"
        Me.txtEarningEarn8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningEarn8.Properties.Appearance.Options.UseFont = True
        Me.txtEarningEarn8.Properties.Mask.EditMask = "######.##"
        Me.txtEarningEarn8.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningEarn8.Properties.MaxLength = 9
        Me.txtEarningEarn8.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningEarn8.TabIndex = 10
        '
        'lbl8
        '
        Me.lbl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbl8.Appearance.Options.UseFont = True
        Me.lbl8.Location = New System.Drawing.Point(19, 276)
        Me.lbl8.Name = "lbl8"
        Me.lbl8.Size = New System.Drawing.Size(14, 14)
        Me.lbl8.TabIndex = 93
        Me.lbl8.Text = "e8"
        '
        'txtEarningEarn7
        '
        Me.txtEarningEarn7.Location = New System.Drawing.Point(276, 247)
        Me.txtEarningEarn7.Name = "txtEarningEarn7"
        Me.txtEarningEarn7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningEarn7.Properties.Appearance.Options.UseFont = True
        Me.txtEarningEarn7.Properties.Mask.EditMask = "######.##"
        Me.txtEarningEarn7.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningEarn7.Properties.MaxLength = 9
        Me.txtEarningEarn7.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningEarn7.TabIndex = 9
        '
        'lbl7
        '
        Me.lbl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbl7.Appearance.Options.UseFont = True
        Me.lbl7.Location = New System.Drawing.Point(19, 250)
        Me.lbl7.Name = "lbl7"
        Me.lbl7.Size = New System.Drawing.Size(14, 14)
        Me.lbl7.TabIndex = 91
        Me.lbl7.Text = "e7"
        '
        'txtEarningEarn6
        '
        Me.txtEarningEarn6.Location = New System.Drawing.Point(276, 221)
        Me.txtEarningEarn6.Name = "txtEarningEarn6"
        Me.txtEarningEarn6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningEarn6.Properties.Appearance.Options.UseFont = True
        Me.txtEarningEarn6.Properties.Mask.EditMask = "######.##"
        Me.txtEarningEarn6.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningEarn6.Properties.MaxLength = 9
        Me.txtEarningEarn6.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningEarn6.TabIndex = 8
        '
        'lbl6
        '
        Me.lbl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbl6.Appearance.Options.UseFont = True
        Me.lbl6.Location = New System.Drawing.Point(19, 224)
        Me.lbl6.Name = "lbl6"
        Me.lbl6.Size = New System.Drawing.Size(14, 14)
        Me.lbl6.TabIndex = 89
        Me.lbl6.Text = "e6"
        '
        'txtEarningEarn5
        '
        Me.txtEarningEarn5.Location = New System.Drawing.Point(276, 195)
        Me.txtEarningEarn5.Name = "txtEarningEarn5"
        Me.txtEarningEarn5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningEarn5.Properties.Appearance.Options.UseFont = True
        Me.txtEarningEarn5.Properties.Mask.EditMask = "######.##"
        Me.txtEarningEarn5.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningEarn5.Properties.MaxLength = 9
        Me.txtEarningEarn5.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningEarn5.TabIndex = 7
        '
        'lbl5
        '
        Me.lbl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbl5.Appearance.Options.UseFont = True
        Me.lbl5.Location = New System.Drawing.Point(19, 198)
        Me.lbl5.Name = "lbl5"
        Me.lbl5.Size = New System.Drawing.Size(14, 14)
        Me.lbl5.TabIndex = 87
        Me.lbl5.Text = "e5"
        '
        'txtEarningEarn4
        '
        Me.txtEarningEarn4.Location = New System.Drawing.Point(276, 169)
        Me.txtEarningEarn4.Name = "txtEarningEarn4"
        Me.txtEarningEarn4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningEarn4.Properties.Appearance.Options.UseFont = True
        Me.txtEarningEarn4.Properties.Mask.EditMask = "######.##"
        Me.txtEarningEarn4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningEarn4.Properties.MaxLength = 9
        Me.txtEarningEarn4.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningEarn4.TabIndex = 6
        '
        'lbl4
        '
        Me.lbl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbl4.Appearance.Options.UseFont = True
        Me.lbl4.Location = New System.Drawing.Point(19, 172)
        Me.lbl4.Name = "lbl4"
        Me.lbl4.Size = New System.Drawing.Size(14, 14)
        Me.lbl4.TabIndex = 85
        Me.lbl4.Text = "e4"
        '
        'txtEarningEarn3
        '
        Me.txtEarningEarn3.Location = New System.Drawing.Point(276, 143)
        Me.txtEarningEarn3.Name = "txtEarningEarn3"
        Me.txtEarningEarn3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningEarn3.Properties.Appearance.Options.UseFont = True
        Me.txtEarningEarn3.Properties.Mask.EditMask = "######.##"
        Me.txtEarningEarn3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningEarn3.Properties.MaxLength = 9
        Me.txtEarningEarn3.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningEarn3.TabIndex = 5
        '
        'lbl3
        '
        Me.lbl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbl3.Appearance.Options.UseFont = True
        Me.lbl3.Location = New System.Drawing.Point(19, 146)
        Me.lbl3.Name = "lbl3"
        Me.lbl3.Size = New System.Drawing.Size(14, 14)
        Me.lbl3.TabIndex = 83
        Me.lbl3.Text = "e3"
        '
        'txtEarningEarn2
        '
        Me.txtEarningEarn2.Location = New System.Drawing.Point(276, 117)
        Me.txtEarningEarn2.Name = "txtEarningEarn2"
        Me.txtEarningEarn2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningEarn2.Properties.Appearance.Options.UseFont = True
        Me.txtEarningEarn2.Properties.Mask.EditMask = "######.##"
        Me.txtEarningEarn2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningEarn2.Properties.MaxLength = 9
        Me.txtEarningEarn2.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningEarn2.TabIndex = 4
        '
        'lbl2
        '
        Me.lbl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbl2.Appearance.Options.UseFont = True
        Me.lbl2.Location = New System.Drawing.Point(19, 120)
        Me.lbl2.Name = "lbl2"
        Me.lbl2.Size = New System.Drawing.Size(14, 14)
        Me.lbl2.TabIndex = 81
        Me.lbl2.Text = "e2"
        '
        'txtEarningEarn1
        '
        Me.txtEarningEarn1.Location = New System.Drawing.Point(276, 91)
        Me.txtEarningEarn1.Name = "txtEarningEarn1"
        Me.txtEarningEarn1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningEarn1.Properties.Appearance.Options.UseFont = True
        Me.txtEarningEarn1.Properties.Mask.EditMask = "######.##"
        Me.txtEarningEarn1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningEarn1.Properties.MaxLength = 9
        Me.txtEarningEarn1.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningEarn1.TabIndex = 3
        '
        'lbl1
        '
        Me.lbl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbl1.Appearance.Options.UseFont = True
        Me.lbl1.Location = New System.Drawing.Point(19, 94)
        Me.lbl1.Name = "lbl1"
        Me.lbl1.Size = New System.Drawing.Size(14, 14)
        Me.lbl1.TabIndex = 79
        Me.lbl1.Text = "e1"
        '
        'txtEarningMed
        '
        Me.txtEarningMed.Location = New System.Drawing.Point(276, 65)
        Me.txtEarningMed.Name = "txtEarningMed"
        Me.txtEarningMed.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningMed.Properties.Appearance.Options.UseFont = True
        Me.txtEarningMed.Properties.Mask.EditMask = "######.##"
        Me.txtEarningMed.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningMed.Properties.MaxLength = 9
        Me.txtEarningMed.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningMed.TabIndex = 2
        '
        'Label7
        '
        Me.Label7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.Label7.Appearance.Options.UseFont = True
        Me.Label7.Location = New System.Drawing.Point(19, 68)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(39, 14)
        Me.Label7.TabIndex = 77
        Me.Label7.Text = "Medical"
        '
        'txtEarningConv
        '
        Me.txtEarningConv.Location = New System.Drawing.Point(276, 39)
        Me.txtEarningConv.Name = "txtEarningConv"
        Me.txtEarningConv.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningConv.Properties.Appearance.Options.UseFont = True
        Me.txtEarningConv.Properties.Mask.EditMask = "######.##"
        Me.txtEarningConv.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningConv.Properties.MaxLength = 9
        Me.txtEarningConv.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningConv.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.Label3.Appearance.Options.UseFont = True
        Me.Label3.Location = New System.Drawing.Point(19, 42)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 14)
        Me.Label3.TabIndex = 75
        Me.Label3.Text = "Conveyance"
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.GroupControl2.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl2.Controls.Add(Me.PopupContainerEditLeave)
        Me.GroupControl2.Controls.Add(Me.PopupContainerControlLeave)
        Me.GroupControl2.Controls.Add(Me.txtAdvance)
        Me.GroupControl2.Controls.Add(Me.LabelControl11)
        Me.GroupControl2.Controls.Add(Me.txtLoan)
        Me.GroupControl2.Controls.Add(Me.LabelControl10)
        Me.GroupControl2.Controls.Add(Me.txtNoticeMonth)
        Me.GroupControl2.Controls.Add(Me.txtNoticePay)
        Me.GroupControl2.Controls.Add(Me.LabelControl8)
        Me.GroupControl2.Controls.Add(Me.txtLeave)
        Me.GroupControl2.Controls.Add(Me.txtLeaveInCash)
        Me.GroupControl2.Controls.Add(Me.LabelControl7)
        Me.GroupControl2.Controls.Add(Me.txtGratuity)
        Me.GroupControl2.Controls.Add(Me.LabelControl4)
        Me.GroupControl2.Controls.Add(Me.txtNetPay)
        Me.GroupControl2.Controls.Add(Me.prvMonth)
        Me.GroupControl2.Controls.Add(Me.LabelControl3)
        Me.GroupControl2.Dock = System.Windows.Forms.DockStyle.Left
        Me.GroupControl2.Location = New System.Drawing.Point(2, 2)
        Me.GroupControl2.LookAndFeel.SkinName = "iMaginary"
        Me.GroupControl2.LookAndFeel.UseDefaultLookAndFeel = False
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(592, 417)
        Me.GroupControl2.TabIndex = 4
        Me.GroupControl2.Text = "Calculated Amount"
        '
        'PopupContainerEditLeave
        '
        Me.PopupContainerEditLeave.Location = New System.Drawing.Point(140, 91)
        Me.PopupContainerEditLeave.Name = "PopupContainerEditLeave"
        Me.PopupContainerEditLeave.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLeave.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditLeave.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLeave.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditLeave.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLeave.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditLeave.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditLeave.Properties.PopupControl = Me.PopupContainerControlLeave
        Me.PopupContainerEditLeave.Size = New System.Drawing.Size(155, 20)
        Me.PopupContainerEditLeave.TabIndex = 4
        '
        'PopupContainerControlLeave
        '
        Me.PopupContainerControlLeave.Controls.Add(Me.GridControlLeave)
        Me.PopupContainerControlLeave.Location = New System.Drawing.Point(88, 247)
        Me.PopupContainerControlLeave.Name = "PopupContainerControlLeave"
        Me.PopupContainerControlLeave.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlLeave.TabIndex = 87
        '
        'GridControlLeave
        '
        Me.GridControlLeave.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlLeave.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlLeave.Location = New System.Drawing.Point(0, 0)
        Me.GridControlLeave.MainView = Me.GridViewLeave
        Me.GridControlLeave.Name = "GridControlLeave"
        Me.GridControlLeave.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit3})
        Me.GridControlLeave.Size = New System.Drawing.Size(300, 300)
        Me.GridControlLeave.TabIndex = 6
        Me.GridControlLeave.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewLeave})
        '
        'GridViewLeave
        '
        Me.GridViewLeave.GridControl = Me.GridControlLeave
        Me.GridViewLeave.Name = "GridViewLeave"
        Me.GridViewLeave.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewLeave.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewLeave.OptionsBehavior.Editable = False
        Me.GridViewLeave.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewLeave.OptionsSelection.MultiSelect = True
        Me.GridViewLeave.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewLeave.OptionsView.ColumnAutoWidth = False
        '
        'RepositoryItemTimeEdit3
        '
        Me.RepositoryItemTimeEdit3.AutoHeight = False
        Me.RepositoryItemTimeEdit3.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit3.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit3.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit3.Name = "RepositoryItemTimeEdit3"
        '
        'txtAdvance
        '
        Me.txtAdvance.Location = New System.Drawing.Point(407, 169)
        Me.txtAdvance.Name = "txtAdvance"
        Me.txtAdvance.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtAdvance.Properties.Appearance.Options.UseFont = True
        Me.txtAdvance.Properties.Mask.EditMask = "######.##"
        Me.txtAdvance.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtAdvance.Properties.MaxLength = 9
        Me.txtAdvance.Size = New System.Drawing.Size(100, 20)
        Me.txtAdvance.TabIndex = 10
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(19, 172)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(95, 14)
        Me.LabelControl11.TabIndex = 85
        Me.LabelControl11.Text = "Pending Advance"
        '
        'txtLoan
        '
        Me.txtLoan.Location = New System.Drawing.Point(407, 143)
        Me.txtLoan.Name = "txtLoan"
        Me.txtLoan.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtLoan.Properties.Appearance.Options.UseFont = True
        Me.txtLoan.Properties.Mask.EditMask = "######.##"
        Me.txtLoan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtLoan.Properties.MaxLength = 9
        Me.txtLoan.Size = New System.Drawing.Size(100, 20)
        Me.txtLoan.TabIndex = 9
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(19, 146)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(74, 14)
        Me.LabelControl10.TabIndex = 83
        Me.LabelControl10.Text = "Pending Loan"
        '
        'txtNoticeMonth
        '
        Me.txtNoticeMonth.Location = New System.Drawing.Point(301, 117)
        Me.txtNoticeMonth.Name = "txtNoticeMonth"
        Me.txtNoticeMonth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtNoticeMonth.Properties.Appearance.Options.UseFont = True
        Me.txtNoticeMonth.Properties.Mask.EditMask = "###"
        Me.txtNoticeMonth.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtNoticeMonth.Properties.MaxLength = 3
        Me.txtNoticeMonth.Size = New System.Drawing.Size(100, 20)
        Me.txtNoticeMonth.TabIndex = 7
        '
        'txtNoticePay
        '
        Me.txtNoticePay.Location = New System.Drawing.Point(407, 117)
        Me.txtNoticePay.Name = "txtNoticePay"
        Me.txtNoticePay.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtNoticePay.Properties.Appearance.Options.UseFont = True
        Me.txtNoticePay.Properties.Mask.EditMask = "######.##"
        Me.txtNoticePay.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtNoticePay.Properties.MaxLength = 9
        Me.txtNoticePay.Size = New System.Drawing.Size(100, 20)
        Me.txtNoticePay.TabIndex = 8
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(19, 120)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(64, 14)
        Me.LabelControl8.TabIndex = 80
        Me.LabelControl8.Text = "Notice Days"
        '
        'txtLeave
        '
        Me.txtLeave.Location = New System.Drawing.Point(301, 91)
        Me.txtLeave.Name = "txtLeave"
        Me.txtLeave.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtLeave.Properties.Appearance.Options.UseFont = True
        Me.txtLeave.Properties.Mask.EditMask = "######.##"
        Me.txtLeave.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtLeave.Properties.MaxLength = 9
        Me.txtLeave.Size = New System.Drawing.Size(100, 20)
        Me.txtLeave.TabIndex = 5
        '
        'txtLeaveInCash
        '
        Me.txtLeaveInCash.Location = New System.Drawing.Point(407, 91)
        Me.txtLeaveInCash.Name = "txtLeaveInCash"
        Me.txtLeaveInCash.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtLeaveInCash.Properties.Appearance.Options.UseFont = True
        Me.txtLeaveInCash.Properties.Mask.EditMask = "######.##"
        Me.txtLeaveInCash.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtLeaveInCash.Properties.MaxLength = 9
        Me.txtLeaveInCash.Size = New System.Drawing.Size(100, 20)
        Me.txtLeaveInCash.TabIndex = 6
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(19, 94)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(90, 14)
        Me.LabelControl7.TabIndex = 77
        Me.LabelControl7.Text = "Leave for Incash"
        '
        'txtGratuity
        '
        Me.txtGratuity.Location = New System.Drawing.Point(407, 65)
        Me.txtGratuity.Name = "txtGratuity"
        Me.txtGratuity.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtGratuity.Properties.Appearance.Options.UseFont = True
        Me.txtGratuity.Properties.Mask.EditMask = "######.##"
        Me.txtGratuity.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtGratuity.Properties.MaxLength = 9
        Me.txtGratuity.Size = New System.Drawing.Size(100, 20)
        Me.txtGratuity.TabIndex = 3
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(19, 68)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(43, 14)
        Me.LabelControl4.TabIndex = 75
        Me.LabelControl4.Text = "Gratuity"
        '
        'txtNetPay
        '
        Me.txtNetPay.Location = New System.Drawing.Point(407, 39)
        Me.txtNetPay.Name = "txtNetPay"
        Me.txtNetPay.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtNetPay.Properties.Appearance.Options.UseFont = True
        Me.txtNetPay.Properties.Mask.EditMask = "######.##"
        Me.txtNetPay.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtNetPay.Properties.MaxLength = 9
        Me.txtNetPay.Size = New System.Drawing.Size(100, 20)
        Me.txtNetPay.TabIndex = 2
        '
        'prvMonth
        '
        Me.prvMonth.Location = New System.Drawing.Point(140, 40)
        Me.prvMonth.Name = "prvMonth"
        Me.prvMonth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.prvMonth.Properties.Appearance.Options.UseFont = True
        Me.prvMonth.Properties.Caption = "Previous Month Also"
        Me.prvMonth.Size = New System.Drawing.Size(146, 19)
        Me.prvMonth.TabIndex = 1
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(19, 42)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(55, 14)
        Me.LabelControl3.TabIndex = 22
        Me.LabelControl3.Text = "Net Salary"
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.btnApply)
        Me.SidePanel1.Controls.Add(Me.cmdOk)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel1.Location = New System.Drawing.Point(2, 419)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(1161, 34)
        Me.SidePanel1.TabIndex = 47
        Me.SidePanel1.Text = "SidePanel1"
        '
        'btnApply
        '
        Me.btnApply.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.btnApply.Appearance.Options.UseFont = True
        Me.btnApply.Enabled = False
        Me.btnApply.Location = New System.Drawing.Point(7, 6)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.Size = New System.Drawing.Size(75, 23)
        Me.btnApply.TabIndex = 19
        Me.btnApply.Text = "Apply"
        '
        'cmdOk
        '
        Me.cmdOk.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.cmdOk.Appearance.Options.UseFont = True
        Me.cmdOk.Enabled = False
        Me.cmdOk.Location = New System.Drawing.Point(88, 6)
        Me.cmdOk.Name = "cmdOk"
        Me.cmdOk.Size = New System.Drawing.Size(75, 23)
        Me.cmdOk.TabIndex = 18
        Me.cmdOk.Text = "Save"
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.GroupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl1.Controls.Add(Me.txtapplicablefrom)
        Me.GroupControl1.Controls.Add(Me.LabelControl12)
        Me.GroupControl1.Controls.Add(Me.ComboNepaliYear)
        Me.GroupControl1.Controls.Add(Me.ComboNEpaliMonth)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.LookUpEdit1)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.txtPayableMonth)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.lblName)
        Me.GroupControl1.Controls.Add(Me.lblDept)
        Me.GroupControl1.Controls.Add(Me.lblCardNum)
        Me.GroupControl1.Controls.Add(Me.LabelControl9)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(1165, 113)
        Me.GroupControl1.TabIndex = 39
        Me.GroupControl1.Text = "Employee"
        '
        'txtapplicablefrom
        '
        Me.txtapplicablefrom.EditValue = Nothing
        Me.txtapplicablefrom.Location = New System.Drawing.Point(811, 39)
        Me.txtapplicablefrom.Name = "txtapplicablefrom"
        Me.txtapplicablefrom.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtapplicablefrom.Properties.Appearance.Options.UseFont = True
        Me.txtapplicablefrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtapplicablefrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtapplicablefrom.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.txtapplicablefrom.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtapplicablefrom.Size = New System.Drawing.Size(135, 20)
        Me.txtapplicablefrom.TabIndex = 3
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(725, 42)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(71, 14)
        Me.LabelControl12.TabIndex = 41
        Me.LabelControl12.Text = "Leaving Date"
        '
        'ComboNepaliYear
        '
        Me.ComboNepaliYear.Location = New System.Drawing.Point(600, 39)
        Me.ComboNepaliYear.Name = "ComboNepaliYear"
        Me.ComboNepaliYear.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYear.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliYear.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYear.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliYear.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliYear.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliYear.Size = New System.Drawing.Size(61, 20)
        Me.ComboNepaliYear.TabIndex = 39
        '
        'ComboNEpaliMonth
        '
        Me.ComboNEpaliMonth.Location = New System.Drawing.Point(528, 39)
        Me.ComboNEpaliMonth.Name = "ComboNEpaliMonth"
        Me.ComboNEpaliMonth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonth.Properties.Appearance.Options.UseFont = True
        Me.ComboNEpaliMonth.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonth.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNEpaliMonth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNEpaliMonth.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNEpaliMonth.Size = New System.Drawing.Size(66, 20)
        Me.ComboNEpaliMonth.TabIndex = 38
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(21, 42)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(84, 14)
        Me.LabelControl5.TabIndex = 21
        Me.LabelControl5.Text = "Select Paycode"
        '
        'LookUpEdit1
        '
        Me.LookUpEdit1.Location = New System.Drawing.Point(127, 40)
        Me.LookUpEdit1.Name = "LookUpEdit1"
        Me.LookUpEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.Appearance.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceDropDown.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceDropDownHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceDropDownHeader.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceFocused.Options.UseFont = True
        Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookUpEdit1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.LookUpEdit1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("PAYCODE", "PAYCODE", 30, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EMPNAME", "Name")})
        Me.LookUpEdit1.Properties.DisplayMember = "PAYCODE"
        Me.LookUpEdit1.Properties.MaxLength = 12
        Me.LookUpEdit1.Properties.NullText = ""
        Me.LookUpEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.LookUpEdit1.Properties.ValueMember = "PAYCODE"
        Me.LookUpEdit1.Size = New System.Drawing.Size(129, 20)
        Me.LookUpEdit1.TabIndex = 1
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(352, 42)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(169, 14)
        Me.LabelControl6.TabIndex = 16
        Me.LabelControl6.Text = "MonthPayable In the Month of"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(21, 74)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(31, 14)
        Me.LabelControl1.TabIndex = 32
        Me.LabelControl1.Text = "Name"
        '
        'txtPayableMonth
        '
        Me.txtPayableMonth.EditValue = New Date(2018, 1, 1, 16, 13, 14, 0)
        Me.txtPayableMonth.Location = New System.Drawing.Point(528, 39)
        Me.txtPayableMonth.Name = "txtPayableMonth"
        Me.txtPayableMonth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtPayableMonth.Properties.Appearance.Options.UseFont = True
        Me.txtPayableMonth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtPayableMonth.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtPayableMonth.Properties.Mask.EditMask = "MM/yyyy"
        Me.txtPayableMonth.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPayableMonth.Size = New System.Drawing.Size(135, 20)
        Me.txtPayableMonth.TabIndex = 2
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(431, 74)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(47, 14)
        Me.LabelControl2.TabIndex = 33
        Me.LabelControl2.Text = "Card No."
        '
        'lblName
        '
        Me.lblName.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblName.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblName.Appearance.Options.UseFont = True
        Me.lblName.Appearance.Options.UseForeColor = True
        Me.lblName.Location = New System.Drawing.Point(127, 74)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(20, 14)
        Me.lblName.TabIndex = 35
        Me.lblName.Text = "     "
        '
        'lblDept
        '
        Me.lblDept.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblDept.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblDept.Appearance.Options.UseFont = True
        Me.lblDept.Appearance.Options.UseForeColor = True
        Me.lblDept.Location = New System.Drawing.Point(811, 74)
        Me.lblDept.Name = "lblDept"
        Me.lblDept.Size = New System.Drawing.Size(24, 14)
        Me.lblDept.TabIndex = 37
        Me.lblDept.Text = "      "
        '
        'lblCardNum
        '
        Me.lblCardNum.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblCardNum.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblCardNum.Appearance.Options.UseFont = True
        Me.lblCardNum.Appearance.Options.UseForeColor = True
        Me.lblCardNum.Location = New System.Drawing.Point(543, 74)
        Me.lblCardNum.Name = "lblCardNum"
        Me.lblCardNum.Size = New System.Drawing.Size(24, 14)
        Me.lblCardNum.TabIndex = 36
        Me.lblCardNum.Text = "      "
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(699, 74)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(66, 14)
        Me.LabelControl9.TabIndex = 34
        Me.LabelControl9.Text = "Department"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(77, 568)
        Me.MemoEdit1.TabIndex = 4
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblEmployeeTableAdapter
        '
        Me.TblEmployeeTableAdapter.ClearBeforeFill = True
        '
        'TblEmployeeBindingSource
        '
        Me.TblEmployeeBindingSource.DataMember = "TblEmployee"
        Me.TblEmployeeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblEmployee1TableAdapter1
        '
        Me.TblEmployee1TableAdapter1.ClearBeforeFill = True
        '
        'XtraPayFullNFinal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraPayFullNFinal"
        Me.Size = New System.Drawing.Size(1250, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.txtEarningEarn10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningEarn9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningEarn8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningEarn7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningEarn6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningEarn5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningEarn4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningEarn3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningEarn2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningEarn1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningMed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningConv.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.PopupContainerEditLeave.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlLeave, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlLeave.ResumeLayout(False)
        CType(Me.GridControlLeave, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewLeave, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAdvance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLoan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNoticeMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNoticePay.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLeave.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLeaveInCash.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGratuity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNetPay.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.prvMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel1.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtapplicablefrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtapplicablefrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPayableMonth.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPayableMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents txtPayableMonth As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblEmployeeTableAdapter As iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter
    Friend WithEvents TblEmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblEmployee1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblDept As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblCardNum As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents ComboNepaliYear As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNEpaliMonth As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents btnApply As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdOk As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtapplicablefrom As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents prvMonth As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtNetPay As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtGratuity As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtNoticeMonth As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNoticePay As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtLeave As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLeaveInCash As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtAdvance As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtLoan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerControlLeave As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlLeave As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewLeave As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemTimeEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerEditLeave As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents txtEarningEarn10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtEarningEarn9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtEarningEarn8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtEarningEarn7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtEarningEarn6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtEarningEarn5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtEarningEarn4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtEarningEarn3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtEarningEarn2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtEarningEarn1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtEarningMed As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtEarningConv As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label3 As DevExpress.XtraEditors.LabelControl

End Class
